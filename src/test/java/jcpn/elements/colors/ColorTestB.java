package jcpn.elements.colors;

import jcpn.elements.colorsets.IColor;

public class ColorTestB extends IColor{
	public ColorTestB(int value) {
		super();
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	private int value;
	
	@Override
	public String toString() {
		return "ColorTestB [value=" + value + "]";
	}

}
