package jcpn.elements.colors;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import jcpn.elements.colorsets.IColor;

public class ColorTestA extends IColor{
	public ColorTestA(int value) {
		super();
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	private int value;

	

	@Override
	public String toString() {
		return "ColorTestA [time=" + time + ", value=" + value + ", number=" + number + "]";
	}

	
	
	
	private static AtomicInteger integer=new AtomicInteger(0);
	int number=integer.incrementAndGet();
	public static List<IColor> generateList(Integer[] values){
		List<IColor> list=new ArrayList<>();
		for(Integer i:values){
			list.add(new ColorTestA(i));
		}
		return list;
	}
}
