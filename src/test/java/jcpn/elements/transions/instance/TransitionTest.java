package jcpn.elements.transions.instance;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.junit.Assert;
import org.junit.Test;

import jcpn.NotSupportException;
import jcpn.elements.CPNet;
import jcpn.elements.Pair;
import jcpn.elements.colors.ColorTestA;
import jcpn.elements.colorsets.IColor;
import jcpn.elements.places.ConnectionPlace;
import jcpn.elements.places.GlobalPlace;
import jcpn.elements.places.IOwner;
import jcpn.elements.places.ITarget;
import jcpn.elements.places.IndividualPlace;
import jcpn.elements.places.Place;
import jcpn.elements.places.StringOwner;
import jcpn.elements.transions.ConnectionTransition;
import jcpn.elements.transions.IndividualTransition;
import jcpn.elements.transions.Transition;
import jcpn.runtime.FoldingCPNInstance;
import jcpn.runtime.IndividualCPNInstance;

public class TransitionTest {
	@Test
	@SuppressWarnings("unchecked")
	public void testGuardInNT() throws NotSupportException{
		CPNet  cpn=new CPNet();
		Map<Integer, Place> places=new HashMap<>();
		Map<Integer, Transition> transitions=new HashMap<>();
		cpn.setVersion("1.0");

		IndividualPlace p1=new IndividualPlace(1,"P1");
		IndividualPlace p2=new IndividualPlace(2,"P2");
		IndividualPlace p3=new IndividualPlace(3,"P3");
		IndividualPlace p4=new IndividualPlace(4,"P4");
		IndividualPlace p5=new IndividualPlace(5,"P5");
		IndividualPlace p6=new IndividualPlace(6,"P6");
		IndividualPlace p7=new IndividualPlace(7,"P7");
		GlobalPlace p8=new GlobalPlace(8,"P8");
		p8.setMergeFunction((lt,t)->lt.add(t));

		IndividualTransition t1=new IndividualTransition(1,"T1");
		p1.addOutput(1, 1);
		p2.addOutput(1, 1);
		p3.addOutput(1, 1);
		p4.addOutput(1, 1);
		p5.addOutput(1, 1);
		p6.addOutput(1, 1);
		p7.addOutput(1, 1);
		p8.addOutput(1, 1);
		//		t1.addOutput(2, 1).addOutput(3, 1);

		places.put(1, p1);
		places.put(2, p2);
		places.put(3, p3);
		places.put(4, p4);
		places.put(5, p5);
		places.put(6, p6);
		places.put(7, p7);
		places.put(8, p8);
		transitions.put(1, t1);
		t1.addSiCondition(2, t->((ColorTestA)t).getValue()>10);
		t1.addSiCondition(3, t->((ColorTestA)t).getValue()>10);
		t1.addBiCondition(3, 4, pair->((ColorTestA)pair.getLeft()).getValue()==((ColorTestA)pair.getRight()).getValue());
		t1.addBiCondition(5, 6, pair->((ColorTestA)pair.getLeft()).getValue()>((ColorTestA)pair.getRight()).getValue());
		t1.addBiCondition(7, 6, pair->((ColorTestA)pair.getLeft()).getValue()>((ColorTestA)pair.getRight()).getValue());
		t1.addBiCondition(7, 5, pair->((ColorTestA)pair.getLeft()).getValue()>((ColorTestA)pair.getRight()).getValue());

		StringOwner owner1=new StringOwner("s1");
		List<IColor> p1tokens=ColorTestA.generateList(new Integer[]{10,15,20,30,40,50,60});
		p1tokens.forEach(t->p1.addInitToken(owner1, t));
		List<IColor> p2tokens=Arrays.asList(new ColorTestA(10),new ColorTestA(15));
		p2tokens.forEach(t->p2.addInitToken(owner1, t));
		List<IColor> p3tokens=ColorTestA.generateList(new Integer[]{10,15});
		p3tokens.forEach(t->p3.addInitToken(owner1, t));
		List<IColor> p4tokens=ColorTestA.generateList(new Integer[]{15,15,20});
		p4tokens.forEach(t->p4.addInitToken(owner1, t));

		List<IColor> p5tokens=ColorTestA.generateList(new Integer[]{15,15,30,5});
		p5tokens.forEach(t->p5.addInitToken(owner1, t));
		List<IColor> p6tokens=ColorTestA.generateList(new Integer[]{5,20,20});
		p6tokens.forEach(t->p6.addInitToken(owner1, t));
		List<IColor> p7tokens=ColorTestA.generateList(new Integer[]{15,15,30,5});
		p7tokens.forEach(t->p7.addInitToken(owner1, t));
		//7,5,6-> (30,15,5), (30,15,5), -> (2,0,0) (2,1,0)


		List<IColor> p8tokens=Arrays.asList(new ColorTestA(15),new ColorTestA(5));
		p8tokens.forEach(t->p8.addInitToken( t));




		Function<MixedInputTokenBinding, IOutputTokenBinding> outputFunction=null;
		t1.setOutputFunction(outputFunction);

		cpn.setPlaces(places);
		cpn.setTransitions(transitions);
		cpn.preCompile();

		FoldingCPNInstance instance=new FoldingCPNInstance(cpn);
		instance.compile();

		IndividualCPNInstance cpnInstance=instance.getIndividualInstances().get(owner1);
		cpnInstance.checkNewlyTokensAndMarkAsTested();
		IndividualTransitionInstance transition=(IndividualTransitionInstance) cpnInstance.getTransitions().get(1);
		Map<Integer, List<IColor>> map1=new HashMap<>();
		map1.put(1, p1tokens);
		map1.put(2, Arrays.asList(p2tokens.get(1)));
		boolean condition=theSame(map1, transition.cachedSiBindings);
		Assert.assertTrue(condition);

		Map< Pair<Integer,Integer>, List<Pair<IColor, IColor>>> map2=new HashMap<>();
		map2.put(new Pair<>(3,4), Arrays.asList(new Pair<>(p3tokens.get(1),p4tokens.get(0)),new Pair<>(p3tokens.get(1),p4tokens.get(1))));
		//7,5,6-> (30,15,5), (30,15,5), -> (2,0,0) (2,1,0)
		map2.put(new Pair<>(5,6), Arrays.asList(new Pair<>(p5tokens.get(0),p6tokens.get(0)),new Pair<>(p5tokens.get(1),p6tokens.get(0))));
		map2.put(new Pair<>(7,6), Arrays.asList(new Pair<>(p7tokens.get(2),p6tokens.get(0))));
		map2.put(new Pair<>(7,5), Arrays.asList(new Pair<>(p7tokens.get(2),p5tokens.get(0)),new Pair<>(p7tokens.get(2),p5tokens.get(1))));

		condition=theSame(map2, transition.cachedBiBindings);
		Assert.assertTrue(condition);
		Assert.assertTrue(cpnInstance.canFire());
		MixedInputTokenBinding binding=cpnInstance.askForFire(1);
//		System.out.println(binding);
		if(cpnInstance.trytoGetTokensFromPlaces(binding)){
			cpnInstance.removeTokensFromAllTransitionsCache(binding);
			IOutputTokenBinding output=cpnInstance.fire(1, binding);
			if (output!=null) {
				cpnInstance.addLocalNewlyTokensAndScheduleNextTime(output);
			}
			cpnInstance.checkNewlyTokensAndMarkAsTested();
		}
//		System.out.println(transition.cachedSiBindings);
//		System.out.println(transition.cachedBiBindings);
		
	}
	@Test
	@SuppressWarnings("unchecked")
	public void testGuardInNTNull() throws NotSupportException{
		CPNet  cpn=new CPNet();
		Map<Integer, Place> places=new HashMap<>();
		Map<Integer, Transition> transitions=new HashMap<>();
		cpn.setVersion("1.0");

		IndividualPlace p1=new IndividualPlace(1,"P1");
		IndividualPlace p2=new IndividualPlace(2,"P2");
		IndividualPlace p3=new IndividualPlace(3,"P3");
		IndividualPlace p4=new IndividualPlace(4,"P4");
		IndividualPlace p5=new IndividualPlace(5,"P5");
		IndividualPlace p6=new IndividualPlace(6,"P6");
		IndividualPlace p7=new IndividualPlace(7,"P7");
		GlobalPlace p8=new GlobalPlace(8,"P8");
		p8.setMergeFunction((lt,t)->lt.add(t));

		IndividualTransition t1=new IndividualTransition(1,"T1");
		p1.addOutput(1, 1);
		p2.addOutput(1, 1);
		p3.addOutput(1, 1);
		p4.addOutput(1, 1);
		p5.addOutput(1, 1);
		p6.addOutput(1, 1);
		p7.addOutput(1, 1);
		p8.addOutput(1, 1);
		//		t1.addOutput(2, 1).addOutput(3, 1);

		places.put(1, p1);
		places.put(2, p2);
		places.put(3, p3);
		places.put(4, p4);
		places.put(5, p5);
		places.put(6, p6);
		places.put(7, p7);
		places.put(8, p8);
		transitions.put(1, t1);
		
		StringOwner owner1=new StringOwner("s1");
		List<IColor> p1tokens=ColorTestA.generateList(new Integer[]{10,15,20,30,40,50,60});
		p1tokens.forEach(t->p1.addInitToken(owner1, t));
		List<IColor> p2tokens=Arrays.asList(new ColorTestA(10),new ColorTestA(15));
		p2tokens.forEach(t->p2.addInitToken(owner1, t));
		List<IColor> p3tokens=ColorTestA.generateList(new Integer[]{10,15});
		p3tokens.forEach(t->p3.addInitToken(owner1, t));
		List<IColor> p4tokens=ColorTestA.generateList(new Integer[]{15,15,20});
		p4tokens.forEach(t->p4.addInitToken(owner1, t));

		List<IColor> p5tokens=ColorTestA.generateList(new Integer[]{15,15,30,5});
		p5tokens.forEach(t->p5.addInitToken(owner1, t));
		List<IColor> p6tokens=ColorTestA.generateList(new Integer[]{5,20,20});
		p6tokens.forEach(t->p6.addInitToken(owner1, t));
		List<IColor> p7tokens=ColorTestA.generateList(new Integer[]{15,15,30,5});
		p7tokens.forEach(t->p7.addInitToken(owner1, t));
		//7,5,6-> (30,15,5), (30,15,5), -> (2,0,0) (2,1,0)


		List<IColor> p8tokens=Arrays.asList(new ColorTestA(15),new ColorTestA(5));
		p8tokens.forEach(t->p8.addInitToken( t));




		Function<MixedInputTokenBinding, IOutputTokenBinding> outputFunction=null;
		t1.setOutputFunction(outputFunction);

		cpn.setPlaces(places);
		cpn.setTransitions(transitions);
		cpn.preCompile();

		FoldingCPNInstance instance=new FoldingCPNInstance(cpn);
		instance.compile();

		IndividualCPNInstance cpnInstance=instance.getIndividualInstances().get(owner1);
		cpnInstance.checkNewlyTokensAndMarkAsTested();
		IndividualTransitionInstance transition=(IndividualTransitionInstance) cpnInstance.getTransitions().get(1);
		
		Assert.assertTrue(cpnInstance.canFire());
		MixedInputTokenBinding binding=cpnInstance.askForFire(1);
//		System.out.println(binding);
		if(cpnInstance.trytoGetTokensFromPlaces(binding)){
			cpnInstance.removeTokensFromAllTransitionsCache(binding);
			IOutputTokenBinding output=cpnInstance.fire(1, binding);
			if (output!=null) {
				cpnInstance.addLocalNewlyTokensAndScheduleNextTime(output);
			}
			cpnInstance.checkNewlyTokensAndMarkAsTested();
		}
//		System.out.println(transition.cachedSiBindings);
//		System.out.println(transition.cachedBiBindings);
		
	}
	@Test
	@SuppressWarnings("unchecked")
	public void testGuardInCT() throws NotSupportException{
		CPNet  cpn=new CPNet();
		Map<Integer, Place> places=new HashMap<>();
		Map<Integer, Transition> transitions=new HashMap<>();
		cpn.setVersion("1.0");
		ConnectionPlace p1=new ConnectionPlace(1,"P1");
		ConnectionPlace p2=new ConnectionPlace(2,"P2");
		ConnectionPlace p3=new ConnectionPlace(3,"P3");
		ConnectionPlace p4=new ConnectionPlace(4,"P4");
		ConnectionPlace p5=new ConnectionPlace(5,"P5");
		ConnectionPlace p6=new ConnectionPlace(6,"P6");
		ConnectionPlace p7=new ConnectionPlace(7,"P7");
		GlobalPlace p8=new GlobalPlace(8,"P8");
		p8.setMergeFunction((lt,t)->lt.add(t));

		ConnectionTransition t1=new ConnectionTransition(1,"T1");
		p1.addOutput(1, 1);
		p2.addOutput(1, 1);
		p3.addOutput(1, 1);
		p4.addOutput(1, 1);
		p5.addOutput(1, 1);
		p6.addOutput(1, 1);
		p7.addOutput(1, 1);
		p8.addOutput(1, 1);
		//		t1.addOutput(2, 1).addOutput(3, 1);

		places.put(1, p1);
		places.put(2, p2);
		places.put(3, p3);
		places.put(4, p4);
		places.put(5, p5);
		places.put(6, p6);
		places.put(7, p7);
		places.put(8, p8);
		transitions.put(1, t1);
		t1.addSiCondition(2, t->((ColorTestA)t).getValue()>10);
		t1.addSiCondition(3, t->((ColorTestA)t).getValue()>10);
		t1.addBiCondition(3, 4, pair->((ColorTestA)pair.getLeft()).getValue()==((ColorTestA)pair.getRight()).getValue());
		t1.addBiCondition(5, 6, pair->((ColorTestA)pair.getLeft()).getValue()>((ColorTestA)pair.getRight()).getValue());
		t1.addBiCondition(7, 6, pair->((ColorTestA)pair.getLeft()).getValue()>((ColorTestA)pair.getRight()).getValue());
		t1.addBiCondition(7, 5, pair->((ColorTestA)pair.getLeft()).getValue()>((ColorTestA)pair.getRight()).getValue());

		ITarget target=new StringOwner("s2");
		IOwner owner=new StringOwner("s1");
		List<IColor> p1tokens=ColorTestA.generateList(new Integer[]{10,15,20,30,40,50,60});
		p1tokens.forEach(t->p1.addInitToken(owner,target, t));
		List<IColor> p2tokens=ColorTestA.generateList(new Integer[]{10,15});
		p2tokens.forEach(t->p2.addInitToken(owner,target, t));
		List<IColor> p3tokens=ColorTestA.generateList(new Integer[]{10,15});
		p3tokens.forEach(t->p3.addInitToken(owner,target, t));
		List<IColor> p4tokens=ColorTestA.generateList(new Integer[]{15,15,20});
		p4tokens.forEach(t->p4.addInitToken(owner,target, t));

		List<IColor> p5tokens=ColorTestA.generateList(new Integer[]{15,15,30,5});
		p5tokens.forEach(t->p5.addInitToken(owner,target, t));
		List<IColor> p6tokens=ColorTestA.generateList(new Integer[]{5,20,20});
		p6tokens.forEach(t->p6.addInitToken(owner,target, t));
		List<IColor> p7tokens=ColorTestA.generateList(new Integer[]{15,15,30,5});
		p7tokens.forEach(t->p7.addInitToken(owner,target, t));
		//7,5,6-> (30,15,5), (30,15,5), -> (2,0,0) (2,1,0)


		List<IColor> p8tokens=Arrays.asList(new ColorTestA(15),new ColorTestA(5));
		p8tokens.forEach(t->p8.addInitToken(t));




		Function<MixedInputTokenBinding, IOutputTokenBinding> outputFunction=null;
		t1.setOutputFunction(outputFunction);

		cpn.setPlaces(places);
		cpn.setTransitions(transitions);
		cpn.preCompile();

		FoldingCPNInstance instance=new FoldingCPNInstance(cpn);
		instance.compile();

		IndividualCPNInstance cpnInstance=instance.getIndividualInstances().get(owner);
		cpnInstance.checkNewlyTokensAndMarkAsTested();
		ConnectionTransitionInstance transition=(ConnectionTransitionInstance) cpnInstance.getTransitions().get(1);
	
		Assert.assertTrue(cpnInstance.canFire());
		MixedInputTokenBinding binding=cpnInstance.askForFire(1);
		System.out.println(binding);
		if(cpnInstance.trytoGetTokensFromPlaces(binding)){
			cpnInstance.removeTokensFromAllTransitionsCache(binding);
			IOutputTokenBinding output=cpnInstance.fire(1, binding);
			if (output!=null) {
				cpnInstance.addLocalNewlyTokensAndScheduleNextTime(output);
			}
			cpnInstance.checkNewlyTokensAndMarkAsTested();
		}
//		System.out.println(transition.cachedSiBindings);
//		System.out.println(transition.cachedBiBindings);
	}
	@Test
	public void testGuardInCTNull() throws NotSupportException{
		CPNet  cpn=new CPNet();
		Map<Integer, Place> places=new HashMap<>();
		Map<Integer, Transition> transitions=new HashMap<>();
		cpn.setVersion("1.0");
		ConnectionPlace p1=new ConnectionPlace(1,"P1");
		ConnectionPlace p2=new ConnectionPlace(2,"P2");
		ConnectionPlace p3=new ConnectionPlace(3,"P3");
		ConnectionPlace p4=new ConnectionPlace(4,"P4");
		ConnectionPlace p5=new ConnectionPlace(5,"P5");
		ConnectionPlace p6=new ConnectionPlace(6,"P6");
		ConnectionPlace p7=new ConnectionPlace(7,"P7");
		GlobalPlace p8=new GlobalPlace(8,"P8");
		p8.setMergeFunction((lt,t)->lt.add(t));

		ConnectionTransition t1=new ConnectionTransition(1,"T1");
		p1.addOutput(1, 1);
		p2.addOutput(1, 1);
		p3.addOutput(1, 1);
		p4.addOutput(1, 1);
		p5.addOutput(1, 1);
		p6.addOutput(1, 1);
		p7.addOutput(1, 1);
		p8.addOutput(1, 1);
		//		t1.addOutput(2, 1).addOutput(3, 1);

		places.put(1, p1);
		places.put(2, p2);
		places.put(3, p3);
		places.put(4, p4);
		places.put(5, p5);
		places.put(6, p6);
		places.put(7, p7);
		places.put(8, p8);
		transitions.put(1, t1);
		ITarget target=new StringOwner("s2");
		IOwner owner=new StringOwner("s1");
				
		
		List<IColor> p1tokens=ColorTestA.generateList(new Integer[]{10,15,20,30,40,50,60});
		p1tokens.forEach(t->p1.addInitToken(owner,target, t));
		List<IColor> p2tokens=ColorTestA.generateList(new Integer[]{10,15});
		p2tokens.forEach(t->p2.addInitToken(owner,target, t));
		List<IColor> p3tokens=ColorTestA.generateList(new Integer[]{10,15});
		p3tokens.forEach(t->p3.addInitToken(owner,target, t));
		List<IColor> p4tokens=ColorTestA.generateList(new Integer[]{15,15,20});
		p4tokens.forEach(t->p4.addInitToken(owner,target, t));

		List<IColor> p5tokens=ColorTestA.generateList(new Integer[]{15,15,30,5});
		p5tokens.forEach(t->p5.addInitToken(owner,target, t));
		List<IColor> p6tokens=ColorTestA.generateList(new Integer[]{5,20,20});
		p6tokens.forEach(t->p6.addInitToken(owner,target, t));
		List<IColor> p7tokens=ColorTestA.generateList(new Integer[]{15,15,30,5});
		p7tokens.forEach(t->p7.addInitToken(owner,target, t));
		//7,5,6-> (30,15,5), (30,15,5), -> (2,0,0) (2,1,0)


		List<IColor> p8tokens=Arrays.asList(new ColorTestA(15),new ColorTestA(5));
		p8tokens.forEach(t->p8.addInitToken(t));




		Function<MixedInputTokenBinding, IOutputTokenBinding> outputFunction=null;
		t1.setOutputFunction(outputFunction);

		cpn.setPlaces(places);
		cpn.setTransitions(transitions);
		cpn.preCompile();

		FoldingCPNInstance instance=new FoldingCPNInstance(cpn);
		instance.compile();

		IndividualCPNInstance cpnInstance=instance.getIndividualInstances().get(owner);
		cpnInstance.checkNewlyTokensAndMarkAsTested();
		ConnectionTransitionInstance transition=(ConnectionTransitionInstance) cpnInstance.getTransitions().get(1);
	
		Assert.assertTrue(cpnInstance.canFire());
		MixedInputTokenBinding binding=cpnInstance.askForFire(1);
		System.out.println(binding);
		if(cpnInstance.trytoGetTokensFromPlaces(binding)){
			cpnInstance.removeTokensFromAllTransitionsCache(binding);
			IOutputTokenBinding output=cpnInstance.fire(1, binding);
			if (output!=null) {
				cpnInstance.addLocalNewlyTokensAndScheduleNextTime(output);
			}
			cpnInstance.checkNewlyTokensAndMarkAsTested();
		}
//		System.out.println(transition.cachedSiBindings);
//		System.out.println(transition.cachedBiBindings);
	}
	private <T,C> boolean theSame(Map<T, List<C>> map1,Map<T, List<C>> map2){
		if(!map1.keySet().equals(map2.keySet())){
			return false;
		}
		for(Map.Entry<T, List<C>> entry:map1.entrySet()){
			T k=entry.getKey();
			List<C> v=entry.getValue();
			if(!new HashSet<>(v).equals(new HashSet<>(map2.get(k)))){
				return false;
			}
		}
		return true;
	}
}
