//package jcpn.elements;
//
//import com.google.gson.Gson;
//import com.google.gson.GsonBuilder;
//import com.google.gson.JsonIOException;
//import com.google.gson.JsonSyntaxException;
//import jcpn.builder.PlaceBuilder;
//import jcpn.builder.TransitionBuilder;
//import jcpn.elements.colors.ColorB;
//import jcpn.elements.colors.ColorC;
//import jcpn.elements.colors.ColorD;
//import jcpn.elements.places.Place.PlaceStrategy;
//
//import org.apache.commons.collections4.Bag;
//import org.apache.commons.collections4.bag.HashBag;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//
//import java.io.FileNotFoundException;
//import java.util.*;
//import java.util.function.BiFunction;
//import java.util.stream.Collectors;
//import java.util.stream.IntStream;
//import java.util.stream.Stream;
//
//import javax.annotation.Generated;
//
///**
// * Created by hxd on 16/5/14.
// */
//public class JsonTest {
////	@Test
////    public  void testCPN() throws JsonSyntaxException, JsonIOException, FileNotFoundException{
////    		Gson gson = new GsonBuilder()
////    		.setPrettyPrinting()
////    		//.excludeFieldsWithoutExposeAnnotation()
////    		.create();
////    		//CPNet net=gson.fromJson(new FileReader("DemoCPN.topologic"), CPNet.class);
////    		CPNet net1=new CPNet();
////    		PlaceBuilder<Integer> intPlaceBuilder=new PlaceBuilder<>();
////    		intPlaceBuilder.addPlaceTo(net1).setName("A1").setType(PlaceType.LIST).addAnInitialToken(1);
////    		intPlaceBuilder.addPlaceTo(net1).setName("A2").setType(PlaceType.BAG).addNInitialToken(2,5);
////    		PlaceBuilder<ColorB> bPlaceBuilder=new PlaceBuilder<>();
////    		bPlaceBuilder.addPlaceTo(net1).setName("B").setType(PlaceType.BAG).addNInitialToken(new ColorB(3),5);
////    		PlaceBuilder<ColorC> cPlaceBuilder=new PlaceBuilder<>();
////    		cPlaceBuilder.addPlaceTo(net1).setName("C").setType(PlaceType.BAG).addNInitialToken(new ColorC(3),5);
////    		PlaceBuilder<ColorC> cPlaceBuilder2=new PlaceBuilder<>();
////    		cPlaceBuilder2.addPlaceTo(net1).setName("D").setType(PlaceType.BAG).addNInitialToken(new ColorC(3),5);
////    		cPlaceBuilder2.<ColorD>changeColorSet().addNInitialToken(new ColorD(5), 5);
////    		
////    		TransitionBuilder builder=new TransitionBuilder();
////    		builder.addTransitionTo(net1).setName("first").setPriority(1).addOneFrom(1).addOneTo(3).addOneTo(4);
////    		builder.addTransitionTo(net1).setName("second").setPriority(2).addOneFrom(2).addOneTo(3).addOneTo(4);
////    		builder.addTransitionTo(net1).setName("third").setPriority(500).addOneFrom(3).addOneFrom(4).addOneTo(5);
////    		builder.addTransitionTo(net1).setName("fourth").setPriority(10).addOneFrom(3).addOneTo(5);
////    		builder.addTransitionTo(net1).setName("fifth").setPriority(10).addOneFrom(4);
////        net1.setVersion("1.0");
////    		
////    		
////    		CPNet net2=new CPNet();
////    		net2.getPlaces().put(1, new Place<Integer>(1, "A1", Collections.emptyList(), Arrays.asList(1), Arrays.asList(1), new ArrayList<>(), PlaceType.LIST));
////    		Bag<Integer> bag=new HashBag<>();
////    		bag.add(2,5);
////    		net2.getPlaces().put(2, new Place<Integer>(2, "A2", Collections.emptyList(), Arrays.asList(2), bag, new HashBag<>(), PlaceType.BAG));
////    		Bag<ColorB> bagb=new HashBag<>();
////    		bagb.add(new ColorB(3),5);
////    		Bag<ColorC> bagc=new HashBag<>();
////    		bagc.add(new ColorC(3),5);
////    		Bag<ColorD> bagd=new HashBag<>();
////    		bagd.add(new ColorD(3),5);
////    		net2.getPlaces().put(3, new Place<ColorB>(3, "B", Arrays.asList(1,2), Arrays.asList(3,4), bagb, new HashBag<>(), PlaceType.BAG));
////    		net2.getPlaces().put(4, new Place<ColorC>(4, "C", Arrays.asList(1,2), Arrays.asList(3,5),bagc, new HashBag<>(), PlaceType.BAG));
////    		net2.getPlaces().put(5, new Place<ColorD>(5, "D",  Arrays.asList(3),Collections.emptyList(), bagd, new HashBag<>(), PlaceType.BAG));
////    		
////    		net2.getTransitions().put(1,new Transition(1, "first", 1, Arrays.asList(1) , Arrays.asList(3,4)));
////       	net2.getTransitions().put(2,new Transition(2, "second", 2, Arrays.asList(2) , Arrays.asList(3,4)));
////       	net2.getTransitions().put(3,new Transition(3, "third", 500, Arrays.asList(3,4) , Arrays.asList(5)));
////       	net2.getTransitions().put(4,new Transition(4, "fourth", 10, Arrays.asList(3) , Arrays.asList(5)));
////       	net2.getTransitions().put(5,new Transition(5, "fifth", 10, Arrays.asList(4) , Collections.emptyList()));
////       	net2.setVersion("1.0");
////       	
////       //	System.out.println(gson.toJson(net1));
////       	Assert.assertEquals(gson.toJson(net1), gson.toJson(net2));
////    }
//	//@Test
//	public void hashbagTest(){
//		HashBag<Float> bag=new HashBag<>();
//		bag.add(5.0f,3);
//		bag.add(8.0f,4);
//		bag.add(10.1f,5);
//		System.out.println(bag.parallelStream().skip(new Random().nextInt(bag.size())).findAny().get());;
////		bag.stream().peek()
//	}
//
//	//@Test
//	public void streamTest(){
//		Stream<Integer> s1=Stream.<Integer>of(1,3,5,7,9);
//		Stream<Integer> s2=Stream.<Integer>of(2,4,6,8,10);
//		
//		List<Integer> cartesian = cartesian((x,y)->x*100+y, s1, s2).collect(Collectors.toList());
//		System.out.println(cartesian);
//	}
//	@SafeVarargs
//	private static <T> Stream<T> cartesian(BiFunction<T, T, T> aggregator, Stream<T>... streams) {
//		return Arrays.stream(streams).reduce((r, s) -> {
//			List<T> collect = s.collect(Collectors.toList());
//			return r.flatMap(m -> collect.stream().map(n -> aggregator.apply(m, n)));
//		}).orElse(Stream.empty());
//	}
//	
//	Bag<String> tokens=new HashBag<>();
//	List<Integer> requirements=new ArrayList<>();
//	@Before
//	public void generateData(){
//		tokens.add("A",10);
//		tokens.add("B",100);
//		tokens.add("C",900);
//		tokens.add("D",100);
//		tokens.add("E",1000);
//		Random random=new Random();
//		for(int i=0;i<10000000;i++){
//			tokens.add(i+"",random.nextInt(3000));
//		}
//		requirements.add(2);
//		//requirements.add(5);
//		//requirements.add(4);
//		//requirements.add(500);
//		for(int i=0;i<0;i++){
//			requirements.add(random.nextInt(10));
//		}
//	}
//	//@Test
//	public void testRandomChooseToken(){
//		Bag<String> tokens=new HashBag<>();
//		List<Integer> requirements=new ArrayList<>();
//		Random random=new Random();
//		for(int i=0;i<1000000000;i++){
//			tokens.add(i+"",random.nextInt(3000));
//		}
//		for(int i=0;i<2;i++){
//			requirements.add(100-i*5);
//		}
//		List<String> result=new CPNet().randomCandidateInOnePlace(0, requirements, tokens,new HashBag<>(), new ArrayList<String>());
//		System.out.println(result);
//	}
//	//@Test
//	public void testList3(){
//		Random random = new Random();
//		Set<String> hashSet=
//		IntStream.range(0, 2000000).mapToObj(k->
//					random.ints(160, 0, 26*2).map(i->(i>=26? 'a'-26: 'A')+i)
//				  .collect(StringBuilder::new,
//				           StringBuilder::appendCodePoint, StringBuilder::append)
//				  .toString()).collect(Collectors.toSet());
//		List<String> list=new ArrayList<>(hashSet);
//		//String randomString=;
//	}
//	//@Test
//	public void testList2(){
//		Random random = new Random();
//		Set<String> hashSet=
//		IntStream.range(0, 2000000).mapToObj(k->
//					random.ints(160, 0, 26*2).map(i->(i>=26? 'a'-26: 'A')+i)
//				  .collect(StringBuilder::new,
//				           StringBuilder::appendCodePoint, StringBuilder::append)
//				  .toString()).collect(Collectors.toSet());
//		List<String> list=hashSet.stream().collect(Collectors.toList());
//	}
//	
//	
//	//@Test
//	public void testSpecial(){
//		new CPNet().candidateInOnePlaceForOneVariable(requirements.get(0), tokens);
//	}	
//	//@Test
//	public void testBFS(){
//		new CPNet().candidateInOnePlaceBFS(requirements, tokens);
//	}	
//	
//	
//	//@Test
//	public void testEqual(){
//		//Set<String> result1=new CPNet().candidateInOnePlace(0, requirements, tokens, new ArrayList<String>()).stream().flatMap(x->x.stream()).collect(Collectors.toSet());
//		//Set<String> result2=new CPNet().candidateInOnePlaceByLambda1(0, requirements, tokens, new ArrayList<String>()).stream().flatMap(x->x.stream()).collect(Collectors.toSet());
//		Set<String> result4=
//				new CPNet().candidateInOnePlaceBFS(requirements, tokens)
//				.stream()
//				.map(list-> list.stream().reduce((a,b)->{return a+b;}))
//				.map(x->x.get())
//				.sorted()
//				//.peek(x->System.out.println(x))
//				.collect(Collectors.toSet());
////		Assert.assertArrayEquals(result4.toArray(), result3.toArray());
//	}
//
//}
