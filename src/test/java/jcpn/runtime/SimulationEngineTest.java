package jcpn.runtime;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.IntStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import jcpn.elements.CPNet;
import jcpn.elements.colors.ColorTestA;
import jcpn.elements.colorsets.IColor;
import jcpn.elements.places.ConnectionPlace;
import jcpn.elements.places.GlobalPlace;
import jcpn.elements.places.IOwner;
import jcpn.elements.places.ITarget;
import jcpn.elements.places.IndividualPlace;
import jcpn.elements.places.Place;
import jcpn.elements.places.StringOwner;
import jcpn.elements.transions.ConnectionTransition;
import jcpn.elements.transions.IndividualTransition;
import jcpn.elements.transions.Transition;
import jcpn.elements.transions.instance.IOutputTokenBinding;
import jcpn.elements.transions.instance.LocalOutputTokenBinding;
import jcpn.elements.transions.instance.MixedInputTokenBinding;
import jcpn.elements.transions.instance.RemoteOutputTokenBinding;
import jcpn.runtime.SimulationEngine.Mode;

public class SimulationEngineTest {
	Logger logger=LogManager.getLogger();
	CPNet[] nets=new CPNet[5];
	FoldingCPNInstance[] foldCPN;
	@Before
	public void initCPNets(){
		int cluster_size=20;
		//first cpn: one NT transition, can fire 6 times.
		nets[0]=new CPNet();
		Map<Integer, Place> places=new HashMap<>();
		Map<Integer, Transition> transitions=new HashMap<>();
		nets[0].setVersion("1.0");

		IndividualPlace p1=new IndividualPlace(1,"P1");
		IndividualPlace p2=new IndividualPlace(2,"P2");
		IndividualPlace p3=new IndividualPlace(3,"P3");
		GlobalPlace p8=new GlobalPlace(8,"P8");
		p8.setMergeFunction((lt,t)->lt.add(t));
		IndividualTransition t1=new IndividualTransition(1,"T1");
		p1.addOutput(1, 1);	places.put(1, p1);
		p2.addOutput(1, 1);	places.put(2, p2);
		p8.addOutput(1, 1); places.put(8, p8);
		t1.addOutput(3, 1); places.put(3, p3);
		transitions.put(1, t1);
		t1.addBiCondition(1, 2, pair-> ((ColorTestA)pair.getLeft()).getValue()== ((ColorTestA)pair.getRight()).getValue());
		List<IColor> p1tokens=ColorTestA.generateList(new Integer[]{10,15,20,30,40,50,60});
		IntStream.range(1, cluster_size).forEach(i-> p1tokens.forEach(t->p1.addInitToken(new StringOwner("s"+i), t)));
		List<IColor> p2tokens=ColorTestA.generateList(new Integer[]{15,20,30,40,50,60});
		IntStream.range(1, cluster_size).forEach(i-> p2tokens.forEach(t->p2.addInitToken(new StringOwner("s"+i), t)));
		p8.addInitToken(new ColorTestA(1));
		t1.setOutputFunction(input->{
			Map<Integer, List<IColor>> map=new HashMap<>();
			map.put(3, ColorTestA.generateList(new Integer[]{((ColorTestA)input.getSimpleTokens().get(1)).getValue()+100}));
			return LocalOutputTokenBinding.simpleBinding(map,10);
		});
		nets[0].setPlaces(places);
		nets[0].setTransitions(transitions);
		nets[0].preCompile();
		foldCPN=new FoldingCPNInstance[nets.length];
		foldCPN[0]=new FoldingCPNInstance(nets[0]);
		
		
		//net 1: one CT transition, can fire 6 times.
		nets[1]=new CPNet();
		places=new HashMap<>();
		transitions=new HashMap<>();
		nets[1].setVersion("1.0");

		ConnectionPlace p4=new ConnectionPlace(4,"P4");
		IndividualPlace p5=new IndividualPlace(5, "P5");
		ConnectionTransition t2=new ConnectionTransition(1,"T1");
		p4.addOutput(1, 1);	places.put(4, p4);
		transitions.put(1, t2);
		t2.addOutput(5,1); places.put(5, p5);
		List<IColor> p4tokens=ColorTestA.generateList(new Integer[]{10,15,20,30,40,50,60});
		IntStream.range(0, cluster_size).forEach(i-> p4tokens.forEach(t->{
			p4.addInitToken(new StringOwner("s"+i), new StringOwner("s"+(i+1)%cluster_size), t);
			p4.addInitToken(new StringOwner("s"+i), new StringOwner("s"+(i+2)%cluster_size), t);
		}));
		p8.addInitToken(new ColorTestA(1));
		t2.setOutputFunction(input->{
			Map<Integer, List<IColor>> map=new HashMap<>();
			map.put(5, ColorTestA.generateList(new Integer[]{((ColorTestA)input.getConnectionTokens().get(4)).getValue()+100}));
			return RemoteOutputTokenBinding.remoteBinding(map,10,input.getTarget());
		});
		nets[1].setPlaces(places);
		nets[1].setTransitions(transitions);
		nets[1].preCompile();
		foldCPN[1]=new FoldingCPNInstance(nets[1]);
		foldCPN[1].addSoutInstance(new StringOwner("CLIENT"),  
				target->Collections.singletonMap(5, ColorTestA.generateList(new Integer[]{111})), 
				target->11,true);
		
	}
//	@Test
	public void test0() throws InterruptedException{
		ManualSimulationEngine engine=new ManualSimulationEngine(foldCPN[0],Mode.STREAM);
		engine.compile();
		while(engine.hasNextTimepoint()){
			Long time=engine.getNextTimepoint();
			Set<IOwner> sendings=engine.getNextSendingInstances(time);
			if(sendings!=null)
				sendings.forEach(owner->{
					Set<ITarget> targets=engine.getAllSendingTargets(time,owner);
					targets.forEach(target->engine.runNextSendingEvent(time,owner,target));
				});
			Set<IOwner> runnings=engine.getNextRunningInstances(time); 
			if(runnings!=null)
				runnings.forEach(owner->{
					while(true){
						List<Integer> tids=engine.getAllPossibleFire(owner);
						if(tids.size()>0){
							Collections.shuffle(tids);
							MixedInputTokenBinding binding=engine.askForBinding(owner,tids.get(0));
							IOutputTokenBinding out=engine.fire(owner,tids.get(0),binding);
							logger.debug(()->"owner:"+owner);
							logger.debug(()->"binding:"+binding);
							logger.debug(()->"out:"+out);
							logger.debug(()->"--------");
						}else{
							break;
						}
					}
				}); 
		}

	}
//	@Test
	public void testAuto0() throws InterruptedException{
		AutoSimulationEngine engine=new AutoSimulationEngine(foldCPN[0],Mode.STREAM);
		engine.compile();
		 while(engine.hasNextTimepoint()){
		  System.out.println("next time point:"+engine.getNextTimepoint());
		 	engine.nextRound();
		 }
	}
	@Test
	public void testAuto1() throws InterruptedException{
		AutoSimulationEngine engine=new AutoSimulationEngine(foldCPN[1],Mode.STREAM);
		engine.compile();
		new Thread(()->{
			try {
				engine.pushSoutTokens(100L, new StringOwner("CLIENT"), new ITarget[]{new StringOwner("s1")});
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();
		 while(engine.hasNextTimepoint()){
		  System.out.println("next time point:"+engine.getNextTimepoint());
		 	engine.nextRound();
		 }
		 
	}
	 

}
