package jcpn.runtime;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import jcpn.NotSupportException;
import jcpn.elements.CPNet;
import jcpn.elements.colors.ColorTestA;
import jcpn.elements.colors.ColorTestB;
import jcpn.elements.places.ConnectionPlace;
import jcpn.elements.places.IOwner;
import jcpn.elements.places.IndividualPlace;
import jcpn.elements.places.Place;
import jcpn.elements.places.StringOwner;
import jcpn.elements.transions.IndividualTransition;
import jcpn.elements.transions.Transition;
import jcpn.elements.transions.instance.IOutputTokenBinding;
import jcpn.elements.transions.instance.MixedInputTokenBinding;
import jcpn.elements.transions.instance.TransitionInstance;

public class CPNBuilderTest {
	CPNet cpn=new CPNet();
	FoldingCPNInstance instance;
	@Before
	public void buildCPN() throws NotSupportException{
		
		Map<Integer, Place> places=new HashMap<>();
		Map<Integer, Transition> transitions=new HashMap<>();
		cpn.setVersion("1.0");
		
		IndividualPlace p1=new IndividualPlace(1,"P1");
		IndividualPlace p2=new IndividualPlace(2,"P2");
		ConnectionPlace p3=new ConnectionPlace(3,"P3");
		IndividualPlace p4=new IndividualPlace(4,"P4");
		IndividualPlace p5=new IndividualPlace(5,"P5");
		IndividualPlace p6=new IndividualPlace(6,"P6");
		
		IndividualTransition t1=new IndividualTransition(1,"T1");
		p1.addOutput(1, 1);
		p4.addOutput(1, 1);
		p5.addOutput(1, 1);
		p6.addOutput(1, 1);
		
		
		t1.addOutput(2, 1).addOutput(3, 1);
		
		places.put(1, p1);
		places.put(2, p2);
		places.put(3, p3);
		places.put(4, p4);
		places.put(5, p5);
		places.put(6, p6);
		transitions.put(1, t1);
	
		IOwner owner1=new StringOwner("s1");
		IOwner owner2=new StringOwner("s2");
		IOwner owner3=new StringOwner("s3");
		
		p1.addInitToken(owner1, new ColorTestA(10))
			.addInitToken(owner1, new ColorTestA(15))
			.addInitToken(owner1, new ColorTestA(15))
			.addInitToken(owner1, new ColorTestA(25))
			.addInitToken(owner1, new ColorTestA(35))
			.addInitToken(owner1, new ColorTestA(40))
			.addInitToken(owner1, new ColorTestA(40))
			.addInitToken(owner1, new ColorTestA(30))
			.addInitToken(owner1, new ColorTestA(50))
			.addInitToken(owner1, new ColorTestA(60))
			.addInitToken(owner2, new ColorTestA(20));
		
		
		p4.addInitToken(owner3, new ColorTestA(10))
			.addInitToken(owner2, new ColorTestA(20))
			.addInitToken(owner2, new ColorTestA(30))
			.addInitToken(owner1, new ColorTestA(10))
			.addInitToken(owner1, new ColorTestA(15))
			.addInitToken(owner1, new ColorTestA(15))
			.addInitToken(owner1, new ColorTestA(25))
			.addInitToken(owner1, new ColorTestA(36))
			.addInitToken(owner1, new ColorTestA(40))
			.addInitToken(owner1, new ColorTestA(40))
			;
		p5.addInitToken(owner3, new ColorTestB(10))
			.addInitToken(owner1, new ColorTestB(10))
			.addInitToken(owner1, new ColorTestB(15))
			.addInitToken(owner1, new ColorTestB(15))
			.addInitToken(owner1, new ColorTestB(25))
			.addInitToken(owner1, new ColorTestB(35))
			.addInitToken(owner1, new ColorTestB(40))
			.addInitToken(owner1, new ColorTestB(40))
			.addInitToken(owner1, new ColorTestB(30))
			.addInitToken(owner1, new ColorTestB(50))
			.addInitToken(owner2, new ColorTestB(20));
		p6.addInitToken(owner3, new ColorTestB(10))
		.addInitToken(owner1, new ColorTestB(10))
		.addInitToken(owner1, new ColorTestB(15))
		.addInitToken(owner1, new ColorTestB(15))
		.addInitToken(owner1, new ColorTestB(25))
		.addInitToken(owner1, new ColorTestB(35))
		.addInitToken(owner1, new ColorTestB(40))
		.addInitToken(owner1, new ColorTestB(40))
		.addInitToken(owner1, new ColorTestB(30))
		.addInitToken(owner1, new ColorTestB(50))
		.addInitToken(owner2, new ColorTestB(20));
//		for(int i=0;i<1000000;i++){
//			p1.addInitToken("s1", new ColorTestA(i+10));
//		}
//		for(int i=0;i<10;i++){
//			p5.addInitToken("s1", new ColorTestB(i+10));
//		}
		
		t1.addSiCondition(5, x-> ((ColorTestB)x).getValue()>25);
		t1.addBiCondition(1, 4, pair->((ColorTestA)pair.getLeft()).getValue()==((ColorTestA)pair.getRight()).getValue());
		t1.addBiCondition(1, 5, pair->((ColorTestA)pair.getLeft()).getValue()==((ColorTestB)pair.getRight()).getValue());	
		Function<MixedInputTokenBinding, IOutputTokenBinding> outputFunction=null;
		t1.setOutputFunction(outputFunction);
		
		cpn.setPlaces(places);
		cpn.setTransitions(transitions);
		cpn.preCompile();
		instance=new FoldingCPNInstance(cpn);
		instance.compile();
	}
	
	
	
	@Test
	public void testInstancelize(){
		IOwner owner1=new StringOwner("s1");
		IndividualCPNInstance cpnInstance=instance.getIndividualInstances().get(owner1);
		TransitionInstance transitionInstance=cpnInstance.getTransitions().get(1);
		transitionInstance.checkNewTokens4Firing(cpnInstance);
//		System.out.println(transitionInstance.toString());
		System.out.println("fire:"+transitionInstance.canFire(cpnInstance));
		MixedInputTokenBinding binding=transitionInstance.randomBinding(cpnInstance);
//		System.out.println("want to bind:"+binding);
		boolean success=cpnInstance.trytoGetTokensFromPlaces(binding);
		System.out.println("binding:"+success);
		cpnInstance.removeTokensFromAllTransitionsCache(binding);
//		System.out.println("after remove:"+transitionInstance);

//		IndividualCPNInstance cpn=instances.get(start);
//		cpn.checkNewlyTokensAndMarkAsTested();
//		while(cpn.canFire()){
//			Integer tid=cpn.getARandomTranstionWhichCanFire();
//			MixedInputTokenBinding binding=cpn.askForFire(tid);
//			if(cpn.trytoGetTokensFromPlaces(binding)){
//				cpn.removeTokensFromAllTransitionsCache(binding);
//				IOutputTokenBinding output=cpn.fire(tid, binding);
//				cpn.addLocalNewlyTokens(output);
//				cpn.checkNewlyTokensAndMarkAsTested();
//			}
//		}
	}
	
	//@Test
	public void jsonTest(){
		Gson gson = new GsonBuilder()
	    		.setPrettyPrinting()
	    		//.excludeFieldsWithoutExposeAnnotation()
	    		.create();
		System.out.println(gson.toJson(cpn));;
	}

}
