package test;

import org.junit.Test;

public class HistogramTest {
	@Test
	public void math3Test(){
		final int BIN_COUNT = 20;
		double[] data = {12, 2, 3.33, 14, 15, 12, 13, 104, 10, 20}; 

		org.apache.commons.math3.random.EmpiricalDistribution distribution = new org.apache.commons.math3.random.EmpiricalDistribution(BIN_COUNT);
		distribution.load(data);
		System.out.print(distribution.getSampleStats().getMin()+",");
		System.out.print(distribution.getSampleStats().getMax()+",");
		System.out.println(distribution.getSampleStats().getMean());
		System.out.println((long)distribution.sample());
	}
}
