package jcpn.elements.places;

import java.util.List;
import java.util.Map;

import jcpn.elements.colorsets.IColor;

public interface IPlaceMonitor {
	public void reportWhenDigest(IOwner owner,IColor digestedToken, Iterable<IColor> currentTokens, String transitionName, int transitionId);
	/**
	 * for simplePlace
	 * @param owner
	 * @param newTokens
	 * @param currentTokens
	 * @param transitionName
	 * @param transitionId
	 */
	public void reportWhenGenerated(IOwner owner,List<IColor> newTokens, Iterable<IColor> currentTokens, String transitionName, int transitionId);
	/**
	 * for connectionPlace
	 * @param owner
	 * @param newTokens
	 * @param testedTokens
	 * @param currentlyTokens
	 * @param futureTokens
	 * @param transitionName
	 * @param transitionId
	 */
	public void reportWhenGenerated(IOwner owner,Map<ITarget,List<IColor>> newTokens, 
			Map<ITarget,List<IColor>> testedTokens,
			Map<ITarget,List<IColor>> currentlyTokens, 
			Map<ITarget,List<IColor>> futureTokens, 
			String transitionName, int transitionId);
}
