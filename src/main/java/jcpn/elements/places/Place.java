package jcpn.elements.places;

import java.util.HashMap;
import java.util.Map;

import jcpn.elements.colorsets.IColor;
import jcpn.elements.colorsets.ReplaceFunction;

public abstract class Place{
	int id;
	String name;
	PlaceType type;
	boolean infiniteRead=false;// when the place as an input, whether the tokens will be digested.  
	ReplaceFunction<IColor> replaceFunction;//only when isInfinitedRead==true, this is active, otherwise the function will never be called.
	
//	boolean freedom=false;
	public static enum PlaceType{
		IndividualPlace, ConnectionPlace, GlobalPlace
	}
	
	public Place(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	/**
	 * transition id -> number of required tokens
	 */
	Map<Integer,Integer> outArcs=new HashMap<>();

	//public abstract Integer getNumberOfOutToken(int transition);
	
	PlaceStrategy strategy=PlaceStrategy.BAG;
	
	/**
	 * bag: disordered
	 * <br>
	 * fifo:  if time is different, ordering them. If time are the same, disordering them.
	 * <br> 
	 * @author hxd
	 *
	 */
	public static enum  PlaceStrategy{
		BAG,FIFO
		
	};
	
	
	

//	public void addTokens(Collection<IColor> collection,IColor token, int number){
//		if(strategy==PlaceStrategy.BAG){
//			((HashBag<IColor>) collection).add(token, number);
//			//((HashBag<ColorSet>) collection).size();
//			//((HashBag<ColorSet>) collection).stream().skip(5).findFirst();
//		}else{
//			for(int i=0;i<number;i++){
//				collection.add(token);
//			}
//		}
//	}
	

	

	
	
	
	public Place(){
		
	}
	public Place(int id){
		this();
		this.id=id;
	}
	

	
	public PlaceStrategy getPlaceStrategy() {
		return strategy;
	}
	public void setPlaceStrategy(PlaceStrategy type) {
		if(this.strategy!=type){
			resetType(type);
		}
		this.strategy = type;
	}
	public void resetType(PlaceStrategy newType){
		//TODO firstly, we only support BAG. 
//		this.initialTokens=copyOneCollection(this.initialTokens, (newType==PlaceType.BAG?new HashBag<>():new ArrayList<>()));
//		this.currentTokens=copyOneCollection(this.currentTokens, (newType==PlaceType.BAG?new HashBag<>():new ArrayList<>()));
	}
	
//	@SuppressWarnings("unused")
//	private Collection<IColor> copyOneCollection(Collection<IColor> ori, Collection<IColor>target){
//		if(ori!=null){
//			target.addAll(this.initialTokens);
//			return target;
//		}else 
//			return ori;
//	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}







	public Map<Integer,Integer> getOutArcs() {
		return outArcs;
	}

	public void setOutArcs(Map<Integer,Integer> outArcs) {
		this.outArcs = outArcs;
	}
	public Place addOutput(Integer transitionId, int number){
		if(outArcs==null) outArcs=new HashMap<>();
		outArcs.put(transitionId, number);
		return this;
	}







	public PlaceType getType() {
		return type;
	}







	public void setType(PlaceType type) {
		this.type = type;
	}


	public void setInfiniteRead(boolean infiniteRead){
		this.infiniteRead=infiniteRead;
	}
	public boolean isInfiniteRead(){
		return infiniteRead;
	}
	public ReplaceFunction<IColor> getReplaceFunction() {
		return replaceFunction;
	}
	/**
	 * only when infinitedRead==truem, it is active.
	 * 
	 * @param replaceFunction
	 */
	public void setReplaceFunction(ReplaceFunction<IColor> replaceFunction) {
		this.replaceFunction = replaceFunction;
	}





//	public boolean isFreedom() {
//		return freedom;
//	}
//	public void setFreedom(boolean freedom) {
//		this.freedom = freedom;
//	}
}
