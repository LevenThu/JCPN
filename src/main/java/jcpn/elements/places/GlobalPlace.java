package jcpn.elements.places;

import java.util.List;

import jcpn.elements.colorsets.GlobalPlaceColor;
import jcpn.elements.colorsets.IColor;
import jcpn.elements.colorsets.MergeFunction;

/**
 * When  global place is used as a input place, it does not consume tokens.
 * That is, the place only accept new tokens (however, we all a negative token to be added in it, then the tokens are digested in fact.) 
 * <br> global place has no replace function, but has mergeFunction.
 * @author hxd
 *
 */
public class GlobalPlace extends Place{
	private GlobalPlaceColor<IColor> initialToken=new GlobalPlaceColor<>();

	public GlobalPlace(int id, String name) {
		super(id, name);
		this.setType(PlaceType.GlobalPlace);
		this.infiniteRead=true;
	}

	public GlobalPlaceColor<IColor> getInitialToken() {
		return initialToken;
	}
	
	public void setMergeFunction(MergeFunction<IColor> function){
		initialToken.setMerger(function);
	}
	/**
	 * only the first token is valid
	 * @param initialTokens
	 */
	public void setInitialTokens(List<GlobalPlaceColor<IColor>> initialTokens) {
		this.initialToken = initialTokens.get(0);
	}

	public void setInitialToken(GlobalPlaceColor<IColor> realToken){
		this.initialToken=realToken;
	}
	
	public GlobalPlace addInitToken(IColor token){
		initialToken.addToken(token);
		return this;
	}
}
