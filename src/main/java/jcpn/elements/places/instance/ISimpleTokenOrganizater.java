package jcpn.elements.places.instance;

import java.util.List;

import jcpn.elements.colorsets.IColor;
import jcpn.elements.places.IOwner;

public interface ISimpleTokenOrganizater {
	public List<IColor> getNewlyTokens();
	public void setNewlyTokens(List<IColor> newlyTokens);
	public List<IColor> getFutureTokens();
	public void setFutureTokens(List<IColor> futureTokens);
	public List<IColor> getTestedTokens();
	public void setTestedTokens(List<IColor> testedTokens);
	/**
	 * currently, this method is not thread safe.
	 * @param tokens
	 */
	public void classfyNewlyTokens(IOwner owner,List<IColor> tokens);
	
	public void removeTokenFromTest(IColor token);
}
