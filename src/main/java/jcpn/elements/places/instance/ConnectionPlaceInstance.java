package jcpn.elements.places.instance;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jcpn.elements.colorsets.IColor;
import jcpn.elements.places.ConnectionPlace;
import jcpn.elements.places.IOwner;
import jcpn.elements.places.ITarget;
import jcpn.elements.places.Place.PlaceStrategy;
import jcpn.elements.transions.instance.MixedInputTokenBinding;
import jcpn.runtime.GlobalClock;
import jcpn.runtime.IndividualCPNInstance;

/**
 * @author hxd
 *
 * @param <O>
 * @param <T>
 */
public class ConnectionPlaceInstance extends PlaceInstance implements IConnectionTokenOrganizater{
	private static Logger logger=LogManager.getLogger();
	public ConnectionPlaceInstance(ConnectionPlace place, IOwner owner, Map<ITarget, List<IColor>> tokens) {
		super(place);
		this.owner=owner;
		this.targets=tokens.keySet().stream().collect(HashSet<ITarget>::new,HashSet::add,HashSet::addAll);
		if(this.getOutArcs().size()>1){
			logger.warn("connection place only support one output arc, only the first one effects");
		}
		classfyNewlyTokens(tokens);
	}
	
	public ConnectionPlaceInstance(ConnectionPlace place, IOwner owner, Set<ITarget> targets) {
		super(place);
		this.owner=owner;
		this.targets=new HashSet<>(targets);
		if(this.getOutArcs().size()>1){
			logger.warn("connection place only support one output arc, only the first one effects");
		}
	}
	IOwner owner;
	Set<ITarget> targets; 
	Map<ITarget,List<IColor>> newlyTokens=new HashMap<>();
	Map<ITarget,List<IColor>> testedTokens=new HashMap<>();
	Map<ITarget,List<IColor>> futureTokens=new HashMap<>();
	
	@Override
	public void classfyNewlyTokens(ITarget target, List<IColor> tokens) {
		if(!targets.contains(target))
			targets.add(target);
		if(this.getPlaceStrategy().equals(PlaceStrategy.BAG)){
			copyTokens(target,tokens, 
					(t,color)->futureTokens.get(t).add(random.nextInt(futureTokens.get(t).size()+1),color), 
					(t,color)->newlyTokens.get(t).add(random.nextInt(newlyTokens.get(t).size()+1),color));
		}else{
			copyTokens(target,tokens, 
					(t,color)->addTokenBehavior(futureTokens.get(t),color), 
					(t,color)->addTokenBehavior(newlyTokens.get(t),color));
		}
	}
	@Override
	public void classfyNewlyTokens(Map<ITarget, List<IColor>> tokens) {
		if(this.getPlaceStrategy().equals(PlaceStrategy.BAG)){
			copyTokens(tokens, 
					(t,color)->futureTokens.get(t).add(random.nextInt(futureTokens.get(t).size()+1),color), 
					(t,color)->newlyTokens.get(t).add(random.nextInt(newlyTokens.get(t).size()+1),color));
		}else{
			copyTokens(tokens, 
					(t,color)->addTokenBehavior(futureTokens.get(t),color), 
					(t,color)->addTokenBehavior(newlyTokens.get(t),color));
		}
	}
	private void copyTokens(Map<ITarget, List<IColor>> tokens,BiConsumer<ITarget,IColor> futuerConsumer,BiConsumer<ITarget,IColor> newlyConsumer) {
		tokens.entrySet().stream().forEach(entry->{
			copyTokens(entry.getKey(),entry.getValue(),futuerConsumer,newlyConsumer);
		});
	}
	private void copyTokens(ITarget target, List<IColor> tokens,BiConsumer<ITarget,IColor> futuerConsumer,BiConsumer<ITarget,IColor> newlyConsumer) {
		GlobalClock globalClock=GlobalClock.getInstance();
			tokens.stream().forEach(color->{
				if(color.getTime()>globalClock.getTime()){
					if(futureTokens.get(target)==null){
						futureTokens.put(target, new ArrayList<>());
					}
					futuerConsumer.accept(target, color);
				}else{
					if(newlyTokens.get(target)==null){
						newlyTokens.put(target, new ArrayList<>());
					}
					newlyConsumer.accept(target, color);
				}
			});
	}
	@Override
	public void markTokensAsTested(IndividualCPNInstance cpn){
		
		newlyTokens.forEach((target,list)->{
			testedTokens.computeIfAbsent(target, t->new ArrayList<>());
			if(isInfiniteRead()&&replaceFunction!=null){
				list.forEach(t->{
					cpn.removeTokensFromAllTransitionsCache(
							MixedInputTokenBinding.connectionBinding(this.getOwner(), target, Collections.singletonMap(this.id, 
									replaceFunction.replace(testedTokens.get(target), t)))
							);
					
				});
			}else{
				testedTokens.get(target).addAll(list);
			}
		});
		newlyTokens.clear();
	}
	@Override
	public String toString() {
		return "CP [owner=" + owner + ", id=" + getId() + ", targets=" + targets + ", newlyTokens=" + newlyTokens
				+ ", testedTokens=" + testedTokens + ", futureTokens=" + futureTokens  + ", name=" + getName()
				+ ", placeStrategy=" + getPlaceStrategy() + ", outArcs=" + getOutArcs() + "]";
	}

	@Override
	public boolean hasNew() {
		futureTokens.entrySet().stream().forEach(entry->{
			List<IColor> timeup=entry.getValue().stream().filter(t->t.getTime()<=GlobalClock.getInstance().getTime()).collect(Collectors.toList());
			if(timeup.size()>0){
				this.newlyTokens.computeIfAbsent(entry.getKey(), t->new ArrayList<>());
				newlyTokens.get(entry.getKey()).addAll(timeup);
			}
			entry.getValue().removeAll(timeup);
		});
		return this.newlyTokens.size()>0;
	}

	public IOwner getOwner() {
		return owner;
	}

	public void setOwner(IOwner owner) {
		this.owner = owner;
	}

	public Set<ITarget> getTargets() {
		return targets;
	}

	public void setTargets(Set<ITarget> targets) {
		this.targets = targets;
	}

	public Map<ITarget, List<IColor>> getNewlyTokens() {
		return newlyTokens;
	}

	public void setNewlyTokens(Map<ITarget, List<IColor>> newlyTokens) {
		this.newlyTokens = newlyTokens;
	}

	public Map<ITarget, List<IColor>> getTestedTokens() {
		return testedTokens;
	}

	public void setTestedTokens(Map<ITarget, List<IColor>> testedTokens) {
		this.testedTokens = testedTokens;
	}

	public Map<ITarget, List<IColor>> getFutureTokens() {
		return futureTokens;
	}

	public void setFutureTokens(Map<ITarget, List<IColor>> futureTokens) {
		this.futureTokens = futureTokens;
	}

	@Override
	public void removeTokenFromTest(ITarget target, IColor token) {
		testedTokens.computeIfPresent(target, 
				(key,list)->{list.remove(token); return list.size()==0?null:list;} );
	}

	
}
