package jcpn.elements.places.instance;

import java.util.List;
import java.util.Map;

import jcpn.elements.colorsets.IColor;
import jcpn.elements.places.ITarget;

public interface IConnectionTokenOrganizater {
	/**
	 * currently, this method is not thread safe.
	 * @param tokens
	 */
	public void classfyNewlyTokens(ITarget target, List<IColor> tokens);
	/**
	 * currently, this method is not thread safe.
	 * @param tokens
	 */	
	public void classfyNewlyTokens(Map<ITarget, List<IColor>> tokens);
	
	public void removeTokenFromTest(ITarget target,IColor token);
}
