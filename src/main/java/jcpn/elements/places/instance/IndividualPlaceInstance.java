package jcpn.elements.places.instance;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jcpn.elements.colorsets.IColor;
import jcpn.elements.places.IOwner;
import jcpn.elements.places.IndividualPlace;
import jcpn.elements.places.Place.PlaceStrategy;
import jcpn.elements.transions.instance.MixedInputTokenBinding;
import jcpn.runtime.GlobalClock;
import jcpn.runtime.IndividualCPNInstance;

public class IndividualPlaceInstance extends PlaceInstance implements ISimpleTokenOrganizater{
	private static Logger logger=LogManager.getLogger();
	public IndividualPlaceInstance(IndividualPlace place,  IOwner owner, List<IColor> tokens) {
		this(place, owner);
		classfyNewlyTokens(owner, tokens);
	}
	public IndividualPlaceInstance(IndividualPlace place, IOwner owner) {
		super(place);
		this.owner=owner;
	}
	IOwner owner;
	List<IColor> newlyTokens=new ArrayList<>();
	List<IColor> testedTokens=new ArrayList<>();
	List<IColor> futureTokens=new ArrayList<>();

	@Override
	public void classfyNewlyTokens(IOwner owner,List<IColor> tokens) {
		if(!owner.equals(this.owner)){
			logger.error("individual place instance requires the same owner for classfyNewlyTokens");
		}
		if(this.getPlaceStrategy().equals(PlaceStrategy.BAG)){
			classfyNewlyTokens(tokens, 
							   color->futureTokens.add(random.nextInt(futureTokens.size()+1),color), 
							   color->newlyTokens.add(random.nextInt(newlyTokens.size()+1),color)
					);
		}else{
			//FIFO is more complicate. If two tokens have the same time, they are disordered. otherwise, they are ordered.
			classfyNewlyTokens(tokens, 
					   color->addTokenBehavior(futureTokens, color), 
					   color->addTokenBehavior(newlyTokens, color)
					);
		}
	}
	
	private void classfyNewlyTokens(List<IColor> tokens, Consumer<IColor> futuerConsumer,Consumer<IColor> newlyConsumer){
		GlobalClock globalClock=GlobalClock.getInstance();
		tokens.stream().forEach(color->{
			if(color.getTime()>globalClock.getTime()){
				futuerConsumer.accept(color);
			}else{
				newlyConsumer.accept(color);
			}
		});
	}
	@Override
	public  void markTokensAsTested(IndividualCPNInstance cpn){
		
		if(isInfiniteRead()&&replaceFunction!=null){
			newlyTokens.forEach(t->{
					cpn.removeTokensFromAllTransitionsCache(
							MixedInputTokenBinding.simpleBinding(this.owner, 
									Collections.singletonMap(this.id, replaceFunction.replace(testedTokens, t))));
				});
		}else{
			testedTokens.addAll(newlyTokens);
		}
		newlyTokens.clear();
	}

	@Override
	public String toString() {
		return "IP [ id=" + id + ", owner=" + owner + ", newlyTokens=" + newlyTokens + ", testedTokens="
				+ testedTokens + ", futureTokens=" + futureTokens +  ", name=" + name + ", placeStrategy="
				+ placeStrategy + ", outArcs=" + outArcs + ", type=" + type + "]";
	}
	@Override
	public boolean hasNew() {
		futureTokens.stream().filter(t->t.getTime()<=GlobalClock.getInstance().getTime())
							 .forEach(t->newlyTokens.add(t));
		futureTokens=futureTokens.stream().filter(t->t.getTime()>GlobalClock.getInstance().getTime()).collect(Collectors.toList());
		return this.newlyTokens.size()>0;
	}
	public IOwner getOwner() {
		return owner;
	}
	public void setOwner(IOwner owner) {
		this.owner = owner;
	}
	public List<IColor> getNewlyTokens() {
		return newlyTokens;
	}
	public void setNewlyTokens(List<IColor> newlyTokens) {
		this.newlyTokens = newlyTokens;
	}
	public List<IColor> getTestedTokens() {
		return testedTokens;
	}
	public void setTestedTokens(List<IColor> testedTokens) {
		this.testedTokens = testedTokens;
	}
	public List<IColor> getFutureTokens() {
		return futureTokens;
	}
	public void setFutureTokens(List<IColor> futureTokens) {
		this.futureTokens = futureTokens;
	}
	@Override
	public void removeTokenFromTest(IColor token) {
		testedTokens.remove(token);
	}

}
