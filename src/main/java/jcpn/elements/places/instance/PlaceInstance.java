package jcpn.elements.places.instance;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import jcpn.elements.colorsets.IColor;
import jcpn.elements.colorsets.ReplaceFunction;
import jcpn.elements.places.Place;
import jcpn.elements.places.Place.PlaceStrategy;
import jcpn.elements.places.Place.PlaceType;
import jcpn.runtime.IndividualCPNInstance;

public abstract class PlaceInstance{
	@Override
	public String toString() {
		return "PlaceInstance [id=" + id + ", name=" + name + ", placeStrategy=" + placeStrategy + ", outArcs="
				+ outArcs + ", type=" + type + "]";
	}
	int id;
	String name;
	PlaceStrategy placeStrategy;
	Map<Integer,Integer> outArcs=new HashMap<>();
	PlaceType type;
	boolean infiniteRead;
	ReplaceFunction<IColor> replaceFunction;
	

//	boolean freedom;
	public abstract  void markTokensAsTested(IndividualCPNInstance cpn);
	public abstract boolean hasNew();
	protected Random random=new Random();
	
	public PlaceInstance(Place place){
		this.placeStrategy=place.getPlaceStrategy();
		this.id=place.getId();
		this.name=place.getName();
		this.outArcs=place.getOutArcs();
		this.type=place.getType();
		this.infiniteRead=place.isInfiniteRead();
		this.replaceFunction=place.getReplaceFunction();
//		this.freedom=place.isFreedom();
	}
	public Integer getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public PlaceStrategy getPlaceStrategy() {
		return placeStrategy;
	}
	public void setPlaceStrategy(PlaceStrategy placeStrategy) {
		this.placeStrategy = placeStrategy;
	}
	public Map<Integer,Integer> getOutArcs() {
		return outArcs;
	}
	public void setOutArcs(Map<Integer,Integer> outArcs) {
		this.outArcs = outArcs;
	}

	public PlaceType getType() {
		return type;
	}
	public void setType(PlaceType type) {
		this.type = type;
	}
	
	protected void addTokenBehavior(List<IColor> tokens,IColor color){
		int start=tokens.size(),end=start+1;
		   for(int i=0;i<tokens.size();i++){
			   if(tokens.get(i).getTime()>color.getTime()){
				   tokens.add(i, color);
				   return;
			   }
			   else if(tokens.get(i).getTime()==color.getTime()){
				   start=i;
				   end=i+1;
				   break;
			   }
		   }
		   if(start==tokens.size()){//size==0 or all are smaller.
			   tokens.add(color);
			   return;
		   }
		   for(int i=end;i<tokens.size();i++){
			   if(tokens.get(i).getTime()>color.getTime()){
				   end=i;
				   break;
			   }
		   }
		   tokens.add(random.nextInt(end-start+1)+start,color);
	}
//	public boolean isFreedom() {
//		return freedom;
//	}
//	public void setFreedom(boolean freedom) {
//		this.freedom = freedom;
//	}
	public boolean isInfiniteRead() {
		return infiniteRead;
	}
	public void setInfiniteRead(boolean infiniteRead) {
		this.infiniteRead = infiniteRead;
	}

	
}
