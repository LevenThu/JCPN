package jcpn.elements.places.instance;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jcpn.elements.colorsets.GlobalPlaceColor;
import jcpn.elements.colorsets.IColor;
import jcpn.elements.places.GlobalPlace;
import jcpn.elements.places.IOwner;
import jcpn.runtime.GlobalClock;
import jcpn.runtime.IndividualCPNInstance;

public class GlobalPlaceInstance extends PlaceInstance implements ISimpleTokenOrganizater{
	private static Logger logger=LogManager.getLogger();	
	GlobalPlaceColor<IColor> newlyToken;
	List<IColor> futureTokens=new LinkedList<>();;
//	List<IColor> testedTokens=new LinkedList<>();;
	public GlobalPlaceInstance(GlobalPlace place) {
		super(place);
		newlyToken=place.getInitialToken();
		this.setInfiniteRead(true);
	}

	Random random=new Random();
	/**
	 * put future tokens into futureTokens. Otherwise put it into newlyTokens
	 * <br> this is not thread safety. But if two threads input different 'target', they can call it in parallel.
	 * <br> //global place has no future Tokens.
	 * @param tokens
	 */
	@Override
	public void classfyNewlyTokens(IOwner target,List<IColor> tokens) {
		GlobalClock globalClock=GlobalClock.getInstance();
		tokens.stream().forEach(color->{
			if(color.getTime()>globalClock.getTime()){
				futureTokens.add(color);
			}else{
				newlyToken.addToken(color);
			}
		});
		
	}
	
	@Override
	public void markTokensAsTested(IndividualCPNInstance cpn){
		//global tokens will never be digested
		//testedTokens.addAll(newlyTokens);
		//newlyTokens.clear();
	}
	@Override
	public String toString() {
		return "GP ["+"id=" + id + ", newlyToken=" + newlyToken //+ ", futureTokens=" + futureTokens 
				+  ", name=" + name + ", placeStrategy=" + placeStrategy + ", outArcs=" + outArcs
				+ ", type=" + type + "]";
	}
	@Override
	public boolean hasNew() {
		futureTokens.stream().filter(t->t.getTime()<=GlobalClock.getInstance().getTime())
		 .forEach(t->newlyToken.addToken(t));
		futureTokens=futureTokens.stream().filter(t->t.getTime()>GlobalClock.getInstance().getTime()).collect(Collectors.toList());
		return false;
	}
	public GlobalPlaceColor<IColor> getNewlyToken() {
		return newlyToken;
	}

	public List<IColor> getFutureTokens() {
		logger.error("please check your code to disable calling getFutureTokens for GlobalPlaceInstance");
		return Collections.emptyList();
	}
	public void setFutureTokens(List<IColor> futureTokens) {
//		this.futureTokens = futureTokens;
		logger.warn("a bug! please check your code to disable calling setFutureTokens for GlobalPlaceInstance");
	}
	
	@Override
	public void removeTokenFromTest(IColor token) {
		logger.warn("shouldn't call removeTokenFromTest of global place instance, please optimize your code");
	}

	@Override
	public List<IColor> getNewlyTokens() {
		logger.warn("shouldn't call getNewlyTokens of global place instance, please optimize your code");
		return Collections.singletonList(newlyToken);
	}

	@Override
	public void setNewlyTokens(List<IColor> newlyTokens) {
		logger.warn("shouldn't call setNewlyTokens of global place instance, please optimize your code");
	}

	@Override
	public List<IColor> getTestedTokens() {
		logger.warn("shouldn't call getTestedTokens of global place instance, please optimize your code");
		return Collections.emptyList();
	}

	@Override
	public void setTestedTokens(List<IColor> testedTokens) {
		logger.warn("shouldn't call getTestedTokens of global place instance, please optimize your code");
		
	}
}
