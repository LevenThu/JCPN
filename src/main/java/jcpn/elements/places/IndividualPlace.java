package jcpn.elements.places;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jcpn.elements.colorsets.IColor;

public class IndividualPlace extends Place {
	private Map<IOwner,List<IColor>> initialTokens;
	
	public IndividualPlace(int id, String name) {
		super(id, name);
		this.setType(PlaceType.IndividualPlace);
	}

	public Map<IOwner,List<IColor>> getInitialTokens() {
		return initialTokens;
	}

	public void setInitialTokens(Map<IOwner,List<IColor>> initialTokens) {
		this.initialTokens = initialTokens;
	}

	public IndividualPlace addInitToken(IOwner owner, IColor token){
		if(initialTokens==null){
			initialTokens=new HashMap<>();
		}
		if(!initialTokens.containsKey(owner)){
			initialTokens.put(owner, new ArrayList<>());
		}
		initialTokens.get(owner).add(token);
		return this;
	}
	


	
}
