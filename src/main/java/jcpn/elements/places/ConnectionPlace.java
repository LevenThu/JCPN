package jcpn.elements.places;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jcpn.elements.Pair;
import jcpn.elements.colorsets.IColor;

public class ConnectionPlace extends Place {
	private static Logger logger=LogManager.getLogger();
	
	public ConnectionPlace(int id, String name) {
		super(id, name);
		this.setType(PlaceType.ConnectionPlace);
	}

	
	public Map<Pair<IOwner, ITarget>, List<IColor>> getInitialTokens() {
		return initialTokens;
	}


	public void setInitialTokens(Map<Pair<IOwner, ITarget>, List<IColor>> initialTokens) {
		this.initialTokens = initialTokens;
	}


	private Map<Pair<IOwner, ITarget>, List<IColor>> initialTokens;
	public ConnectionPlace addInitToken(IOwner owner,ITarget target, IColor token){
		if(initialTokens==null){
			initialTokens=new HashMap<>();
		}
		Pair<IOwner, ITarget> pair=new Pair<>(owner, target);
		if(!initialTokens.containsKey(owner)){
			initialTokens.put(pair, new ArrayList<>());
		}
		initialTokens.get(pair).add(token);
		return this;
	}
	
	public Place addOutput(Integer transitionId, int number){
		if(outArcs==null) outArcs=new HashMap<>();
		if(!outArcs.isEmpty()){
			logger.warn("connection place only support one output arc, will and the new one and cover others");
			outArcs.clear();
		}
		outArcs.put(transitionId, number);
		return this;
	}
	
}
