package jcpn.elements.transions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import jcpn.elements.CPNet;
import jcpn.elements.Pair;
import jcpn.elements.colorsets.IColor;
import jcpn.elements.places.GlobalPlace;
import jcpn.elements.transions.instance.IOutputTokenBinding;
import jcpn.elements.transions.instance.MixedInputTokenBinding;

public abstract class Transition {
	@Override
	public String toString() {
		return "Transition [id=" + id + ", type=" + type + ", name=" + name + ", outArcs=" + outArcs + ", inArcs="
				+ inArcs + ", biConditions=" + biConditions + ", siConditions=" + siConditions
				+ ", freeFromBiConditions=" + freeFromBiConditions + ", constraintByBiConditions="
				+ Arrays.toString(constraintByBiConditions) + "]";
	}

	public Transition(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	int id;
	
	TransitionType type;
	int priority=500;
	public static enum TransitionType{
		IndividualTransition, ConnectionTransition
	}

	String name;	
	/**
	 * place id -> number of required tokens
	 */
	Map<Integer,Integer> outArcs=new HashMap<>();
	/**
	 * inArcs can be filled automatically by using CPNet.fill().
	 */
	Map<Integer, Integer> inArcs=new HashMap<>();
	
	Map<Pair<Integer, Integer>,Predicate<Pair<IColor, IColor>> > biConditions;
	Map<Integer,Predicate< IColor> > siConditions;
	Set<Integer> freeFromBiConditions;
	//this is a set of place id: imply that which input place has a biconstraint (i.e., the place appears in one BI guard function)
	Integer[] constraintByBiConditions;

	public Map<Integer,Integer> getOutArcs() {
		return outArcs;
	}

	public void setOutArcs(Map<Integer,Integer> outArcs) {
		this.outArcs = outArcs;
	}
	public Transition addOutput(Integer placeId, int number){
		if(outArcs==null) outArcs=new HashMap<>();
		outArcs.put(placeId, number);
		return this;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



	

	public TransitionType getType() {
		return type;
	}

	public void setType(TransitionType type) {
		this.type = type;
	}

	public Map<Integer, Integer> getInArcs() {
		return inArcs;
	}

	public void setInArcs(Map<Integer, Integer> inArcs) {
		this.inArcs = inArcs;
	}

	public void addInArc(int id, int number) {
		inArcs.put(id, number);
	}

	public Map<Pair<Integer, Integer>, Predicate<Pair<IColor, IColor>> > getBiConditions() {
		return biConditions;
	}

	public void setBiConditions(Map<Pair<Integer, Integer>, Predicate<Pair<IColor, IColor>> > biConditions) {
		this.biConditions = biConditions;
	}

	public Map<Integer, Predicate< IColor>> getSiConditions() {
		return siConditions;
	}

	public void setSiConditions(Map<Integer, Predicate< IColor>> siConditions) {
		this.siConditions = siConditions;
	}
	public void addSiCondition(Integer place,Predicate< IColor> unit){
		if(this.siConditions==null) siConditions=new HashMap<>();
		siConditions.put(place, unit);
	}
	public void addBiCondition(Pair<Integer, Integer> placePair,Predicate<Pair<IColor, IColor>>  unit){
		if(this.biConditions==null) biConditions=new HashMap<>();
		biConditions.put(placePair, unit);
	}
	public void addBiCondition(Integer left, Integer right,Predicate<Pair<IColor, IColor>>  unit){
		if(this.biConditions==null) biConditions=new HashMap<>();
		biConditions.put(new Pair<Integer,Integer>(left,right), unit);
	}
	

	//寻找没有约束条件的place
	/**
	 * find freedom places, constraint places,
	 * <br> before call this method, make sure that inArcs has been initialized. (CPNet.fill)
	 * 
	 */
	public void preCompile(CPNet net){
		freeFromBiConditions=new HashSet<>(inArcs.keySet());
		if(biConditions!=null)
		freeFromBiConditions.removeAll(
				biConditions.keySet().stream()
				.collect(ArrayList<Integer>::new, (list,unit)->{list.add(unit.getLeft());;list.add(unit.getRight());}, ArrayList::addAll)
				);
//		freeFromBiConditions.removeAll(siConditions.keySet().stream().collect(Collectors.toList()));
		Set<Integer> constraints=new HashSet<>(inArcs.keySet());
		constraints.removeAll(freeFromBiConditions);
		freeFromBiConditions=freeFromBiConditions.stream().filter(pid->! (net.getPlace(pid) instanceof GlobalPlace)).collect(Collectors.toSet());
		this.constraintByBiConditions=constraints.toArray(new Integer[]{});
	}

	public Set<Integer> getFreeFromBiConditions() {
		return freeFromBiConditions;
	}

	public void setFreeFromBiConditions(Set<Integer> freeFromBiConditions) {
		this.freeFromBiConditions = freeFromBiConditions;
	}

	public Integer[] getConstraintByBiConditions() {
		return constraintByBiConditions;
	}

	public void setConstraintByBiConditions(Integer[] constraintByBiConditions) {
		this.constraintByBiConditions = constraintByBiConditions;
	}
	Function<MixedInputTokenBinding, IOutputTokenBinding> outputFunction;
//	Function<MixedInputTokenBinding,Long> timeCostFunction;
	public Function<MixedInputTokenBinding, IOutputTokenBinding> getOutputFunction() {
		return outputFunction;
	}
	/**
	 * 
	 * @param outputFunction notice the time cost is relative time rather than absolute time
	 */
	public void setOutputFunction(Function<MixedInputTokenBinding, IOutputTokenBinding> outputFunction) {
		this.outputFunction = outputFunction;
	}
//	public Function<MixedInputTokenBinding, Long> getTimeCostFunction() {
//		return timeCostFunction;
//	}
//	public void setTimeCostFunction(Function<MixedInputTokenBinding, Long> timeCostFunction) {
//		this.timeCostFunction = timeCostFunction;
//	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

}
