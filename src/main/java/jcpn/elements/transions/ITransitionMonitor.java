package jcpn.elements.transions;

import jcpn.elements.places.IOwner;
import jcpn.elements.transions.instance.IOutputTokenBinding;
import jcpn.elements.transions.instance.MixedInputTokenBinding;

public interface ITransitionMonitor {
	public void reportWhenFired(IOwner owner, String transitionName, int transitionId,MixedInputTokenBinding inputs, IOutputTokenBinding outputTokenBinding);
	
}
