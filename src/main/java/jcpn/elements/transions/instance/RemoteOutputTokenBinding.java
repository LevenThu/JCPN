package jcpn.elements.transions.instance;

import java.util.List;
import java.util.Map;

import jcpn.elements.colorsets.IColor;
import jcpn.elements.places.ITarget;

/**
 * if the output is locally (np->np, or np->ctp), then simpleTokens store the output np tokens, connection Token store the output ctp tokens. connectionPid stores the output ctp id (only one).  Only one time cost
 * <br>
 * if the output is remotely (CTT->np), one target, a map tokens for remote, a map tokens for locally. Two time costs.
 * @author hxd
 *
 */
public class RemoteOutputTokenBinding implements IOutputTokenBinding{
	@Override
	public String toString() {
		return "ROutB [target=" + target + ", localEffective=" + localEffective + ", remoteEffective=" + remoteEffective
				+ ", localTokensNP=" + localTokensForNP + ", localTokensConnection=" + localTokensForConnection 
				 + ", remoteTokens=" + remoteTokens 
				+ ", hasLocal=" + hasLocalForNP + ", hasRemote=" + hasRemote + "]";
	}
	ITarget target;
	Map<Integer, List<IColor>> localTokensForNP;
	Map<Integer, Map<ITarget, List<IColor>>> localTokensForConnection;
	long localEffective;

	Map<Integer, List<IColor>> remoteTokens;
	long remoteEffective;

	boolean hasLocalForNP=false;
	boolean hasRemote=false;
	boolean hasLocalForConnection=false;
	private RemoteOutputTokenBinding(ITarget target, 
			Map<Integer, List<IColor>> localTokensForNP,
			Map<Integer, Map<ITarget, List<IColor>>> localTokensForConnection,
			long localEffective,
			Map<Integer, List<IColor>> remoteTokens, long remoteEffective) {
		super();
		this.target = target;
		this.localTokensForNP = localTokensForNP;
		this.localTokensForConnection=localTokensForConnection;
		this.localEffective = localEffective;
		this.remoteTokens = remoteTokens;
		this.remoteEffective = remoteEffective;
		this.hasLocalForNP=localTokensForNP!=null&&localTokensForNP.size()>0;
		this.hasLocalForConnection=localTokensForConnection!=null&&localTokensForConnection.size()>0;
		this.hasRemote=remoteTokens!=null&&remoteTokens.size()>0;
	}
	public static RemoteOutputTokenBinding localBinding(Map<Integer, List<IColor>> localTokensForNP, Map<Integer, Map<ITarget, List<IColor>>> localTokensForConnection, long localEffective){
		return new RemoteOutputTokenBinding(null, localTokensForNP,localTokensForConnection, localEffective, null, 0);
	}
	public static RemoteOutputTokenBinding remoteBinding(Map<Integer, List<IColor>> remoteTokens, long remoteEffective, ITarget target){
		return new RemoteOutputTokenBinding(target, null,null, 0, remoteTokens, remoteEffective);
	}
	public static RemoteOutputTokenBinding mixBinding(ITarget target, Map<Integer, List<IColor>> localTokensForNP, Map<Integer, Map<ITarget, List<IColor>>> localTokensForConnection, long localEffective,
			Map<Integer, List<IColor>> remoteTokens, long remoteEffective){
		return new RemoteOutputTokenBinding(target, localTokensForNP,localTokensForConnection, localEffective, remoteTokens, remoteEffective);				
	}
	@Override
	public boolean islocal() {
		return false;
	}
	@Override
	public boolean isRemote() {
		return true;
	}
	public ITarget getTarget() {
		return target;
	}
	public void setTarget(ITarget target) {
		this.target = target;
	}
	


	public void setLocalEffective(long localEffective) {
		this.localEffective = localEffective;
	}
	public Map<Integer, List<IColor>> getRemoteTokens() {
		return remoteTokens;
	}

	public long getRemoteEffective() {
		return remoteEffective;
	}
	public void setRemoteEffective(long remoteEffective) {
		this.remoteEffective = remoteEffective;
	}

	@Override
	public boolean hasLocalForNP() {
		return hasLocalForNP;
	}
	@Override
	public boolean hasLocalForConnection() {
		return hasLocalForConnection;
	}
	@Override
	public Map<Integer, List<IColor>> getLocalForNP() {
		return localTokensForNP;
	}

	@Override
	public long getLocalEffective() {
		return localEffective;
	}
	@Override
	public Map<Integer, Map<ITarget, List<IColor>>> getLocalForConnection() {
		return localTokensForConnection;
	}
	@Override
	public Map<Integer, List<IColor>> getRemoteForNP() {
		return remoteTokens;
	}
	@Override
	public long getTargetEffective() {
		return remoteEffective;
	}

	
}
