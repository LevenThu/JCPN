package jcpn.elements.transions.instance;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jcpn.elements.Pair;
import jcpn.elements.colorsets.IColor;
import jcpn.elements.places.IOwner;
import jcpn.elements.places.Place.PlaceStrategy;
import jcpn.elements.places.Place.PlaceType;
import jcpn.elements.places.instance.GlobalPlaceInstance;
import jcpn.elements.places.instance.ISimpleTokenOrganizater;
import jcpn.elements.places.instance.IndividualPlaceInstance;
import jcpn.elements.places.instance.PlaceInstance;
import jcpn.elements.transions.Transition;
import jcpn.runtime.IndividualCPNInstance;

public class IndividualTransitionInstance extends TransitionInstance {
	private static Logger logger=LogManager.getLogger();
	@Override
	public String toString() {
		return "IT [cachedSiBindings=" + cachedSiBindings + ", cachedBiIndex=" + cachedBiIndex
				+ ", cachedBiBindings=" + cachedBiBindings + ", owner=" + owner + ", id=" + id + ", name=" + name
				+ ", inArcs=" + inArcs + ", freeFromBiConditions=" + freeFromBiConditions
				+ ", constraintByBiConditions=" + Arrays.toString(constraintByBiConditions) + "]";
	}

	Map<Integer,List<IColor>> cachedSiBindings;
	Map<Integer, Set<Pair<Integer,Integer>>> cachedBiIndex;
	Map<Pair<Integer,Integer>, List<Pair<IColor,IColor>>> cachedBiBindings;


	/** for test*/
	Map<Integer, List<IColor>> getCachedSiBindings() {
		return cachedSiBindings;
	}
	/** for test*/
	Map<Integer, Set<Pair<Integer, Integer>>> getCachedBiIndex() {
		return cachedBiIndex;
	}
	/** for test*/
	Map<Pair<Integer, Integer>, List<Pair<IColor, IColor>>> getCachedBiBindings() {
		return cachedBiBindings;
	}

	public IndividualTransitionInstance(Transition transition,IOwner owner,IndividualCPNInstance cpn) {
		super(transition,owner,cpn);
	}

	@Override
	protected void initCache(IndividualCPNInstance cpn) {
		//reCache(this.freeFromBiConditions, this.biConditions.keySet());
		cachedSiBindings=new HashMap<>();
		for(Integer unit:freeFromBiConditions){
			//TODO  we can optimize the speed by skipping cache freed place
			//if(!cpn.getPlace(unit).isFreedom())
			cachedSiBindings.put(unit, new LinkedList<>());
		}
		if(biConditions!=null&&biConditions.size()>0){
			cachedBiIndex=new HashMap<>();
			cachedBiBindings=new HashMap<>();
			for(Pair<Integer,Integer> pair:biConditions.keySet()){
				cachedBiBindings.put(pair, new LinkedList<>());
				if(cachedBiIndex.get(pair.getLeft())==null){
					cachedBiIndex.put(pair.getLeft(), new HashSet<>());
				}
				cachedBiIndex.get(pair.getLeft()).add(pair);
				if(cachedBiIndex.get(pair.getRight())==null){
					cachedBiIndex.put(pair.getRight(), new HashSet<>());
				}
				cachedBiIndex.get(pair.getRight()).add(pair);
			}
		}

	}

	@Override
	public void checkNewTokens4Firing(IndividualCPNInstance cpnInstance){
		Map<Integer, PlaceInstance> places=cpnInstance.getPlaces();
		Predicate<IColor> siUnit=null;
		Random random=new Random();
		for(Integer pid:freeFromBiConditions){
			if(places.get(pid).hasNew()){//global place never return true.
				IndividualPlaceInstance p1=(IndividualPlaceInstance) places.get(pid);
				List<IColor> existedTokens=cachedSiBindings.get(p1.getId());
				if(siConditions!=null && (siUnit=siConditions.get(p1.getId()))!=null){
					if(p1.getPlaceStrategy().equals(PlaceStrategy.FIFO)){
						existedTokens.addAll(p1.getNewlyTokens().stream()
								.filter(siUnit).collect(Collectors.toList())// filter that do not fulfill single condition candidates.
								);
					}else if(p1.getPlaceStrategy().equals(PlaceStrategy.BAG)){
						(p1).getNewlyTokens().stream()
						.filter(siUnit)// filter that do not fulfill single condition candidates.
						.forEach(t->existedTokens.add(random.nextInt(existedTokens.size()+1), t));
					}
					//individual transition does not allow connection place as its input arc
				}else{
					if(p1.getPlaceStrategy().equals(PlaceStrategy.FIFO)){
						existedTokens.addAll(((IndividualPlaceInstance) p1).getNewlyTokens()
								.stream().collect(Collectors.toList())
								);
					}else if(p1.getPlaceStrategy().equals(PlaceStrategy.BAG)){
						(p1).getNewlyTokens()
						.stream().forEach(t->existedTokens.add(random.nextInt(existedTokens.size()+1), t));
					}
				}
			}
		}
		if(biConditions!=null){			
			for(int i=0;i<constraintByBiConditions.length;i++){
				if(places.get(constraintByBiConditions[i]).hasNew()){
					PlaceInstance p1=places.get(constraintByBiConditions[i]);
					for(int j=0;j<i;j++){//only scan tested
						PlaceInstance p2=places.get(constraintByBiConditions[j]);
						checkAPair(p1, p2, p-> p.getTestedTokens().stream());
					}
					for(int j=i+1;j<constraintByBiConditions.length;j++){//scan both current and tested
						PlaceInstance p2=places.get(constraintByBiConditions[j]);
						checkAPair(p1, p2, p-> Stream.concat(p.getNewlyTokens().stream(), p.getTestedTokens().stream()));
					}
				}
			}
			//Then, we merge the results to exclude the conflicted binding
			cachedBiIndex.entrySet().stream()
			.forEach(entry->{
				Integer pid=entry.getKey();
				for(Pair<Integer, Integer> pair:entry.getValue()){
					Set<IColor> values=this.cachedBiBindings.get(pair).stream()//list<TokenPair>
							.map(tokenPair-> ( pair).getLeft().equals(pid)? tokenPair.getLeft():tokenPair.getRight())// extract out pid's candidate values.
							.collect(Collectors.toSet());
					if(values.size()==0){
						entry.getValue().stream()
						.forEach(pair2->{
							this.cachedBiBindings.get(pair2).clear();
						});
						//we can end the forEach now. because all the data has been cleared.
						break;
					}else{
						for(Pair<Integer, Integer> pair2:entry.getValue()){
							if(!pair2.equals(pair)){
								this.cachedBiBindings.put(pair2, 
										this.cachedBiBindings.get(pair2).stream()//list<TokenPair>
										.filter(tokenPair->(pair2).getLeft().equals(pid)? values.contains(tokenPair.getLeft()):values.contains(tokenPair.getRight())).collect(Collectors.toList())
										);
								//because pair2 may have been looped in higher level loop, we need to kill all data again.
								if(this.cachedBiBindings.get(pair2).size()==0){
									entry.getValue().stream()
									.forEach(pair3->{
										this.cachedBiBindings.get(pair3).clear();
									});
									break;//though we only jump this loop, the higher level loop will stop quickly.
								}
							}
						}
					}
				}
			});
		}

	}
	/**
	 * choose tokens pair from p1 and p2 which satisfy the predicater of p1 and p2,  
	 * and each token of p1 or p2 also satisfy the single predicater, if exists.
	 * @param p1
	 * @param p2
	 * @param streamFunction
	 */
	private <T> void checkAPair(PlaceInstance p1,PlaceInstance p2,
			Function<ISimpleTokenOrganizater, Stream<IColor>> streamFunction){
		//individual transition does not allow connection place as its input arc
		Pair<Integer,Integer> pair=new Pair<>(p1.getId(), p2.getId());
		if((biConditions.get(pair))!=null){
			checkAPair(pair,p1,p2, streamFunction);
		}
		pair=new Pair<>(p2.getId(), p1.getId());
		if((biConditions.get(pair))!=null) {
			checkAPair(pair,p2,p1, streamFunction);
		}
	}
	private <T> void checkAPair(Pair<Integer, Integer> pair,PlaceInstance left,PlaceInstance right,
			Function<ISimpleTokenOrganizater, Stream<IColor>> streamFunction){
		//individual transition does not allow connection place as its input arc
		Predicate<Pair<IColor, IColor>> biUnit=biConditions.get(pair);
		List<Pair<IColor, IColor>> existed=cachedBiBindings.get(pair);
		if(left.getPlaceStrategy().equals(PlaceStrategy.FIFO)&&right.getPlaceStrategy().equals(PlaceStrategy.BAG)){
			addIntoExistedBiCache(existed, left, right, true, streamFunction, biConditions.get(pair));
		}else if(left.getPlaceStrategy().equals(PlaceStrategy.BAG)&&right.getPlaceStrategy().equals(PlaceStrategy.FIFO)){
			addIntoExistedBiCache(existed, right, left, false, streamFunction, biConditions.get(pair));
		}else if(left.getPlaceStrategy().equals(PlaceStrategy.FIFO)&&right.getPlaceStrategy().equals(PlaceStrategy.FIFO)){
			cachedBiBindings.get(pair).addAll(
					generateBiStream(left, right, true, streamFunction, biUnit)
					.collect(Collectors.toList())
					);	
		}else if(left.getPlaceStrategy().equals(PlaceStrategy.BAG)&&right.getPlaceStrategy().equals(PlaceStrategy.BAG)){
			Random random=new Random();
			generateBiStream(left, right, true, streamFunction, biUnit)
			.forEach(tokenPair->{
				existed.add(random.nextInt(existed.size()+1),tokenPair);
			});	
		}

		//		List<Pair<IColor, IColor>> existed=cachedBiBindings.get(pair);


	}
	private void addIntoExistedBiCache(List<Pair<IColor, IColor>> existedCache, PlaceInstance fifoPlace, PlaceInstance bagPlace, boolean fifoIsLeft,
			Function<ISimpleTokenOrganizater, Stream<IColor>> streamFunction,Predicate<Pair<IColor, IColor>> biUnit){
		Map<IColor, List<Pair<IColor, IColor>>> result=
				generateBiStream(fifoPlace, bagPlace, fifoIsLeft, streamFunction, biUnit)
				.collect(Collectors.groupingBy(tokenPair->fifoIsLeft?tokenPair.getLeft():tokenPair.getRight()));

		((ISimpleTokenOrganizater) fifoPlace).getNewlyTokens().stream()
		.filter( t1->  siConditions==null || siConditions.get(fifoPlace.getId())==null || siConditions.get(fifoPlace.getId()).test(t1))//filter tokens which do not meet the single condition
		.forEach(t1->{
			Collections.shuffle(result.get(t1));
			existedCache.addAll(result.get(t1));
		});
	}
	private Stream<Pair<IColor, IColor>> generateBiStream( PlaceInstance place1, PlaceInstance place2, boolean firstIsLeft,
			Function<ISimpleTokenOrganizater, Stream<IColor>> streamFunction,Predicate<Pair<IColor, IColor>> biUnit){
		return ((ISimpleTokenOrganizater) place1).getNewlyTokens().stream()
				.filter( t1->  siConditions==null || siConditions.get(place1.getId())==null || siConditions.get(place1.getId()).test(t1))//filter tokens which do not meet the single condition
				.flatMap(t1->
				streamFunction.apply((ISimpleTokenOrganizater) place2)
				.filter( t2-> ( siConditions==null ||  siConditions.get(place2.getId())==null ||  siConditions.get(place2.getId()).test(t2)) && biUnit.test(firstIsLeft? new Pair<>(t1, t2):new Pair<>(t2, t1)))
				.map(t2 -> firstIsLeft? new Pair<>(t1, t2):new Pair<>(t2, t1))
						);
	}


	@Override
	public <T> void removeTokenFromCache(PlaceInstance place, IColor token){
		if(place.getType().equals(PlaceType.GlobalPlace)){
			logger.warn("you should not call removeTokenFromCache method, if the place is a globalPlace");
			return;
		}
		if( cachedSiBindings.get(place.getId())!=null){
			cachedSiBindings.get(place.getId()).remove(token);
		}
		if(cachedBiIndex!=null&&cachedBiIndex.get(place.getId())!=null){
			cachedBiIndex.get(place.getId()).stream()
			.forEach(pair->{
				cachedBiBindings.get(pair).removeAll(
						cachedBiBindings.get(pair).stream()
						.filter(tokenPair->
						place.getId().equals(pair.getLeft())? tokenPair.getLeft().equals(token):tokenPair.getRight().equals(token)
								).collect(Collectors.toList())
						);
			});
		}
	}
	@Override
	public boolean canFire(IndividualCPNInstance cpnInstance){
		if(this.getInArcs().size()==0){
			return true;
		}
		for(Integer key:cachedSiBindings.keySet()){
			if(cachedSiBindings.get(key).size()==0){
				return false;
			}
		}
		if(cachedBiBindings!=null)
			for(Pair<Integer, Integer> pair:cachedBiBindings.keySet()){
				if(cachedBiBindings.get(pair).size()==0){
					return false;
				}
			}
		//TODO  optimization: check those freedom data. 
		return true;
	}
	/**
	 * 
	 * @param cpnInstance
	 * @return
	 */
	@Override
	synchronized public MixedInputTokenBinding randomBinding(IndividualCPNInstance cpnInstance){
		Map<Integer, IColor> binding=getFirstBinding(cpnInstance);
		return MixedInputTokenBinding.simpleBinding(this.getOwner(),binding);
	}


	private Map<Integer, IColor> getFirstBinding(IndividualCPNInstance cpnInstance){
		Map<Integer, IColor> binding=new HashMap<>();
		if(inArcs==null)
			return binding;
		if(cachedBiBindings!=null){
			for(Pair<Integer, Integer> pair:cachedBiBindings.keySet()){
				if(binding.containsKey(pair.getLeft()) && binding.containsKey(pair.getRight())){
					continue;
				}
				else if(binding.containsKey(pair.getLeft())){
					for( Pair<IColor,IColor> tokenPair:cachedBiBindings.get(pair)){
						if(tokenPair.getLeft().equals(binding.get(pair.getLeft()))){
							binding.put(pair.getRight(), tokenPair.getRight());
							break;
						}
					}
				}else if(binding.containsKey(pair.getRight())){
					for( Pair<IColor,IColor> tokenPair:cachedBiBindings.get(pair)){
						if(tokenPair.getRight().equals(binding.get(pair.getRight()))){
							binding.put(pair.getLeft(), tokenPair.getLeft());
							break;
						}
					}
				}else{
					Pair<IColor,IColor> pair2=cachedBiBindings.get(pair).get(0);
					binding.put(pair.getLeft(), pair2.getLeft());
					binding.put(pair.getRight(), pair2.getRight());
				}
			}
		}
		if(cachedSiBindings!=null)
			for(Integer pid:this.cachedSiBindings.keySet()){
				//			PlaceInstance placeInstance=cpnInstance.getPlaces().get(pid);
				binding.put(pid, cachedSiBindings.get(pid).get(0));
			}
		for(Integer pid: this.inArcs.keySet()){
			if(cpnInstance.getPlace(pid) instanceof GlobalPlaceInstance){
				binding.put(pid, ((GlobalPlaceInstance)cpnInstance.getPlace(pid)).getNewlyToken());
			}
		}
		//optimization: get those freedom data
		//TODO
		return binding;
	}



}
