package jcpn.elements.transions.instance;

import java.util.List;
import java.util.Map;

import jcpn.elements.colorsets.IColor;
import jcpn.elements.places.ITarget;

/**
 * if the output is locally (np->np, or np->ctp), then simpleTokens store the output np tokens, connection Token store the output ctp tokens. connectionPid stores the output ctp id (only one).  Only one time cost
 * <br>
 * if the output is remotely (CTT->np), one target, a map tokens for remote, a map tokens for locally. Two time costs.
 * @author hxd
 *
 */
public class LocalOutputTokenBinding implements IOutputTokenBinding{
	@Override
	public String toString() {
		return "LOutB [effective=" + effective + ", simpleTokens=" + simpleTokens 
				+ ", connectionTokens=" + connectionTokens + ", hasConnection=" + hasConnection + ", hasSimple="
				+ hasSimple + "]";
	}
	Map<Integer, List<IColor>> simpleTokens;
	long effective=0;// effective time
	/**
	 * connection place
	 */
	//int connectionPid;//connection place
	Map<Integer, Map<ITarget, List<IColor>>> connectionTokens;//connectionPid-> target->tokens
	
	boolean hasConnection=false;
	boolean hasSimple=false;
	
	public LocalOutputTokenBinding(Map<Integer, List<IColor>> simpleTokens, long effective, 
			Map<Integer, Map<ITarget, List<IColor>>> connectionTokens) {
		super();
		this.simpleTokens = simpleTokens;
		this.effective = effective;
		this.connectionTokens = connectionTokens;
		this.hasSimple=simpleTokens!=null;
		this.hasConnection=connectionTokens!=null&&connectionTokens.size()>0;
	}
	public static LocalOutputTokenBinding simpleBinding(Map<Integer, List<IColor>> simpleTokens, long effective){
		return new LocalOutputTokenBinding(simpleTokens,effective,null);
	}
	public static LocalOutputTokenBinding connectionBinding(long effective,
			Map<Integer, Map<ITarget, List<IColor>>> connectionTokens){
		return new LocalOutputTokenBinding(null,effective,connectionTokens);
	}
	public static LocalOutputTokenBinding mixBinding(Map<Integer, List<IColor>> simpleTokens, long effective, 
			Map<Integer, Map<ITarget, List<IColor>>> connectionTokens){
		return new LocalOutputTokenBinding(simpleTokens,effective,connectionTokens);
	}
	@Override
	public boolean islocal() {
		return true;
	}
	@Override
	public boolean isRemote() {
		return false;
	}
	public Map<Integer, List<IColor>> getSimpleTokens() {
		return simpleTokens;
	}

	public long getEffective() {
		return effective;
	}
	public void setEffective(long effective) {
		this.effective = effective;
	}
	
	public Map<Integer, Map<ITarget, List<IColor>>> getConnectionTokens() {
		return connectionTokens;
	}


	public void setHasSimple(boolean hasSimple) {
		this.hasSimple = hasSimple;
	}
	@Override
	public boolean hasLocalForNP() {
		return hasSimple;
	}
	@Override
	public boolean hasLocalForConnection() {
		return hasConnection;
	}
	@Override
	public Map<Integer, List<IColor>> getLocalForNP() {
		return simpleTokens;
	}
	@Override
	public long getLocalEffective() {
		return effective;
	}

	@Override
	public Map<Integer, Map<ITarget, List<IColor>>> getLocalForConnection() {
		return connectionTokens;
	}
	@Override
	public Map<Integer, List<IColor>> getRemoteForNP() {
		return null;
	}
	@Override
	public ITarget getTarget() {
		return null;
	}
	@Override
	public long getTargetEffective() {
		return 0;
	}

	
}
