package jcpn.elements.transions.instance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jcpn.elements.Pair;
import jcpn.elements.colorsets.IColor;
import jcpn.elements.places.IOwner;
import jcpn.elements.places.ITarget;
import jcpn.elements.places.Place.PlaceType;
import jcpn.elements.places.instance.ConnectionPlaceInstance;
import jcpn.elements.places.instance.GlobalPlaceInstance;
import jcpn.elements.places.instance.PlaceInstance;
import jcpn.elements.transions.IndividualTransition;
import jcpn.elements.transions.Transition;
import jcpn.runtime.IndividualCPNInstance;

public class ConnectionTransitionInstance extends TransitionInstance {
	private static Logger logger=LogManager.getLogger();
	


	Set<ITarget> targets;

	public ConnectionTransitionInstance(Transition transition, IOwner owner, Set<ITarget> targets,IndividualCPNInstance cpn) {
		super(transition, owner,cpn);
		this.targets=targets;
		preCompile(cpn);
	}


	/**
	 * use a simple Transition to handle that NP type places and their conditions.
	 */
	private IndividualTransitionInstance simpleTransition;


	private Map<Integer, Integer> simpleInArcs=new HashMap<>();
	private Map<Integer, Integer> connectionInArcs=new HashMap<>();

	private Map<Pair<Integer, Integer>,Predicate<Pair<IColor, IColor>> > simpleBiConditions;
	private Map<Integer,Predicate< IColor> > simpleSiConditions;
	private Map<Pair<Integer, Integer>,Predicate<Pair<IColor, IColor>> > connectionBiConditions;
	private Map<Integer,Predicate< IColor> > connectionSiConditions;

	private Set<Integer> simpleFreeFromBiConditions;
	private  Integer[] simpleConstraintByBiConditions;

	private Set<Integer> connectionFreeFromBiConditions;
	private  Integer[] connectionConstraintByBiConditions;

	private void preCompile(IndividualCPNInstance cpn) {
		inArcs.forEach((pid,number)->{
			if(cpn.getPlace(pid).getType().equals(PlaceType.ConnectionPlace)) 
				connectionInArcs.put(pid, number);
			else
				simpleInArcs.put(pid, number);
		});
		if(siConditions!=null)
		siConditions.forEach((pid,perdicter)->{
			if(cpn.getPlace(pid).getType().equals(PlaceType.ConnectionPlace)) {
				if(connectionSiConditions==null) connectionSiConditions=new HashMap<>();
				connectionSiConditions.put(pid, perdicter);
			}
			else{
				if(simpleSiConditions==null)simpleSiConditions=new HashMap<>();
				simpleSiConditions.put(pid, perdicter);
			}
		});
		if(biConditions!=null)
		biConditions.forEach((pair,predicator)->{
			if(cpn.getPlace(pair.getLeft()).getType().equals(PlaceType.ConnectionPlace)
					&&cpn.getPlace(pair.getRight()).getType().equals(PlaceType.ConnectionPlace)){
				if(connectionBiConditions==null) connectionBiConditions=new HashMap<>();
				connectionBiConditions.put(pair, predicator);
			}else if((cpn.getPlace(pair.getLeft()).getType().equals(PlaceType.ConnectionPlace)
					&&!cpn.getPlace(pair.getRight()).getType().equals(PlaceType.ConnectionPlace))
					||(!cpn.getPlace(pair.getLeft()).getType().equals(PlaceType.ConnectionPlace)
							&&cpn.getPlace(pair.getRight()).getType().equals(PlaceType.ConnectionPlace))){
				logger.warn("current version does not support set bi-condition which one side is connectionPlace while the other side is not. We will omit these conditions in transion:"+this.name);
			}
			else{
				if(simpleBiConditions==null) simpleBiConditions=new HashMap<>();
				simpleBiConditions.put(pair, predicator);
			}
		});
		if(simpleInArcs.size()>0){
			simpleFreeFromBiConditions=new HashSet<>(simpleInArcs.keySet());
			this.simpleConstraintByBiConditions=computeFreeAndConstraint(simpleFreeFromBiConditions, simpleBiConditions, simpleInArcs,cpn);
		}
		if(connectionInArcs.size()>0){
			connectionFreeFromBiConditions=new HashSet<>(connectionInArcs.keySet());
			this.connectionConstraintByBiConditions=computeFreeAndConstraint(connectionFreeFromBiConditions, connectionBiConditions, connectionInArcs,cpn);
		}
		if(simpleInArcs.size()>0){
			IndividualTransition individualTransition=new IndividualTransition(this.id, this.name+"_simple");
			individualTransition.setBiConditions(simpleBiConditions);
			individualTransition.setConstraintByBiConditions(simpleConstraintByBiConditions);
			individualTransition.setFreeFromBiConditions(simpleFreeFromBiConditions);
			individualTransition.setInArcs(simpleInArcs);
			individualTransition.setSiConditions(simpleSiConditions);
			simpleTransition=new IndividualTransitionInstance(individualTransition, owner, cpn);
		}
		//then,	freeFromBiCOnditions, constraintByBiConditions are useless, we set them as null to help us find bugs ASAP
		this.freeFromBiConditions=null;
		this.connectionBiConditions=null;
	}
	/**
	 * 
	 * @param freeFromBiConditions notice, the content of this parameter will be modified
	 * @param biConditions
	 * @param inArcs
	 * @return constraintByBiConditions
	 */
	private Integer[] computeFreeAndConstraint(Set<Integer> freeFromBiConditions,
			Map<Pair<Integer, Integer>,Predicate<Pair<IColor, IColor>>> biConditions,
			Map<Integer, Integer> inArcs, IndividualCPNInstance cpn){
		if(biConditions!=null)
			freeFromBiConditions.removeAll(
				biConditions.keySet().stream()
				.collect(ArrayList<Integer>::new, (list,unit)->{list.add(unit.getLeft());;list.add(unit.getRight());}, ArrayList::addAll)
				);
		
		Set<Integer> constraints=inArcs==null?new HashSet<>():new HashSet<>(inArcs.keySet());
		constraints.removeAll(freeFromBiConditions);
		freeFromBiConditions.removeAll(freeFromBiConditions.stream()
				.filter(
						pid->(cpn.getPlace(pid) instanceof GlobalPlaceInstance))
				.collect(Collectors.toSet()));
		return constraints.toArray(new Integer[]{});
	}
	//	/**
	//	 * these parts are for the ISimpleTokenBindings places 
	//	 */
	//	Map<Integer,List<IColor>> cachedSimpleSiBindings=new HashMap<>();
	//	Map<Integer, Set<Pair<Integer,Integer>>> cachedSimpleBiIndex;
	//	Map<Pair<Integer,Integer>, List<Pair<IColor,IColor>>> cachedSimpleBiBindings;




	
	@Override
	protected void initCache(IndividualCPNInstance cpn) {
		//the part of simple transition has been initialized in the preCompile method
	}
	Map<ITarget, IndividualTargetTransitionInstance> targetIndividualTransitions=new HashMap<>();
	@Override
	public void checkNewTokens4Firing(IndividualCPNInstance cpnInstance) {
		if(simpleTransition!=null)
			simpleTransition.checkNewTokens4Firing(cpnInstance);
		connectionInArcs.forEach((pid,number)->{
			ConnectionPlaceInstance place=(ConnectionPlaceInstance) cpnInstance.getPlace(pid);
			place.getNewlyTokens().keySet().forEach(t->{
				targetIndividualTransitions.computeIfAbsent(t, target->constructTargetTransition(t,cpnInstance));
			});
		});
		targetIndividualTransitions.values().stream().forEach(t->t.checkNewTokens4Firing(cpnInstance));
	}
	private IndividualTargetTransitionInstance constructTargetTransition(ITarget target,IndividualCPNInstance cpn){
		IndividualTransition individualTransition=new IndividualTransition(this.id, this.name+"_"+target.getName());
		individualTransition.setBiConditions(connectionBiConditions);
		individualTransition.setConstraintByBiConditions(connectionConstraintByBiConditions);
		individualTransition.setFreeFromBiConditions(connectionFreeFromBiConditions);
		individualTransition.setInArcs(connectionInArcs);
		individualTransition.setSiConditions(connectionSiConditions);
		return new IndividualTargetTransitionInstance(individualTransition, owner,target, cpn);
	}
	
	@Override
	public boolean canFire(IndividualCPNInstance cpnInstance) {
		if(this.simpleTransition!=null&&!this.simpleTransition.canFire(cpnInstance))
			return false;
		if(this.targetIndividualTransitions.size()==0)
			return false;
		boolean canfile=false;
		for(IndividualTargetTransitionInstance transition:this.targetIndividualTransitions.values()){
			if(transition.canFire(cpnInstance)){
				canfile=true;
				break;
			}
		}
		return canfile;
	}
	
	Random random=new Random();
	@Override
	public MixedInputTokenBinding randomBinding(IndividualCPNInstance cpnInstance) {
		List<ITarget> targets=targetIndividualTransitions.entrySet().stream()
		.filter(entry->entry.getValue().canFire(cpnInstance))
		.map(entry->entry.getKey()).collect(Collectors.toList());
		
		ITarget selected=targets.get(random.nextInt(targets.size()));
		if(simpleInArcs.size()>0){
			 return MixedInputTokenBinding.mixBinding(
					 this.getOwner(),
					simpleTransition.randomBinding(cpnInstance).getSimpleTokens(),
					selected,
					targetIndividualTransitions.get(selected).randomBinding(cpnInstance).getSimpleTokens()
					);
		}else{
			return MixedInputTokenBinding.connectionBinding(
					this.getOwner(),
					selected,
					targetIndividualTransitions.get(selected).randomBinding(cpnInstance).getSimpleTokens()
					);		
		}
	}
	
	@Override
	public void removeTokenFromCache(PlaceInstance place, IColor token,ITarget target) {
		targetIndividualTransitions.get(target).removeTokenFromCache(place, token);
	}
	@Override
	public void removeTokenFromCache(PlaceInstance place, IColor token) {
		simpleTransition.removeTokenFromCache(place, token);
	}

}
