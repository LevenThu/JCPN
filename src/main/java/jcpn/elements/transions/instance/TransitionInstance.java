package jcpn.elements.transions.instance;

import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;

import jcpn.elements.Pair;
import jcpn.elements.colorsets.IColor;
import jcpn.elements.places.IOwner;
import jcpn.elements.places.ITarget;
import jcpn.elements.places.instance.PlaceInstance;
import jcpn.elements.transions.Transition;
import jcpn.runtime.GlobalClock;
import jcpn.runtime.IndividualCPNInstance;

public abstract class TransitionInstance {
	IOwner owner;
	int id;
	String name;
	Map<Pair<Integer, Integer>,Predicate<Pair<IColor, IColor>>> biConditions;
	Map<Integer, Predicate<IColor>> siConditions;
	int priority;

	Map<Integer, Integer> outArcs;
	Map<Integer, Integer> inArcs;
	public TransitionInstance(Transition transition, IOwner owner,IndividualCPNInstance cpn){
		this.owner=owner;
		this.id=transition.getId();
		this.name=transition.getName();
		this.biConditions=transition.getBiConditions();
		this.siConditions=transition.getSiConditions();
		this.outArcs=transition.getOutArcs();
		this.inArcs=transition.getInArcs();
		this.freeFromBiConditions=transition.getFreeFromBiConditions();
		this.constraintByBiConditions=transition.getConstraintByBiConditions();
//		this.timeCostFunction=transition.getTimeCostFunction();
		this.outputFunction=transition.getOutputFunction();
		this.priority=transition.getPriority();
		initCache(cpn);
	}
	public IOwner getOwner() {
		return owner;
	}
	public void setOwner(IOwner owner) {
		this.owner = owner;
	}
	public Map<Integer, Integer> getInArcs() {
		return inArcs;
	}
	public void setInArcs(Map<Integer, Integer> inArcs) {
		this.inArcs = inArcs;
	}
	public Map<Integer, Integer> getOutArcs() {
		return outArcs;
	}
	public void setOutArcs(Map<Integer, Integer> outArcs) {
		this.outArcs = outArcs;
	}
	/**
	 * freeFromBiConditions does not contain GlobalPlace
	 */
	Set<Integer> freeFromBiConditions;
	/**
	 * also does not contain GlobalPlace
	 */
	Integer[] constraintByBiConditions;
	Function<MixedInputTokenBinding, IOutputTokenBinding> outputFunction;
//	Function<MixedInputTokenBinding,Long> timeCostFunction;
	public void getOneBinding(){

	}

	
	public Set<Integer> getFreeFromBiConditions() {
		return freeFromBiConditions;
	}
	public void setFreeFromBiConditions(Set<Integer> freeFromBiConditions) {
		this.freeFromBiConditions = freeFromBiConditions;
	}
	public Integer[] getConstraintByBiConditions() {
		return constraintByBiConditions;
	}
	public void setConstraintByBiConditions(Integer[] constraintByBiConditions) {
		this.constraintByBiConditions = constraintByBiConditions;
	}
	public Integer getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public Map<Pair<Integer, Integer>, Predicate<Pair<IColor, IColor>> > getBiConditions() {
		return biConditions;
	}

	public void setBiConditions(Map<Pair<Integer, Integer>, Predicate<Pair<IColor, IColor>> > biConditions) {
		this.biConditions = biConditions;
	}

	public Map<Integer, Predicate<IColor>> getSiConditions() {
		return siConditions;
	}

	public void setSiConditions(Map<Integer, Predicate<IColor>> siConditions) {
		this.siConditions = siConditions;
	}
	public Function<MixedInputTokenBinding, IOutputTokenBinding> getOutputFunction() {
		return outputFunction;
	}

	public void setOutputFunction(Function<MixedInputTokenBinding, IOutputTokenBinding> outputFunction) {
		this.outputFunction = outputFunction;
	}

//	public Function<MixedInputTokenBinding, Long> getTimeCostFunction() {
//		return timeCostFunction;
//	}
//
//	public void setTimeCostFunction(Function<MixedInputTokenBinding, Long> timeCostFunction) {
//		this.timeCostFunction = timeCostFunction;
//	}
	
	
	/**
	 * according to the conditions to initialize the cache binding structure. (has no relation with the initial tokens)
	 */
	protected abstract void initCache(IndividualCPNInstance cpn);
	/**
	 * when the tokens in input place changes, this method needs to be called.
	 * <br> the method will update its cachedBinding
	 * <br> NOTICE: this class is not idempotent. That is, it assumes after checkNewTokens4Firing, all the newly tokens are moved into tested (However, this method does not implement it, a CPNInstance class needs to control that)
	 * @param cpnInstance
	 */
	public abstract void checkNewTokens4Firing(IndividualCPNInstance cpnInstance);
	/**
	 * remove all the candidate bindings related with the given token. 
	 * <br> this method needs to be called, if you find you can not get token any more from input place, while the token is still in cached bindings.
	 * <br> (only multi-threads mode requires )
	 * @param place
	 * @param token
	 */
	public  abstract  <T> void removeTokenFromCache(PlaceInstance place, IColor token);
	/**
	 * remove all the candidate bindings related with the given token. 
	 * <br> this method needs to be called, if you find you can not get token any more from input place, while the token is still in cached bindings.
	 * <br> (only multi-threads mode requires )
	 * <br> the default implementation is empty.
	 * @param place
	 * @param token
	 * @param target 
	 */
	public void removeTokenFromCache(PlaceInstance place, IColor token, ITarget target ){}
	/**
	 * remove all the candidate bindings related with the given token. 
	 * <br> this method needs to be called, if you find you can not get token any more from input place, while the token is still in cached bindings.
	 * <br> (only multi-threads mode requires)
	 * <br> defaultly, this method is do nothing. Subclasses can override this method if needed.
	 * @param place
	 * @param token
	 */
	public  void removeTokenFromCache(PlaceInstance place, ITarget target, IColor token){
	}
	/**
	 * get a binding from the cached. 
	 * <br> NOTICE, just from the cached bindings. You have not get them from places. Please call trytoGet method
	 * @param cpnInstance
	 * @return null if outputFunction is null.
	 */
	public abstract MixedInputTokenBinding randomBinding(IndividualCPNInstance cpnInstance);
	
	public  IOutputTokenBinding firing(MixedInputTokenBinding binding){
//		Long timepoint=this.timeCostFunction.apply(binding)+GlobalClock.getInstance().getTime();
//		GlobalClock.getInstance().addTimepoint(owner, timepoint);
		if(outputFunction==null)
			return null;
		IOutputTokenBinding  out=this.outputFunction.apply(binding);
		modifyTokenTime(out);
		return out ;
	}
	private void modifyTokenTime(IOutputTokenBinding tokens){
		long time=GlobalClock.getInstance().getTime();
		if(tokens.hasLocalForNP()){
			tokens.getLocalForNP().values().forEach(lt->{
				lt.forEach(t-> t.setTime(time+tokens.getLocalEffective()));
			});
		}
		if(tokens.hasLocalForConnection()){
			tokens.getLocalForConnection().values().forEach(map->
				map.values().forEach(lt->{
					lt.forEach(t->t.setTime(time+tokens.getLocalEffective()));
				})
			);
		}
		if(tokens.isRemote()){
			tokens.getRemoteForNP().values().forEach(lt->{
				lt.forEach(t->t.setTime(time+tokens.getTargetEffective()));
			});
		}
	}
	/**
	 * whether exists a valid binding
	 * @return
	 */
	public abstract boolean canFire(IndividualCPNInstance cpnInstance);
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}

}
