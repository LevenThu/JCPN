package jcpn.elements.transions.instance;

import java.util.HashMap;
import java.util.Map;

import jcpn.elements.colorsets.IColor;
import jcpn.elements.places.IOwner;
import jcpn.elements.places.ITarget;
/**
 * in fact, input token binding class should not exist. The reason that has this class is a mistake..
 * <br> That is, currently, we limit the number of tokens on each input Arc of a transitions as 1.
 * <br> In the future, we will discard this class.
 * <br>(MixedOutputTokenBinding is the correct design because it supports number >1)
 * @author hxd
 *
 */
public class MixedInputTokenBinding {
	
	private IOwner owner;
	@Override
	public String toString() {
		return "InB [simpleTokens=" + simpleTokens + ", owner=" + owner + ", target=" + target + ", connectionTokens="
				+ connectionTokens + "]";
	}
	Map<Integer, IColor> simpleTokens;
	ITarget target;
	Map<Integer, IColor> connectionTokens;//pid->tokens

	private MixedInputTokenBinding(IOwner owner,Map<Integer, IColor> simpleTokens,ITarget target){
		if(simpleTokens!=null)
			this.simpleTokens=simpleTokens;
		if(target!=null){
			this.target=target;
			this.connectionTokens=new HashMap<>();
		}
		this.owner=owner;
	}
	private MixedInputTokenBinding(IOwner owner,Map<Integer, IColor> simpleTokens,ITarget target,Map<Integer, IColor> connectionTokens){
		if(simpleTokens!=null)
			this.simpleTokens=simpleTokens;
		if(target!=null){
			this.target=target;
			this.connectionTokens=connectionTokens;
		}
		this.owner=owner;
	}
	public static MixedInputTokenBinding simpleBinding(IOwner owner,Map<Integer, IColor> simpleTokens){
		return new MixedInputTokenBinding(owner, simpleTokens, null);
	}

	public static MixedInputTokenBinding mixBinding(IOwner owner,Map<Integer, IColor> simpleTokens,ITarget target,Map<Integer, IColor> connectionTokens){
		return new MixedInputTokenBinding(owner,simpleTokens, target,connectionTokens);
	}
	public static MixedInputTokenBinding connectionBinding(IOwner owner,ITarget target,Map<Integer, IColor> connectionTokens){
		return new MixedInputTokenBinding(owner,null, target,connectionTokens);
	}
	public MixedInputTokenBinding addSimpleToken(Integer pid, IColor token){
		simpleTokens.put(pid, token);
		return this;
	}
	public MixedInputTokenBinding addConnectionTokens(ITarget target, Integer pid, IColor token){
		connectionTokens.put(pid,token);
		return this;
	}
	
	
	public boolean hasSimpleTokens(){
		return simpleTokens!=null &&simpleTokens.size()>0;
	}
	public boolean hasConnectionTokens(){
		return connectionTokens!=null && connectionTokens.size()>0;
	}

	public Map<Integer, IColor> getSimpleTokens() {
		return simpleTokens;
	}
	public void setSimpleTokens(Map<Integer, IColor> simpleTokens) {
		this.simpleTokens = simpleTokens;
	}
	public ITarget getTarget() {
		return target;
	}
	public void setTarget(ITarget target) {
		this.target = target;
	}
	public Map<Integer, IColor> getConnectionTokens() {
		return connectionTokens;
	}
	public void setConnectionTokens(Map<Integer, IColor> connectionTokens) {
		this.connectionTokens = connectionTokens;
	}
	public IOwner getOwner() {
		return owner;
	}
	public void setOwner(IOwner owner) {
		this.owner = owner;
	}
	

}
