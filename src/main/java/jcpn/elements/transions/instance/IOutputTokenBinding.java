package jcpn.elements.transions.instance;

import java.util.List;
import java.util.Map;

import jcpn.elements.colorsets.IColor;
import jcpn.elements.places.ITarget;

public interface IOutputTokenBinding {
	public boolean islocal();
	public boolean isRemote();
	 //this is a leave over problem. When the IOutputTokenBinding is remoted,  localForNP still may has connection place??
	public boolean hasLocalForNP();
	public boolean hasLocalForConnection();
	public Map<Integer, List<IColor>> getLocalForNP();
	public long getLocalEffective();
	public Map<Integer, Map<ITarget, List<IColor>>> getLocalForConnection();
	public Map<Integer, List<IColor>> getRemoteForNP();
	public ITarget getTarget();
	public long getTargetEffective();
}
