package jcpn.elements;

import java.util.HashMap;
import java.util.Map;

import jcpn.elements.places.Place;
import jcpn.elements.transions.Transition;

/**
 * Created by hxd on 16/5/14.
 */
public class CPNet {

	Map<Integer,Place> places;
	Map<Integer,Transition> transitions;
	String version;
	public CPNet() {
		super();
		this.places=new HashMap<>();
		this.transitions=new HashMap<>();
	}
	public Place getPlace(int placeId){
		return places.get(placeId);
	}
	public Map<Integer, Place> getPlaces() {
		return places;
	}

	public void setPlaces(Map<Integer, Place> places) {
		this.places = places;
	}

	public Map<Integer, Transition> getTransitions() {
		return transitions;
	}

	public void setTransitions(Map<Integer, Transition> transitions) {
		this.transitions = transitions;
	}

	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public Transition removeTransition(int id){
		return transitions.remove(id);
	}
	public Place removePlace(int id){
		return places.remove(id);
	}
	public void preCompile(){
		fill();
		transitions.values().forEach(t->t.preCompile(this));
//		places.values().forEach(p->{
//			p.setFreedom(true);
//			if(p.getOutArcs()!=null)
//				for(Integer tid:p.getOutArcs().keySet()){
//					if(!transitions.get(tid).getFreeFromBiConditions().contains(p.getId())
//							||transitions.get(tid).getSiConditions().keySet().contains(p.getId())){
//						p.setFreedom(false);
//						break;
//					}
//				}
//		});
	}
	/**
	 * auto fill Transions.inArcs.
	 * <br> 
	 * and, mark whether a place is freedom
	 */
	private void fill(){
		places.values().stream()
		.forEach(place->{
			place.getOutArcs().entrySet().stream()
			.forEach(entry->{
				transitions.get(entry.getKey()).addInArc(place.getId(),entry.getValue());
			});
		});
	}

	//	public void fire(Transition transition){
	//		List<PTArc> inputs=transition.getInputs();
	//		for(PTArc arc:inputs){
	//			Place place=this.getPlace(arc.getPlaceId());
	//			Bag<?> tokens=place.getCurrentTokens();
	//			for(int number: arc.getRequires()){
	//				tokens.stream().filter( token-> tokens.getCount(token)>=number);
	//			}
	//
	//			//			tokens.stream().filter(arc.get)
	//		}
	//	}
	//
	//	public Collection<Map<Place, List<IColor>>> candidateForFiring(Map<Place, Collection<List<IColor>>> candidateMap,Predicate<Map<Place, List<IColor>>> predicator){
	//		//每个<Place<?>,List<?>> 是一个binding
	//		
	//		return null;
	//	}






}
