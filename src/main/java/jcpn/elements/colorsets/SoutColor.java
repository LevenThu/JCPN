package jcpn.elements.colorsets;

import java.util.ArrayList;
import java.util.List;

import jcpn.elements.places.ITarget;

public class SoutColor extends IColor {
	@Override
	public String toString() {
		return "SoutColor [time=" + time + ", target=" + target + "]";
	}
	public SoutColor(ITarget target){
		this.target=target;
	}
	private ITarget target;


	public ITarget getTarget() {
		return target;
	}

	public void setTarget(ITarget target) {
		this.target = target;
	}
	public static List<IColor> generateList(ITarget[] values){
		List<IColor> list=new ArrayList<>();
		for(ITarget i:values){
			list.add(new SoutColor(i));
		}
		return list;
	}

}
