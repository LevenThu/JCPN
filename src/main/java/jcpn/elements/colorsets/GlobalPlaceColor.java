package jcpn.elements.colorsets;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * all the cpn instances will share tokens
 * <br> GlobalPlaceColor has no time properties
 * <br> be careful that global place may has concurrent exception, so it is better to copy the contents when you want to use it for a long time.
 * @author hxd
 *
 * @param <D>
 */
public class GlobalPlaceColor<D extends IColor> extends IColor{
	//	Map<Object, List<D>> tokens=new HashMap<>();
	private final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
	private List<D> contents=new ArrayList<>();
	@Override
	public String toString() {
		return "GlobalPlaceColor [contents=" + contents + ", merger=" + merger + "]";
	}
	//	public abstract void merge(Object target, D token);
	private MergeFunction<D> merger=(lt,t)->lt.add(t);
	@Override
	public final long getTime() {
		return 0;
	}
	public MergeFunction<D> getMerger() {
		return merger;
	}
	public void setMerger(MergeFunction<D> merger) {
		this.merger = merger;
	}
	public void addToken(D token){
		//		tokens.computeIfAbsent(target, t->contents);
		rwl.writeLock().lock();
		try{
			merger.merge(contents, token);
		}finally{
			rwl.writeLock().unlock();
		}
	}

	@Override
	public void setTime(long time) {
	}
	/**
	 * Notice that you'd better use getRwl to get a ReetrantReadWriteLock to guarantee concurrent safety.
	 * @return
	 */
	public List<D> getContents(){
		return contents;
	}
	public ReentrantReadWriteLock getRwl() {
		return rwl;
	}

}
