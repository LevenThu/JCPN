package jcpn.elements.colorsets;

import java.util.List;
@FunctionalInterface
/**
 *  given a new token, return the replaced token.
 *  <br> Notice that, only the new token and replaced token are used for updating caches of transitions.
 *  If you modify other tokens in the replace function, the behavior is undefined.
 * @author hxd
 *
 * @param <D>
 */
public interface ReplaceFunction<D extends IColor> {
	/**
	 * given a new token, return the replaced token.
	 * <br>Remember, if you want to replace the old token with new token, call tokens.add method to put the new token into the list.
	 * @param tokens
	 * @param token
	 * @return
	 */
		public  D  replace(List<D> tokens, D token);
}
