package jcpn.elements.colorsets;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import jcpn.runtime.GlobalClock;

public class UnitColor extends IColor{

	@Override
	public String toString() {
		return "UnitColor [time=" + time + "]";
	}
	public UnitColor(){
		super();
		time=GlobalClock.getInstance().getTime();
	}

	
	public static List<IColor> generateList(int number){
		List<IColor> list=new ArrayList<>();
		IntStream.range(0, number).forEach(i->list.add(new UnitColor()));
		return list;
	}
}
