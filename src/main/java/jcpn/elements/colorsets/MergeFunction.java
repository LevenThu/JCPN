package jcpn.elements.colorsets;

import java.util.List;
@FunctionalInterface
public interface MergeFunction<D extends IColor> {
		public  void merge(List<D> tokens, D token);
}
