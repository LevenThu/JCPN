package jcpn;

@SuppressWarnings("serial")
public class NotSupportException extends Exception{

	public NotSupportException(String string) {
		super(string);
	}

}
