package jcpn.runtime;

import java.util.List;

import jcpn.elements.colorsets.IColor;
import jcpn.elements.places.ITarget;

public interface GlobalConnector {
	/**
	 * NOTICE: this method MUST be thread safe.
	 * @param target
	 * @param tokens
	 * @param time
	 * @return
	 */
	public boolean submitTokensToSomeone(ITarget target, Integer pid, List<IColor> tokens);
}
