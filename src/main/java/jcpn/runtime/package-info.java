/**
 * Constraints (but we do not check them when compiling.):
 * - each input Arc only accepts one token.<br>
 * - the number of tokens on the output Arc is omitted. You need to design your output function to achieve to generate mulitple tokens for each output Arc<br>
 * - GlobalPlace can not be as a single constraint or join constraint on Transitions.<br>
 * - we do not recommend that the time cost of connection transition as 0. <br>
 * - Both Individual place and global place  can be as inputs of individual transitions. Connection place is banned.<br>
 * - All the places can be as inputs of connection transitions. However, individual place and connection place can not exist in one connection condition.<br>
 * - Individual transition can output to any place, while connection transition can only output to Individual place.<br>
 * - Place strategies:<br>
 *   - If any tokens can be used, we set it as BAG. For example, in a thread pool, any idle thread can provide service. Normally, data set is not BAG, but resource is BAG.<br>
 *   - If the latest tokens can not be used when the stale tokens have not been used, we set it as FIFO. However, if two tokens have the same time, their order is random. 
 *      For example, p1->t1->p2->t2,  t1 has 3 threads and t2 has only two threads. If 3 threads digest tokens from p1 and generate 3 tokens at the same time to p2, then which one will be chosen in t2 is random.
 *       But if thread 2 in t1 starts later than other 2 threads, but ends earlier than them, its output token will be first digest by t2.
 *   - customized. For example, a priori queue. Not supported now.<br>
 * - A place has a property called infiniteRead (default value is false). It means that when the place is an input place, it provides tokens while does not consume any tokens. Global place is an infinite read place. 
 *   The infinite read place is just an improvement for simulation performance. It is not a necessary place. It is surely that you can use individual place to implement this kind of place.<br>
 *     - You can set a replaceFunction to infinitedRead place. Notice that, though 
 * @author hxd
 *
 */
package jcpn.runtime;
