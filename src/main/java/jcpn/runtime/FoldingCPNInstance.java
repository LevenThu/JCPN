package jcpn.runtime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jcpn.elements.CPNet;
import jcpn.elements.colorsets.IColor;
import jcpn.elements.places.ConnectionPlace;
import jcpn.elements.places.GlobalPlace;
import jcpn.elements.places.IOwner;
import jcpn.elements.places.ITarget;
import jcpn.elements.places.IndividualPlace;
import jcpn.elements.places.Place;
import jcpn.elements.places.Place.PlaceType;
import jcpn.elements.places.StringOwner;
import jcpn.elements.places.instance.GlobalPlaceInstance;
import jcpn.elements.transions.Transition;
import jcpn.elements.transions.Transition.TransitionType;
import jcpn.runtime.HiddenCPNInstance.TokenAssignmentForHidden;
import jcpn.runtime.HiddenCPNInstance.TokenRelativeTimeForHidden;

public class FoldingCPNInstance implements GlobalConnector{
	// sys hidden instance is used for sending tokens to users' CPN instances
	public static final StringOwner _SYS_HIDDEN_INSTANCE = new StringOwner("_SYS_HIDDEN_INSTANCE");
	private static Logger logger=LogManager.getLogger();
	CPNet graph;
	private
	Map<IOwner,IndividualCPNInstance> individualInstances=new HashMap<>();
	Map<StringOwner, IndividualCPNInstance> soutInstances=new HashMap<>();
	//	@SuppressWarnings("rawtypes")
	//	List<IndividualCPNInstance> instances=new ArrayList<>();
	Map<Integer, GlobalPlaceInstance> globalPlaces=new HashMap<>();


	/**
	 * this instance can not have any tokens. It is only used for storing tokens which are temp added into the system.
	 * <br> E.g., when we register a new owner, we may want to add initial tokens for it. 
	 */
	HiddenCPNInstance hiddenInstance;

	public FoldingCPNInstance(CPNet graph){
		this.graph=graph;

	}
	public Set<IOwner> getOwners(){
		return individualInstances.keySet();
	}
	public boolean hasHidden(){
		return soutInstances.size()>0;
	}
	/**
	 *  A sout can send data to any individual cpn instances. 
	 *  However, when call addSoutInstance, we will add sout for each cpn instances. These sout are disable by default, because they have no initial tokens. You can send initial tokens to them by using SimulationEngine.pushSoutTokens to enable them.
	 *  <br> Notice that the sout can only send to Individual places.
	 * @param owner the sout name
	 * @param assignment Function: given a target, return a map&lt;Integer,List&lt;IColor&gt;&gt;, which means the tokens to the target cpn instance.
	 * @param timeCosts  Funtion: given a target, return  a long, which means the time cost that the target cpn instance can get the tokens.
	 * @param infinite whether generate tokens infinitely.
	 */
	public void addSoutInstance(StringOwner owner
			,TokenAssignmentForHidden assignment, TokenRelativeTimeForHidden timeCosts, boolean infinite){
		soutInstances.put(owner, new HiddenCPNInstance(owner, this, assignment, timeCosts,infinite));
		individualInstances.put(owner, soutInstances.get(owner));
	}
	/**
	 * ask hiddenInstance to addTokens in the future.
	 * <br> Meanwhile, the method will register a time point in the sending time line. 
	 * <br>
	 * this is not thread safety.
	 * @param absoluteTime absolute time
	 * @param target
	 * @param tokens
	 * @return the actually added time point
	 */
	public long addTokens(long absoluteTime, ITarget target,  Map<Integer, List<IColor>> tokens){
		if(GlobalClock.getInstance().getTime()>absoluteTime){
			absoluteTime=GlobalClock.getInstance().getTime()+1;
			logger.warn("apply for add a token in the past time, we will add it in the future (next time):"+absoluteTime);
		}
//		hiddenInstance.getRemoteSchedule().computeIfAbsent(absoluteTime, t->new ConcurrentHashMap<>());
//		Map<Integer, List<IColor>> map=hiddenInstance.getRemoteSchedule().get(absoluteTime).computeIfAbsent(target, t->new ConcurrentHashMap<>());
//		tokens.forEach((k,v)->map.computeIfAbsent(k, v1->new ArrayList<>()).addAll(v));
		GlobalClock.getInstance().addAbsoluteTimepointForSending(_SYS_HIDDEN_INSTANCE, absoluteTime);
		cacheSendingTask(absoluteTime,target,tokens);
		return absoluteTime;
	}
	//	/**
	//	 * ask hiddenInstance to addTokens in the future.
	//	 * <br> Meanwhile, the method will register a time point in the sending time line. 
	//	 * <br>
	//	 * this is not thread safety.
	//	 * @param absoluteTime
	//	 * @param target
	//	 * @param tokens
	//	 * @return the actually added time point
	//	 */
	//	public long addTokensIntoSout(Long absoluteTime, ITarget target,  List<IColor> tokens){
	//		if(GlobalClock.getInstance().getTime()>absoluteTime){
	//			absoluteTime=GlobalClock.getInstance().getTime()+1;
	//			logger.warn("apply for add a token in the past time, we will add it in the future (next time):"+absoluteTime);
	//		}
	//		hiddenInstance.getRemoteSchedule().computeIfAbsent(absoluteTime, t->new HashMap<>());
	//		hiddenInstance.getRemoteSchedule().get(absoluteTime).computeIfAbsent(target, t->new HashMap<>());
	//		hiddenInstance.getRemoteSchedule().get(absoluteTime).computeIfPresent(target, (k,v)->{v.put(1,tokens); return v;});
	//		GlobalClock.getInstance().addAbsoluteTimepointForSending(_SYS_HIDDEN_INSTANCE, absoluteTime);
	//		return absoluteTime;
	//	}


	public boolean registerNewOwner(IOwner owner){
		if(!compiled) 
			compile();
		Set<IOwner> owners=individualInstances.keySet();
		if(owners.contains(owner)){
			logger.warn("owner "+owner+" existed already.");
			return false;
		}
		Map<PlaceType, List<Place>> places=graph.getPlaces().values().stream()
				.collect(Collectors.groupingBy(p->p.getType() ));
		Map<TransitionType, List<Transition>> transitions=graph.getTransitions().values().stream()
				.collect(Collectors.groupingBy(t->t.getType() ));
		IndividualCPNInstance instance=new IndividualCPNInstance(owner,this);
		individualInstances.put(owner, instance);
		//		instances.add(instance);
		Set<ITarget> otherIndividuals=owners.stream().map(x->(ITarget)x).collect(Collectors.toSet());
		otherIndividuals.remove(owner);
		//copy all the places and transitions first.
		instance.construct(places, transitions, globalPlaces, otherIndividuals);
		//just register a new owner, no new tokens

		return true;
	}
	private boolean compiled=false;
	/**
	 * unfold the CPN net.
	 * @return
	 */
	public boolean compile(){
		if(compiled) return true;
		hiddenInstance=new HiddenCPNInstance(_SYS_HIDDEN_INSTANCE, this, null, null,true);
		individualInstances.put(_SYS_HIDDEN_INSTANCE, hiddenInstance);
		Map<PlaceType, List<Place>> places=graph.getPlaces().values().stream()
				.collect(Collectors.groupingBy(p->p.getType() ));
		Map<TransitionType, List<Transition>> transitions=graph.getTransitions().values().stream()
				.collect(Collectors.groupingBy(t->t.getType() ));
		Set<IOwner> owners=new HashSet<>();
		if(places.get(PlaceType.IndividualPlace)!=null){
			places.get(PlaceType.IndividualPlace).stream()
			.filter(p->((IndividualPlace) p).getInitialTokens()!=null)
			.forEach(p->owners.addAll(((IndividualPlace) p).getInitialTokens().keySet()));
		}
		if(places.get(PlaceType.ConnectionPlace)!=null){
			places.get(PlaceType.ConnectionPlace).stream()
			.filter(p->((ConnectionPlace) p).getInitialTokens()!=null)
			.forEach(p->{
				((ConnectionPlace) p).getInitialTokens().keySet()
				.forEach(pair->{owners.add(pair.getLeft()); owners.add(pair.getRight());});
			});
		}
		//places.get(PlaceType.ConnectionPlace).stream()

		//copy all the global tokens 
		if(places.containsKey(PlaceType.GlobalPlace)){
			places.get(PlaceType.GlobalPlace).stream()
			.forEach(place->{
				globalPlaces.put(place.getId(), new GlobalPlaceInstance((GlobalPlace)place));
			});
		}
		owners.stream().forEach(
				owner->
				{
					if(individualInstances.get(owner)==null){
						IndividualCPNInstance instance=new IndividualCPNInstance(owner,this);
						individualInstances.put(owner, instance);
						//						instances.add(instance);
						Set<ITarget> otherIndividuals=owners.stream().map(x->(ITarget)x).collect(Collectors.toSet());
						otherIndividuals.remove(owner);
						//copy all the places and transitions first.
						instance.construct(places, transitions, globalPlaces, otherIndividuals);
					}else{
						//logger.warn("owner "+owner+" existed already.");
					}
				}
				);
		if(places.containsKey(PlaceType.IndividualPlace)){
			places.get(PlaceType.IndividualPlace).stream()
			.forEach(place->{
				this.fillIndividualPlaceInstanceTokens((IndividualPlace) place);
			});
		}
		if(places.containsKey(PlaceType.ConnectionPlace)){
			places.get(PlaceType.ConnectionPlace).stream()
			.forEach(place->{
				this.fillConnectionPlaceInstanceTokens((ConnectionPlace)place);
			});
		}
		compiled=true;
		return true;
	}
	private <O> void fillIndividualPlaceInstanceTokens(IndividualPlace place){
		if((place).getInitialTokens()==null){
			return;
		}
		(place).getInitialTokens().entrySet().stream()
		.forEach(entry->{
			individualInstances.get(entry.getKey()).fillTokens(place,entry.getValue());
		});
	}
	private <O,T> void  fillConnectionPlaceInstanceTokens(ConnectionPlace place){
		//firstly, change tokens map
		if(place.getInitialTokens()==null){
			return;
		}
		HashMap<IOwner, Map<ITarget, List<IColor>>> tokens=
				place.getInitialTokens().entrySet().stream()
				.collect(HashMap<IOwner, Map<ITarget, List<IColor>>>::new,
						(m,entry)->{
							if(!m.containsKey(entry.getKey().getLeft())){
								m.put(entry.getKey().getLeft(),new HashMap<ITarget, List<IColor>>());
							}		
							m.get(entry.getKey().getLeft()).put(entry.getKey().getRight(),entry.getValue());
						},
						HashMap<IOwner, Map<ITarget, List<IColor>>>::putAll);
		//secondly, generate instance
		tokens.entrySet().stream()
		.forEach(entry->{
			individualInstances.get(entry.getKey()).fillTokens(place,entry.getValue());
		});
	}
	@Override
	public String toString() {
		return "FoldingCPNInstance [graph=" + graph + ", individualInstances=" + individualInstances + ", globalPlaces="
				+ globalPlaces + "]";
	}
	public Map<IOwner, IndividualCPNInstance> getIndividualInstances() {
		return individualInstances;
	}
	public void setIndividualInstances(Map<IOwner, IndividualCPNInstance> individualInstances) {
		this.individualInstances = individualInstances;
	}

	/***
	 * this is thread safety
	 */
	@Override
	public boolean submitTokensToSomeone(ITarget target, Integer pid, List<IColor> tokens) {
		if(individualInstances.get(target)==null){
			System.out.println("");
		}
		individualInstances.get(target).addLocalNPNewlyTokens(pid, tokens);
		return false;
	}

	public boolean isCompiled() {
		return compiled;
	}
	/**
	 * 
	 * cache sending tasks which belong to _SYSMTE_HIDDEN_INSTANCE_
	 * @param absoluteTime
	 * @param target
	 * @param tokens
	 */
	private void cacheSendingTask(long absoluteTime, ITarget target, Map<Integer, List<IColor>> tokens) {
		Map<Integer, List<IColor>> tasks=systemHiddenInstanceRemoteTasks
										.computeIfAbsent(absoluteTime, time->new ConcurrentHashMap<>())
									   .computeIfAbsent(target, tar->new ConcurrentHashMap<>());
		tokens.forEach((pid,lt)->tasks.computeIfAbsent(pid, p->new ArrayList<>()).addAll(lt));
	}
	Map<Long, Map<ITarget, Map<Integer, List<IColor>>>> systemHiddenInstanceRemoteTasks=new ConcurrentHashMap<>();
	
	/**
	 * this method can only be called before execute next sending tasks in GlobalClock (i.e., this class's sendRemoteNewlyTokens). 
	 * Only in this way, we can guarantee thread safety (no concurrency, in fact).
	 * @param absoluteTime
	 * @return
	 */
	public boolean applyCachedSendingTasks(long absoluteTime){
		if(!systemHiddenInstanceRemoteTasks.containsKey(absoluteTime)){
			return false;
		}else{
			hiddenInstance.getRemoteSchedule().computeIfAbsent(absoluteTime, t->new HashMap<>());
			systemHiddenInstanceRemoteTasks.remove(absoluteTime).forEach((target,tokens)->{
				Map<Integer, List<IColor>> map=hiddenInstance.getRemoteSchedule().get(absoluteTime).computeIfAbsent(target, t->new HashMap<>());
				tokens.forEach((k,v)->map.computeIfAbsent(k, v1->new ArrayList<>()).addAll(v));
			});
			return true;
		}
	}
}
