package jcpn.runtime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jcpn.elements.colorsets.IColor;
import jcpn.elements.places.ConnectionPlace;
import jcpn.elements.places.IOwner;
import jcpn.elements.places.ITarget;
import jcpn.elements.places.IndividualPlace;
import jcpn.elements.places.Place;
import jcpn.elements.places.Place.PlaceStrategy;
import jcpn.elements.places.Place.PlaceType;
import jcpn.elements.places.instance.ConnectionPlaceInstance;
import jcpn.elements.places.instance.GlobalPlaceInstance;
import jcpn.elements.places.instance.ISimpleTokenOrganizater;
import jcpn.elements.places.instance.IndividualPlaceInstance;
import jcpn.elements.places.instance.PlaceInstance;
import jcpn.elements.transions.ConnectionTransition;
import jcpn.elements.transions.IndividualTransition;
import jcpn.elements.transions.Transition;
import jcpn.elements.transions.Transition.TransitionType;
import jcpn.elements.transions.instance.ConnectionTransitionInstance;
import jcpn.elements.transions.instance.IOutputTokenBinding;
import jcpn.elements.transions.instance.IndividualTransitionInstance;
import jcpn.elements.transions.instance.MixedInputTokenBinding;
import jcpn.elements.transions.instance.TransitionInstance;
/**
 * TODO transition priority related codes need to be re-writen.
 * @author hxd
 *
 */
public class IndividualCPNInstance {
	private static Logger logger=LogManager.getLogger();
	IOwner owner;
	Map<Integer,PlaceInstance> places=new HashMap<>();
	
	public String printAllTokens() {
		return "ICPN [owner=" + owner + ", places=" + places + "]";
	}
	public String printInterestingTokens(int[] pids){
		Set<Integer> pidSet=new HashSet<>();
		for(int i:pids){
			pidSet.add(i);
		}
		return "ICPN [owner=" + owner + ", places=" + places.values().stream().filter(p->pidSet.contains(p.getId())).collect(Collectors.toList()) + "]";
	}
	Map<Integer,TransitionInstance> transitions=new HashMap<>();
	GlobalConnector connector;
	public PlaceInstance getPlace(Integer id){
		return this.places.get(id);
	}
	//	@SuppressWarnings("unchecked")
	//	public void compilePlaces(Map<PlaceType, List<Place>> places){
	//		this.places=places.get(PlaceType.NodePlace).stream()
	//		.collect(()-> this.places,
	//				(m,p) -> m.put(p.getId(),new NodePlaceInstance<O>((NodePlace<O>)p, owner)),
	//				(l,r) -> l.putAll(r)
	//				);
	//		
	//	}

	private void addPlaceInstance(GlobalPlaceInstance place) {
		places.put(place.getId(), place);
	}
	private void addPlaceInstance(IndividualPlace place) {
		places.put(place.getId(), new IndividualPlaceInstance(place, this.owner));
	}
	public void fillTokens(IndividualPlace place, List<IColor> tokens) {
		((IndividualPlaceInstance)places.get(place.getId())).classfyNewlyTokens(owner, tokens);
		places.put(place.getId(), new IndividualPlaceInstance(place, this.owner, tokens));
	}


	public void fillTokens(ConnectionPlace place, Map<ITarget,List<IColor>> tokens) {
		((ConnectionPlaceInstance )places.get(place.getId())).classfyNewlyTokens(tokens);
		places.put(place.getId(), new ConnectionPlaceInstance(place, this.owner, tokens));
	}
	private void addPlaceInstance(ConnectionPlace place, Set<ITarget> targets) {
		places.put(place.getId(), new ConnectionPlaceInstance(place, this.owner, targets));
	}
	private  void addTransitionInstance(IndividualTransition transition){
		transitions.put(transition.getId(), new IndividualTransitionInstance(transition, owner, this));
	}
	private  void addTransitionInstance(ConnectionTransition transition,Set<ITarget> targets){
		transitions.put(transition.getId(), new ConnectionTransitionInstance(transition, owner,targets, this));

	}
	/**
	 * copy places and transitions into this cpn instance.  Initial tokens are not included.
	 * <br> Though global places have been included in places, we  extract them into this instance again
	 * @param places  no matter whether places have global places, we do not copy the global places in them
	 * @param transitions
	 * @param globalPlaces we copy these global places into the cpn instance
	 */
	public void construct(Map<PlaceType, List<Place>> places,Map<TransitionType, List<Transition>> transitions,Map<Integer, GlobalPlaceInstance> globalPlaces, Set<ITarget> otherIndividuals){
		//copy all the places and transitions first.
		if(places.containsKey(PlaceType.IndividualPlace)){
			places.get(PlaceType.IndividualPlace).stream().forEach(place->{
				this.addPlaceInstance((IndividualPlace) place);
			});
		}
		if(places.containsKey(PlaceType.ConnectionPlace)){
			places.get(PlaceType.ConnectionPlace).stream()
			.forEach(place->{
				this.addPlaceInstance((ConnectionPlace) place,otherIndividuals);
			});
		}
		if(globalPlaces.size()>0){
			for(GlobalPlaceInstance place:globalPlaces.values()){
				this.addPlaceInstance(place);
			}
		}

		if(transitions.containsKey(TransitionType.IndividualTransition)){
			transitions.get(TransitionType.IndividualTransition).stream()
			.forEach(transition->{
				this.addTransitionInstance((IndividualTransition)transition);
			});
		}
		if(transitions.containsKey(TransitionType.ConnectionTransition)){
			transitions.get(TransitionType.ConnectionTransition).stream()
			.forEach(transition->{
				this.addTransitionInstance((ConnectionTransition)transition, otherIndividuals);
			});
		}
		this.initPriorities();

	}

	@Override
	public String toString() {
		return "ICPN [owner=" + owner + ", places=" + places + ", transitions=" + transitions + "]";
	}
	public IndividualCPNInstance(IOwner owner,GlobalConnector connector) {
		this.owner=owner;
		this.connector=connector;
	}
	public IOwner getOwner() {
		return owner;
	}
	public void setOwner(IOwner owner) {
		this.owner = owner;
	}
	public Map<Integer, PlaceInstance> getPlaces() {
		return places;
	}
	public void setPlaces(Map<Integer, PlaceInstance> places) {
		this.places = places;
	}
	public Map<Integer, TransitionInstance> getTransitions() {
		return transitions;
	}
	public void setTransitions(Map<Integer, TransitionInstance> transitions) {
		this.transitions = transitions;
	}
	//	public void checkForFiring(TransitionInstance<?> transitionInstance){
	//	}
	/**
	 * process all the newly tokens and update transitions' cached, then mark these tokens as tested.
	 * this method is idempotent
	 */
	public void checkNewlyTokensAndMarkAsTested(){
		transitions.values().forEach(t->t.checkNewTokens4Firing(this));
		places.values().forEach(p->p.markTokensAsTested(this));
	}
	/**
	 * remove selected tokens from the caches from all the transitions.
	 * @param binding
	 */
	public void removeTokensFromAllTransitionsCache(MixedInputTokenBinding binding){
		if(binding.hasSimpleTokens()){
			binding.getSimpleTokens().forEach((pid,token)->{
				PlaceInstance place= places.get(pid);
				if(!place.getType().equals(PlaceType.GlobalPlace))
					place.getOutArcs().keySet().forEach(tid->{
						transitions.get(tid).removeTokenFromCache(place, token);
					});
			});
		}
		if(binding.hasConnectionTokens()){
			binding.getConnectionTokens().forEach((pid,token)->{
				PlaceInstance place= places.get(pid);
				place.getOutArcs().keySet().forEach(tid->{
					transitions.get(tid).removeTokenFromCache(place, token,binding.getTarget());
				});
			});	
		}
	}
	/**
	 * after calling fire(), you must call this method because it will tell the clock the next time point.
	 * <br>
	 * this is not thread safe.
	 * <br>
	 * currently, this method is not thread safe, because ISimpleTokenBinding.classfyNewlyTokens is not thread safe.
	 * @param tokens
	 */
	public void addLocalNewlyTokensAndScheduleNextTime(IOutputTokenBinding tokens){
		logger.trace(()->"handling a output token binding, " +tokens.getClass().getSimpleName());
		long currentTime=GlobalClock.getInstance().getTime();
		if(tokens.hasLocalForNP()){
			tokens.getLocalForNP().forEach((pid,lt)->
			((ISimpleTokenOrganizater)places.get(pid)).classfyNewlyTokens(owner, lt)
					);
			if(tokens.getLocalEffective()!=0){
				logger.trace(()->"ask for adding a new running event into the queue at time "+(currentTime+tokens.getLocalEffective()) +" for owner "+owner);
				GlobalClock.getInstance().addAbsoluteTimepointForRunning(owner, currentTime+tokens.getLocalEffective());
			}
		}
		
		if(tokens.hasLocalForConnection()){
			tokens.getLocalForConnection()
			.forEach((pid, map)->
				map.forEach((target,lt)->
				((ConnectionPlaceInstance)places.get(pid)).classfyNewlyTokens(target, lt))
			);
			if(tokens.getLocalEffective()!=0){
				logger.trace(()->"ask for adding a new running event into the queue at time "+(currentTime+tokens.getLocalEffective()) +" for owner "+owner);
				GlobalClock.getInstance().addAbsoluteTimepointForRunning(owner, currentTime+tokens.getLocalEffective());
			}
		}
		if(tokens.isRemote()){
			long time=GlobalClock.getInstance().getTime()+tokens.getTargetEffective();
			remoteSchedule.computeIfAbsent(time, t->newRemoteScheduleMapAtOneTime());
			remoteSchedule.get(time).computeIfAbsent(tokens.getTarget(), t->newRemoteScheduleMapForOneTarget());
			Map<Integer, List<IColor>> map=remoteSchedule.get(time).get(tokens.getTarget());
			tokens.getRemoteForNP().forEach((pid,lt)->{
				map.computeIfPresent(pid, (p,pre)->{pre.addAll(lt); return pre;});
				map.computeIfAbsent(pid, p->new ArrayList<>(lt));
			});
			//tell clock the owner will send data to others
			if(tokens.getTargetEffective()==0){
				logger.debug("the time cost of a connection transition for sending data can not be 0, it will slow down the simulation");
			}
			logger.trace(()->"ask for adding a new sending event into the queue at time "+(currentTime+tokens.getTargetEffective()) +" for owner "+owner);
			GlobalClock.getInstance().addAbsoluteTimepointForSending(owner, currentTime+tokens.getTargetEffective());
			//tell clock who needs to be wake up to handle data.
			if(tokens.hasLocalForNP()&& tokens.getTargetEffective()!=0){//TODO: whether do we need to add this condition?
				//FIXME: tokens.hasLocalForNP and tokens.getTargetEffective may be wrong, because one is for local event and the other is for remote event.
				//please recheck it.
				logger.trace(()->"ask for adding a new running event into the queue at time "+(currentTime+tokens.getTargetEffective()) +" for owner "+owner);
				GlobalClock.getInstance().addAbsoluteTimepointForRunning(tokens.getTarget(), currentTime+tokens.getTargetEffective());
			}
		}
	}
	/**
	 * this is thread safe.
	 * <br>
	 * only used for individual and global places
	 * <br> this method do not register any events on the timeline.
	 * @param pid
	 * @param tokens
	 */
	public void addLocalNPNewlyTokens(Integer pid, List<IColor> tokens){
		ISimpleTokenOrganizater instance=(ISimpleTokenOrganizater) places.get(pid);
		synchronized (instance) {
			instance.classfyNewlyTokens(owner, tokens);
		}
	}

	//effectiveTime-> target-> pid->tokens
	Map<Long, Map<ITarget, Map<Integer, List<IColor>>>> remoteSchedule=new HashMap<>();

	/**
	 * send all the output tokens to other individual cpn instances.
	 * <br> Then, to guarantee other programs have no error, we roughly add a time point in the running time line.
	 * @param absolutiveTime
	 * @param connector
	 */
	public void sendRemoteNewlyTokens(Long absolutiveTime,GlobalConnector connector){
		if(logger.isDebugEnabled()){
			remoteSchedule.get(absolutiveTime).forEach((target,tokens)->logger.debug(()->this.getOwner() +" sends "+tokens+" to "+target));
		}
		remoteSchedule.get(absolutiveTime)
		.forEach((target,tokens)->{
			tokens.forEach((pid,lt)->connector.submitTokensToSomeone(target, pid,lt));
		});
		//if sent, we must tell the timeline to running new data
		remoteSchedule.get(absolutiveTime).keySet().forEach(target->{
			GlobalClock.getInstance().addAbsoluteTimepointForRunning(target, absolutiveTime);
		});
		remoteSchedule.remove(absolutiveTime);
	}
	/**this method is used for control the cpn instance manually.*/
	public  Map<ITarget, Map<Integer, List<IColor>>> getSendingTasks(Long absolutiveTime){
		return remoteSchedule.get(absolutiveTime);
	}
	/**this method is used for control the cpn instance manually.
	 * <br> To guarantee other programs have no error, we roughly add a time point in the running time line.*/
	public void sendRemoteNewlyTokens(Long absolutiveTime,ITarget target, GlobalConnector connector){
		if(remoteSchedule.containsKey(absolutiveTime)){
			remoteSchedule.get(absolutiveTime).get(target).forEach((pid,lt)->connector.submitTokensToSomeone(target, pid,lt));
			GlobalClock.getInstance().addAbsoluteTimepointForRunning(target, absolutiveTime);
		}
	}
	
	/**
	 * check which transitions can be fired
	 * @return
	 */
	public boolean canFire(){
		List<TransitionInstance> canFireTransitions= this.transitions.values().stream().filter(t->t.canFire(this)).collect(Collectors.toList());
		this.initPriorities();
		canFireTransitions.forEach(t->{
			canFireTransitionsOrderByPriority.get(t.getPriority()).add(t.getId());
		});
		return canFireTransitions.size()>0;
	}
	Map<Integer, List<Integer>> canFireTransitionsOrderByPriority=new TreeMap<>();//priority-> transitions
	
	/**meaningful only when asked canFire().<br>
	 * this method is used for control cpn manually*/
	public List<Integer> getCanFireTransitions() {
		for(Map.Entry<Integer, List<Integer>> entry:canFireTransitionsOrderByPriority.entrySet()){
			if(entry.getValue().size()>0){
				return entry.getValue();
			}
		}
		return null;
	}



	/**
	 * fire selected transition
	 * @param tid
	 * @return
	 */
	public MixedInputTokenBinding askForFire(Integer tid){
		canFireTransitionsOrderByPriority=null;//.forEach((k,v)->v.clear());
		if(this.transitions.get(tid).canFire(this)){
			return this.transitions.get(tid).randomBinding(this);
		}
		else
			return null;
	}
	Random random=new Random();
	/**
	 * randomly select a (possible can be fired) transition to fire
	 * @return null if there is no transitions can be fired.
	 */
	public Integer getARandomTranstionWhichCanFire(){
		if(canFireTransitionsOrderByPriority==null)
			canFire();
		List<Integer> prioirtyFire=getCanFireTransitions();
		if(prioirtyFire==null){
			return null;
		}
		return prioirtyFire.get(random.nextInt(prioirtyFire.size()));
	}
	/**
	 * re-check all the transitions to check whether there is a transition can be fired.
	 * @return
	 */
	public boolean confirmNoTransitionCanFire(){
		return this.transitions.values().stream().noneMatch(t->t.canFire(this));
	}

	public IOutputTokenBinding fire(Integer tid,MixedInputTokenBinding binding){
		return transitions.get(tid).firing(binding);
	}
	/**
	 * try to digest tokens from  places.
	 * if success, the tokens will be put into binding, otherwise nothing happened 
	 * (But, the related places will be taken some tokens and then returned temp).
	 * therefore, it is not a thread safe method.
	 * @param cpnInstance
	 * @param binding
	 * @return
	 */
	public boolean trytoGetTokensFromPlaces(MixedInputTokenBinding binding){
		Map<Integer, IColor> simpleRemoved=new HashMap<>();

		if(binding.hasSimpleTokens()){
			for(Map.Entry<Integer, IColor> entry:binding.getSimpleTokens().entrySet()){
				PlaceInstance placeInstance=places.get(entry.getKey());
				if(placeInstance.isInfiniteRead()){//At least, global place is infiniteRead.
					continue;
				}
				if(((ISimpleTokenOrganizater)placeInstance).getTestedTokens().remove(entry.getValue())){
					simpleRemoved.put(entry.getKey(), entry.getValue());
				}else{
					returnBack(simpleRemoved);
					return false;
				}
			}
		}
		Map<Integer, IColor> connectionRemoved=new HashMap<>();
		if(binding.hasConnectionTokens()){
			for(Map.Entry<Integer, IColor> entry:binding.getConnectionTokens().entrySet()){
				PlaceInstance placeInstance=places.get(entry.getKey());
				if(placeInstance.isInfiniteRead()){//At least, global place is infiniteRead.
					continue;
				}
				if(((ConnectionPlaceInstance)placeInstance).getTestedTokens().get(binding.getTarget())!=null&&((ConnectionPlaceInstance)placeInstance).getTestedTokens().get(binding.getTarget()).remove(entry.getValue())){
					connectionRemoved.put(entry.getKey(), entry.getValue());
				}else{
					returnBack(simpleRemoved);
					returnBack(connectionRemoved,binding.getTarget());
					return false;
				}
			}
		}
		return true;
	}

	private void returnBack( Map<Integer, IColor> removed) {
		for(Map.Entry<Integer, IColor> entry:removed.entrySet()){
			PlaceInstance placeInstance=places.get(entry.getKey());
			if(placeInstance.getType().equals(PlaceType.GlobalPlace)){
				//global place do not consume tokens
				logger.warn("global place should not be put into a removed map, please optimize your program");
				continue;
			}
			if(placeInstance.getPlaceStrategy().equals(PlaceStrategy.BAG)){
				((ISimpleTokenOrganizater)placeInstance).getTestedTokens().add(entry.getValue());
			}else{
				((ISimpleTokenOrganizater)placeInstance).getTestedTokens().add(0, entry.getValue());
			}
		}
	}

	private void returnBack( Map<Integer, IColor> removed, ITarget target) {
		for(Map.Entry<Integer, IColor> entry:removed.entrySet()){
			ConnectionPlaceInstance placeInstance=(ConnectionPlaceInstance) places.get(entry.getKey());
			placeInstance.getTestedTokens().computeIfAbsent(target, t->new ArrayList<>());
			if(placeInstance.getPlaceStrategy().equals(PlaceStrategy.BAG)){
				placeInstance.getTestedTokens().get(target).add(entry.getValue());
			}else{
				placeInstance.getTestedTokens().get(target).add(0, entry.getValue());
			}
		}
	}

	public Map<Long, Map<ITarget, Map<Integer, List<IColor>>>> getRemoteSchedule() {
		return remoteSchedule;
	}
	/**
	 * after construct the transitions, call this method to sort them according to the priority
	 */
	private void initPriorities() {
		canFireTransitionsOrderByPriority=new TreeMap<>();
		transitions.values().forEach(t->{
			canFireTransitionsOrderByPriority.computeIfAbsent(t.getPriority(), tid->new ArrayList<>());
		});
	}

	protected Map<ITarget, Map<Integer, List<IColor>>> newRemoteScheduleMapAtOneTime(){
		return new HashMap<>();
	}
	protected Map<Integer, List<IColor>> newRemoteScheduleMapForOneTarget(){
		return new HashMap<>();
	}
}
