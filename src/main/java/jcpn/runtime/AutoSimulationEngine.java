package jcpn.runtime;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jcpn.elements.Pair;
import jcpn.elements.places.IOwner;
import jcpn.elements.transions.instance.IOutputTokenBinding;
import jcpn.elements.transions.instance.MixedInputTokenBinding;
import jcpn.elements.transions.instance.TransitionInstance;
/**
 * auto mode:
 * <br>
 * <pre>
 * SimulationEngine engine=new SimulationEngine(foldCPN);
 * engine.compile();
 * while(engine.hasNextTimepoint()){
 *  System.out.println("next time point:"+engine.getNextTimepoint());
 * 	engine.nextRound();
 * }
 * </pre>
 * <br> 
 * manually mode:
 * 
 * @author hxd
 *
 */
public class AutoSimulationEngine extends SimulationEngine{
	private static Logger logger=LogManager.getLogger();
	
	
	public AutoSimulationEngine(FoldingCPNInstance cpnInstance,Mode mode) {
		super(cpnInstance,mode);
	}

	/**
	 * this method may block iff (1) the engine is in a stream mode, (2) no transition can fire.
	 * <br>Notice, the method may not execute a time point from getNextTimepoint(), because the latest time may have been modified.
	 * <br> if you want to guarantee that, use synchronized keyword to wrap this object and code snippet from getNextTimepoint to netRound
	 * @return
	 * @throws InterruptedException
	 */
	public boolean nextRound() throws InterruptedException{
//		AdditionalTokenPackage package1=null;
//		while((package1=queue.poll())!=null){
//			cpnInstance.addTokens(GlobalClock.getInstance().getTime()+package1.getRelativeTime(), package1.getTarget(), package1.getTokens());
//		}
//		if(mode.equals(Mode.STREAM)&&!hasNextTimepointWithoutQueue()){
//			package1=queue.take();
//			cpnInstance.addTokens(GlobalClock.getInstance().getTime()+package1.getRelativeTime(), package1.getTarget(), package1.getTokens());
//		}
		checkStatus();
		if(mode.equals(Mode.STREAM)&&!hasNextTimepointWithoutQueue()){
			makeSureWeHaveTasks();
		}
		Pair<Long, Long> nextTime=clock.nextTime();
		if(nextTime.getLeft()<nextTime.getRight()){
			logger.debug(()->"call nextSendRound");
			return nextSendRound();
		}else if(nextTime.getLeft().equals(nextTime.getRight())){
			boolean ok=false;
			if(nextTime.getLeft()>0){
				logger.debug(()->"call nextSendRound");
				ok=nextSendRound();
			}
			else{
				logger.debug(()->"call nextRunRound");
				ok=nextRunRound();
			}
			return ok;
		}else{
			return nextRunRound();
		}
	}
	private boolean nextSendRound(){
		Map.Entry<Long, Map<IOwner, Object>> entry=clock.pollNextSendingTime();
		if(entry==null)
			return false;
		cpnInstance.applyCachedSendingTasks(entry.getKey());
		entry.getValue().keySet().parallelStream()
			.forEach(owner->	cpnInstance.getIndividualInstances().get(owner).sendRemoteNewlyTokens(entry.getKey(), cpnInstance));
		return true;
	}
	private boolean nextRunRound(){
		Map.Entry<Long, Map<IOwner, Object>> entry=clock.pollNextRunningTime();
		if(entry==null)
			return false;
		logger.trace(()->"will get next running event..."+entry);
		entry.getValue().keySet().parallelStream()
			.forEach(owner->runACPNInstance(cpnInstance.getIndividualInstances().get(owner)));
		return true;
	}
	
	private void runACPNInstance(IndividualCPNInstance cpn){
		logger.trace(()->"run cpn instacne "+cpn.getOwner());
		cpn.checkNewlyTokensAndMarkAsTested();
		logger.trace(()->"have checked newly tokens in cpn instacne "+cpn.getOwner());
		while(cpn.canFire()){
			Integer tid=cpn.getARandomTranstionWhichCanFire();
			MixedInputTokenBinding binding=cpn.askForFire(tid);
			reportPlaceBeforeFiring(binding, cpn, tid);
			if(cpn.trytoGetTokensFromPlaces(binding)){
				cpn.removeTokensFromAllTransitionsCache(binding);
				IOutputTokenBinding output=cpn.fire(tid, binding);
				if(output!=null){
					cpn.addLocalNewlyTokensAndScheduleNextTime(output);
				}
				cpn.checkNewlyTokensAndMarkAsTested();	
				reportPlaceAfterFiring(binding,output, cpn, tid);
				logger.debug(()->String.format("\ntime:%d \nowner:%s \nfired:%d,%s \nbinding:%s \nout:%s\n------",
									GlobalClock.getInstance().getTime(),cpn.getOwner(),
									tid,((TransitionInstance)cpn.getTransitions().get(tid)).getName(),
									binding,output
									));
			}else{
				logger.debug(()->"can fire but binding failed in cpn instacne "+cpn.getOwner());
			}
		}
	}
}
