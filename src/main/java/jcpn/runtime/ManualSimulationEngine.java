package jcpn.runtime;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jcpn.elements.colorsets.IColor;
import jcpn.elements.places.IOwner;
import jcpn.elements.places.ITarget;
import jcpn.elements.transions.instance.IOutputTokenBinding;
import jcpn.elements.transions.instance.MixedInputTokenBinding;
/**

 * manually mode:
 * <pre>
 * ManualSimulationEngine engine=new ManualSimulationEngine(foldCPN);
 * engine.compile();
 * while(engine.hasNextTimepoint()){
 *  Long time=engine.getNextTimepoint();
 *  Set&lt;Object&gt; sendings=engine.getNextSendingInstances(time);
 *  if(sendings!=null)
 *  sendings.forEach(owner->{
 *     Set&lt;Object&gt; targets=engine.getAllSendingTargets(time,owner);
 *     targets.forEach(target->engine.runNextSendingEvent(time,owner,target));
 *    });
 *  Set&lt;Object&gt; runnings=engine.getNextRunningInstances(time); 
 *  if(runnings!=null)
 *  runnings.forEach(owner->{
 *      while(true){
 *  	  List&lt;Integer&gt; tids=engine.getAllPossibleFire(owner);
 *        if(tids.size()>0){
 *  	    Collections.shuffle(tids);
 *          MixedInputTokenBinding binding=engine.askForBinding(owner,tids.get(0));
 *          IOutputTokenBinding out=engine.fire(owner,tids.get(0),binding);
 *        }else{
 *          break;
 *        }
 *      }
 *  }); 
 * }
 * </pre>
 * 	 * <br>Notice, the class may not execute a time point from getNextTimepoint(), because the latest time may have been modified.
	 * <br> if you want to guarantee that, use synchronized keyword to wrap this object and code snippet from getNextTimepoint to next****() method


 * @author hxd
 *
 */
public class ManualSimulationEngine extends SimulationEngine{


	public ManualSimulationEngine(FoldingCPNInstance cpnInstance,Mode mode) {
		super(cpnInstance,mode);
	}


	/**
	 * Notice: you MUST run runNextSendingEvent, if you have called getNextSendingInstances.
	 * It is because these events have been cleaned from background schedule.
	 * <br> this method may  run a more earlier time event.
	 * @param time
	 * @return null if the time has no sending events
	 * @throws InterruptedException 
	 */
	public Set<IOwner> getNextSendingInstances(Long time) throws InterruptedException{
//		AdditionalTokenPackage package1=null;
//		long now=GlobalClock.getInstance().getTime();
//		while((package1=queue.poll())!=null){
//			cpnInstance.addTokens(now+package1.getRelativeTime(), package1.getTarget(), package1.getTokens());
//		}
//		if(mode.equals(Mode.STREAM)&&!hasNextTimepointWithoutQueue()){
//			package1=queue.take();
//			cpnInstance.addTokens(now+package1.getRelativeTime(), package1.getTarget(), package1.getTokens());
//		}
		checkStatus();
		if(mode.equals(Mode.STREAM)&&!hasNextTimepointWithoutQueue()){
			makeSureWeHaveTasks();
		}
		Map.Entry<Long, Map<IOwner, Object>> entry=clock.pollNextSendingTime();
		if(entry!=null){
			cpnInstance.applyCachedSendingTasks(entry.getKey());
			return entry.getValue().keySet();
		}
		return null;
	}
	public Set<ITarget> getAllSendingTargets(Long time, IOwner owner){
		Map<Long, Map<ITarget, Map<Integer, List<IColor>>>>map=cpnInstance.getIndividualInstances().get(owner).getRemoteSchedule();
		if(!map.containsKey(time)){
			return null;
		}
		return map.get(time).keySet();
	}

	public void runNextSendingEvent(Long absolutiveTime, IOwner owner, ITarget target){
		cpnInstance.getIndividualInstances().get(owner).sendRemoteNewlyTokens(absolutiveTime, target, cpnInstance);
	}

	/**
	 * Notice: you MUST run runNextRunningEvent, if you have called getNextRunningInstances.
	 * It is because these events have been cleaned from background schedule. 
	 * @param time
	 * @return null if the time has no running events
	 */
	public Set<IOwner> getNextRunningInstances(Long time){
		checkStatus();
		Map.Entry<Long, Map<IOwner, Object>> entry=clock.pollNextRunningTime();
		if(entry!=null)
			return entry.getValue().keySet();
		return null;
	}
	public List<Integer> getAllPossibleFire(IOwner owner){
		IndividualCPNInstance instance=cpnInstance.getIndividualInstances().get(owner);
		instance.checkNewlyTokensAndMarkAsTested();
		if(instance.canFire()){
			return instance.getCanFireTransitions();
		}else{
			return Collections.emptyList();
		}
	}
	public MixedInputTokenBinding  askForBinding(IOwner owner, Integer tid){
		return cpnInstance.getIndividualInstances().get(owner).askForFire(tid);
	}
	public IOutputTokenBinding fire(IOwner owner, Integer tid,MixedInputTokenBinding binding){
		IndividualCPNInstance instance=cpnInstance.getIndividualInstances().get(owner);
		reportPlaceBeforeFiring(binding, instance, tid);
		if(instance.trytoGetTokensFromPlaces(binding)){
			instance.removeTokensFromAllTransitionsCache(binding);
			IOutputTokenBinding output=instance.fire(tid, binding);
			if(output!=null)
				instance.addLocalNewlyTokensAndScheduleNextTime(output);
			instance.checkNewlyTokensAndMarkAsTested();
			reportPlaceAfterFiring(binding, output, instance, tid);
			return output;
		}
		return null;
	}

}
