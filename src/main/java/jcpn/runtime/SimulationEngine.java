package jcpn.runtime;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Iterables;

import jcpn.elements.Pair;
import jcpn.elements.colorsets.IColor;
import jcpn.elements.colorsets.SoutColor;
import jcpn.elements.places.IPlaceMonitor;
import jcpn.elements.places.ITarget;
import jcpn.elements.places.instance.ConnectionPlaceInstance;
import jcpn.elements.places.instance.ISimpleTokenOrganizater;
import jcpn.elements.transions.ITransitionMonitor;
import jcpn.elements.transions.instance.IOutputTokenBinding;
import jcpn.elements.transions.instance.MixedInputTokenBinding;
/**
 * auto mode:
 * <br>
 * <pre>
 * SimulationEngine engine=new SimulationEngine(foldCPN);
 * engine.compile();
 * while(engine.hasNextTimepoint()){
 *  System.out.println("next time point:"+engine.getNextTimepoint());
 * 	engine.nextRound();
 * }
 * </pre>
 * <br> 
 * manually mode:
 * 
 * @author hxd
 *
 */
public class SimulationEngine {
	private static Logger logger=LogManager.getLogger();
	FoldingCPNInstance cpnInstance;
	long maximumExecutionTime=Long.MAX_VALUE;
	Status status=Status.STOPPED;
	//because we use stream.parallel, we do not need to use ForkJoinPool or ExecutorService 
	//for firing
	//	ForkJoinPool pool;
	//for sending (because sending is more fast, we use a simple executorService thread pool rather than a forkjoin pool)
	//	ExecutorService service;
	GlobalClock clock;
	public static enum Mode{
		NORMAL,STREAM
	}
	public static enum Status{
		RUNNING,STOPPED
	}
	/**
	 * 
	 * @param cpnInstance
	 * @param mode if it is stream mode, the engine will never stop even though there is no transition can fire, because we can use a stream way to add new tokens
	 */
	Mode mode;
	//	BlockingQueue<AdditionalTokenPackage> queue=new LinkedBlockingQueue<>();


	Map<Integer,IPlaceMonitor> placeMonitors=new HashMap<>();
	Map<Integer, ITransitionMonitor> transitionMonitors=new HashMap<>();

	public SimulationEngine(FoldingCPNInstance cpnInstance,Mode mode) {
		super();
		this.cpnInstance = cpnInstance;
		clock=GlobalClock.getInstance();
		//		pool=new ForkJoinPool(Runtime.getRuntime().availableProcessors()*4);
		cpnInstance.getIndividualInstances().keySet().forEach(owner->clock.addAbsoluteTimepointForRunning(owner, 0L));
		//		service=Executors.newCachedThreadPool(r->new Thread(r, "Sending Thread"));
		this.mode=mode;
	}

	public boolean compile(){
		this.status=Status.RUNNING;
		if(cpnInstance.isCompiled()){
			return true;
		}else{
			cpnInstance.compile();
			cpnInstance.getIndividualInstances().keySet().forEach(owner->clock.addAbsoluteTimepointForRunning(owner, 0L));
			return true;
		}
	}


	public boolean hasNextTimepoint(){
		checkStatus();
		if(mode.equals(Mode.NORMAL))
			return clock.hasNextTime();
		else 
			return true;
	}

	/**
	 *
	 * @return whether the sending queue has events to do.
     */
	public boolean hasNextSendingTimepoint(){
		checkStatus();
			return clock.hasNextSendingTime();
	}

	public boolean hasNextTimepointWithoutQueue(){
		checkStatus();
		return clock.hasNextTime();
	}

	protected synchronized void makeSureWeHaveTasks(){
		try {
			logger.debug(()->"no events in the time line, will wait for additional tokens into the system...");
			wait();
			logger.trace(()->"received additional tokens  out of system.. ");
		} catch (InterruptedException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}
	public void checkStatus(){
		if(GlobalClock.getInstance().getTime()>=maximumExecutionTime){
			this.status=Status.STOPPED;
		}
	}
	/**
	 * 
	 * @return 
	 * @throws InterruptedException
	 */
	public long getNextTimepoint() throws InterruptedException{
		//		long time=GlobalClock.getInstance().getTime();
		//		AdditionalTokenPackage package1=null;
		//		while((package1=queue.poll())!=null){
		//			cpnInstance.addTokens(package1.getRelativeTime(), package1.getTarget(), package1.getTokens());
		//		}
		//		if(mode.equals(Mode.STREAM)&&!hasNextTimepointWithoutQueue()){
		//			package1=queue.take();
		//			cpnInstance.addTokens(package1.getRelativeTime(), package1.getTarget(), package1.getTokens());
		//		}
		checkStatus();
		if(mode.equals(Mode.STREAM)&&!hasNextTimepointWithoutQueue()){
			makeSureWeHaveTasks();
		}
		Pair<Long, Long> nextTime=clock.nextTime();
		if(nextTime.getLeft()<nextTime.getRight()){
			return nextTime.getLeft();
		}
		else{
			if(nextTime.getRight()<0){
				return -nextTime.getRight();
			}else{
				return nextTime.getRight();
			}
		}
	}
	/**
	 * 
	 * @param absoluteTime
	 * @param target
	 * @param tokens
	 * @return the actually time that the time line accepts (it is: absoluteTime or currentTime+1)
	 * @throws InterruptedException
	 */
	public synchronized long pushTokens(Long absoluteTime, ITarget target, Map<Integer, List<IColor>> tokens) throws InterruptedException{
		//		queue.put(AdditionalTokenPackage.normalTokens(absoluteTime, target, tokens));
		long time=cpnInstance.addTokens(absoluteTime, target, tokens);
		notifyAll();
		return time;
	}
	/**
	 * 
	 * @param absoluteTime
	 * @param sout the sout name.
	 * @param targets  the cpn instances you want to enable.
	 * @return the actually time that the time line accepts (it is: absoluteTime or currentTime+1)
	 * @throws InterruptedException
	 */
	public  synchronized long pushSoutTokens(Long absoluteTime,ITarget sout, ITarget[] targets) throws InterruptedException{
		//		queue.put(AdditionalTokenPackage.soutTokens(absoluteTime, sout, SoutColor.generateList(targets)));
		logger.trace(()->String.format("push a sout token at time %s to target %s. Contents: %s",absoluteTime,sout,Arrays.toString(targets)));
//		logger.info(()->String.format("at time %d, add token to sout: %s",absoluteTime,Arrays.toString(targets)));
		long time=cpnInstance.addTokens(absoluteTime, sout, Collections.singletonMap(1, SoutColor.generateList(targets)));
		logger.trace(()->"will notify all..");
		notifyAll();
		return time;
	}

	protected void reportPlaceBeforeFiring(MixedInputTokenBinding binding, IndividualCPNInstance cpn, Integer tid){
		if(placeMonitors.size()>0&&binding.hasSimpleTokens()){
			binding.getSimpleTokens().keySet().stream().filter(pid->placeMonitors.containsKey(pid)).forEach(pid->{
				placeMonitors.get(pid).reportWhenDigest(cpn.getOwner(), binding.getSimpleTokens().get(pid), 
						Iterables.concat(((ISimpleTokenOrganizater)cpn.getPlace(pid)).getTestedTokens(),
								((ISimpleTokenOrganizater)cpn.getPlace(pid)).getNewlyTokens(),
								((ISimpleTokenOrganizater)cpn.getPlace(pid)).getFutureTokens()), 
						cpn.getTransitions().get(tid).getName(),tid);
			});

		}
		if(placeMonitors.size()>0&&binding.hasConnectionTokens()){
			binding.getConnectionTokens().keySet().stream().filter(pid->placeMonitors.containsKey(pid)).forEach(pid->{
				placeMonitors.get(pid).reportWhenDigest(cpn.getOwner(), binding.getSimpleTokens().get(pid), 
						Iterables.concat(((ConnectionPlaceInstance)cpn.getPlace(pid)).getTestedTokens().get(binding.getTarget()),
								((ConnectionPlaceInstance)cpn.getPlace(pid)).getNewlyTokens().get(binding.getTarget()),
								((ConnectionPlaceInstance)cpn.getPlace(pid)).getFutureTokens().get(binding.getTarget())), 
						cpn.getTransitions().get(tid).getName(),tid);
			});
		}
	}
	protected void reportPlaceAfterFiring(MixedInputTokenBinding binding,IOutputTokenBinding output,IndividualCPNInstance cpn, Integer tid){
		if(transitionMonitors.containsKey(tid)){
			transitionMonitors.get(tid).reportWhenFired(cpn.getOwner(), cpn.getTransitions().get(tid).getName(), tid, binding, output);
		}
		if(placeMonitors.size()>0&&output.hasLocalForNP()){
			output.getLocalForNP().keySet().stream().filter(pid->placeMonitors.containsKey(pid)).forEach(pid->{
				placeMonitors.get(pid).reportWhenGenerated(cpn.getOwner(), output.getLocalForNP().get(pid), 
						Iterables.concat(((ISimpleTokenOrganizater)cpn.getPlace(pid)).getTestedTokens(),
								((ISimpleTokenOrganizater)cpn.getPlace(pid)).getNewlyTokens(),
								((ISimpleTokenOrganizater)cpn.getPlace(pid)).getFutureTokens()), 
						cpn.getTransitions().get(tid).getName(),tid);
			});
		}
		if(placeMonitors.size()>0&&output.hasLocalForConnection()){
			output.getLocalForConnection().keySet().stream().filter(pid->placeMonitors.containsKey(pid)).forEach(pid->{
				placeMonitors.get(pid).reportWhenGenerated(cpn.getOwner(), 
						output.getLocalForConnection().get(pid),
						((ConnectionPlaceInstance)cpn.getPlace(pid)).getTestedTokens(),
						((ConnectionPlaceInstance)cpn.getPlace(pid)).getNewlyTokens(),
						((ConnectionPlaceInstance)cpn.getPlace(pid)).getFutureTokens(), 
						cpn.getTransitions().get(tid).getName(),tid);
			});
		}
	}
	public void addPlaceMonitor(Integer pid,IPlaceMonitor monitor){
		placeMonitors.put(pid, monitor);
	}
	public void addTransitionMonitor(Integer tid,ITransitionMonitor monitor){
		transitionMonitors.put(tid, monitor);
	}

	public long getMaximumExecutionTime() {
		return maximumExecutionTime;
	}

	public void setMaximumExecutionTime(long maximumExecutionTime) {
		this.maximumExecutionTime = maximumExecutionTime;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
}
