package jcpn.runtime;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jcpn.elements.CPNet;
import jcpn.elements.colorsets.IColor;
import jcpn.elements.colorsets.SoutColor;
import jcpn.elements.colorsets.UnitColor;
import jcpn.elements.places.ConnectionPlace;
import jcpn.elements.places.ITarget;
import jcpn.elements.places.IndividualPlace;
import jcpn.elements.places.Place;
import jcpn.elements.places.Place.PlaceType;
import jcpn.elements.places.StringOwner;
import jcpn.elements.transions.ConnectionTransition;
import jcpn.elements.transions.IndividualTransition;
import jcpn.elements.transions.Transition;
import jcpn.elements.transions.Transition.TransitionType;
import jcpn.elements.transions.instance.LocalOutputTokenBinding;
import jcpn.elements.transions.instance.RemoteOutputTokenBinding;

public class HiddenCPNInstance extends IndividualCPNInstance{
	private UnitColor unitToken=new UnitColor();
	private boolean infinite=true;
	public HiddenCPNInstance(StringOwner owner, GlobalConnector connector
			,TokenAssignmentForHidden assignment, TokenRelativeTimeForHidden timeCosts,boolean infinite) {
		super(owner, connector);
		this.infinite=infinite;
		/*
		 * 
		 * 
		 *        P1                 T1                P2                T2                 P3
		 *   ____________        ___________       ____________       ___________        ____________      
		 *  /            \       |         |      /            \      |         |       /            \   
		 * |  SYS_INPUT   |----->| INPUT   |---> |  SYS_INPUT2  |---->| INPUT   |----> |  OUTPUT      |
		 *  \____________/ I     |_________|I     \____________/C     |_________|C      \____________/I
		 *  	/|\												         |     
		 * 	     |_______________________________________________________|
		 * Tokens in P1: owner names of users' CPNs.
		 * 
		 */
		IndividualPlace place=new IndividualPlace(1, "SYS_INPUT");
		//		place.addInitToken(owner,unitToken);
		place.addOutput(1, 1);
		ConnectionPlace connectionPlace=new ConnectionPlace(2, "SYS_INPUT2");
		connectionPlace.addOutput(2, 1);
		Map<PlaceType, List<Place>> places=new HashMap<>();
		places.put(PlaceType.IndividualPlace, Arrays.asList(place));
		places.put(PlaceType.ConnectionPlace, Arrays.asList(connectionPlace));

		Map<TransitionType, List<Transition>> transitions=new HashMap<>();
		IndividualTransition transition=new IndividualTransition(1, "SYS_INPUT");
		transition.addInArc(1, 1);
		transition.addOutput(2, 1);
		transitions.put(TransitionType.IndividualTransition, Arrays.asList(transition));
		//the first transition is used for generate ConnectionPlace.
		//the color in ConnectionPlace is: (owner, target, unit), 
		//unit is meaningless, owner is the hidden instance itself (so, it is also meaning less)
		//target is useful, it comes from the input. Therefore, the color of place 1 MUST BE SoutColor
		transition.setOutputFunction(input-> 
		LocalOutputTokenBinding.connectionBinding(0,  
				Collections.singletonMap(2, 
						Collections.singletonMap( ((SoutColor)input.getSimpleTokens().get(1)).getTarget(), 
						Collections.singletonList(unitToken))))
				);

		ConnectionTransition transition2=new ConnectionTransition(2, "SYS_INPUT_2");
		transition2.addInArc(2, 1);
		if(this.infinite){
			transition2.setOutputFunction(input->{
						long time=timeCosts.apply(input.getTarget());
						return RemoteOutputTokenBinding.mixBinding(input.getTarget(), 
								Collections.singletonMap(1, Collections.<IColor>singletonList(soutColors.computeIfAbsent(input.getTarget(), t-> new SoutColor(t)))),
								null,
								time, 
								assignment.apply(input.getTarget()), 
								time);
						}
			);
		}
		else{
			transition2.setOutputFunction(input->{
				return RemoteOutputTokenBinding.remoteBinding(assignment.apply(input.getTarget()), timeCosts.apply(input.getTarget()), input.getTarget());
				}
			);
		}
		transitions.put(TransitionType.ConnectionTransition, Arrays.asList(transition2));
		CPNet net=new CPNet();
		Map<Integer,Place> tmpPlaces=new HashMap<>();
		tmpPlaces.put(1, place);
		tmpPlaces.put(2, connectionPlace);
		net.setPlaces(tmpPlaces);
		transition.preCompile(net);
		transition2.preCompile(net);
		this.construct(places, transitions, Collections.emptyMap(), Collections.emptySet());
//		this.remoteSchedule=new ConcurrentHashMap<>();
	}
	/**this is just for saving memory spaces... (we need not to generate new sout object )*/
	Map<ITarget, SoutColor> soutColors=new HashMap<>();


	/**
	 * declare which targets the generator will send to.
	 */
	@FunctionalInterface
	public static interface TokenRelativeTimeForHidden{
		/**
		 * declare which targets the generator will send to.
		 * @return
		 */
		public long apply(ITarget target);
	}


	/**
	 * using this method to tell the simulation that 
	 * what tokens the generator will generate and where (which places) the tokens will be put.
	 */
	@FunctionalInterface
	public static interface TokenAssignmentForHidden{
		/**
		 * using this method to tell the simulation that 
		 * what tokens the generator will generate and where (which places) the tokens will be put.
		 * @return
		 */
		public Map<Integer, List<IColor>> apply(ITarget target);
	}

}
