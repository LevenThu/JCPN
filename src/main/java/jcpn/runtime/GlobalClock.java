package jcpn.runtime;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jcpn.elements.Pair;
import jcpn.elements.places.IOwner;

public class GlobalClock {
	private static Logger logger=LogManager.getLogger();
	private static GlobalClock globalClock=new GlobalClock();
	private long time=0L;
	public static GlobalClock getInstance(){
		return globalClock;
	}
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}
	
	ConcurrentSkipListMap<Long, Map<IOwner, Object>> timelineForRunning=new ConcurrentSkipListMap<>();
	ConcurrentSkipListMap<Long, Map<IOwner, Object>> timelineForSending=new ConcurrentSkipListMap<>();
	
	public void addAbsoluteTimepointForRunning(IOwner owner, long absolutiveTime){
		timelineForRunning.computeIfAbsent(absolutiveTime, o->new ConcurrentHashMap<IOwner, Object>()).put(owner, owner);
	}
	/**
	 * notice, we do not recommend that add a sending event whose time point is the same with current clock (i.e., the time cost of a sending event is zero.). 
	 * @param owner
	 * @param absolutiveTime
	 */
	public void addAbsoluteTimepointForSending(IOwner owner, long absolutiveTime){
		timelineForSending.computeIfAbsent(absolutiveTime, o->new ConcurrentHashMap<IOwner, Object>()).put(owner, owner);
	}
	public boolean hasNextRunningTime(){
		return timelineForRunning.size()>0;
	}
	public boolean hasNextSendingTime(){
		return timelineForSending.size()>0;
	}
	public boolean hasNextTime(){
		return hasNextRunningTime()|| hasNextSendingTime();
	}
	/***
	 * return (next sending time, next running time)
	 *  <br> the global clock will be updated
	 * @return
	 */
	public Pair<Long, Long> nextTime(){
		if(timelineForSending.size()==0&&timelineForRunning.size()==0){
			logger.debug(()->"no next time, but nextTime is called");
			return null;
		}
		else if(timelineForSending.size()==0){
			logger.debug(()->"next time is running event, and the current clock is "+time);
			time=timelineForRunning.firstKey();
			logger.debug(()->"next time is "+time);
			logger.trace(()->timelineForRunning.firstEntry().getValue());
			return new Pair<Long, Long>(-time, -time);
		}else if(timelineForRunning.size()==0){
			logger.debug(()->"next time is sending event, and the current clock is "+time);
			time=timelineForSending.firstKey();
			logger.debug(()->"next time is "+time);
			logger.trace(()->timelineForSending.firstEntry().getValue());
			return new Pair<Long, Long>(time, time);
		}
		time=timelineForSending.firstKey()< timelineForRunning.firstKey()?timelineForSending.firstKey(): timelineForRunning.firstKey();
		logger.debug(()->"next time is " +(timelineForSending.firstKey()< timelineForRunning.firstKey()?"sending":"running")+" event, current clock is "+time);
		return new Pair<Long, Long>(timelineForSending.firstKey(), timelineForRunning.firstKey());
	}
	/**
	 * return timepoint-> (owner,owner) (that is, the inner Map is a set actually)
	 * <br> the global clock will be updated
	 * @return
	 */
	public Map.Entry<Long, Map<IOwner, Object>>  pollNextRunningTime(){
		if(timelineForRunning.size()==0){
			logger.trace(()->"no running timepoint, but called getNextRunningTime");
			return null;
		}
		time=timelineForRunning.firstKey();
		return timelineForRunning.pollFirstEntry();
	}
	/**
	 * return timepoint-> (owner,owner) (that is, the inner Map is a set actually)
	 * <br> the global clock will be updated
	 * @return
	 */
	public Map.Entry<Long, Map<IOwner, Object>>  pollNextSendingTime(){
		if(timelineForSending.size()==0)
			return null;
		time=timelineForSending.firstKey();
		Map.Entry<Long, Map<IOwner, Object>> next=timelineForSending.pollFirstEntry();
		logger.debug(()->"next sending event: "+ next.getKey()+","+next.getValue());
		return next;
	}
	
}
