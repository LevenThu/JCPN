package cassandra.distributions;

import java.util.stream.IntStream;

import org.apache.commons.math3.distribution.ExponentialDistribution;
import org.junit.Test;

public class TimeDistributionUtil {

	@Test
	public void testExponential(){
		ExponentialDistribution exponentialDistribution=new ExponentialDistribution(10);
		IntStream.range(0, 1000).forEach(i->System.out.println((long)exponentialDistribution.sample()));
	}
}
