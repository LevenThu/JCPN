package cassandra.colors;

import jcpn.elements.colorsets.IColor;
import jcpn.elements.places.IOwner;

public class Server extends IColor {
	@Override
	public String toString() {
		return "Server [time=" + time + ", name=" + name + "]";
	}

	IOwner name;
	
	public Server(IOwner name) {
		super();
		this.name = name;
	}

	
	public IOwner getName() {
		return name;
	}

	public void setName(IOwner name) {
		this.name = name;
	}

}
