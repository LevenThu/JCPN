package cassandra.colors;

import cassandra.colors.ClientRequest.ClientRequestType;
import jcpn.elements.colorsets.IColor;
import jcpn.elements.places.IOwner;
import jcpn.elements.places.ITarget;

public class ServerTask extends IColor{
	@Override
	public String toString() {
		return "ServerTask [datum=" + datum + ", from=" + from + ", callback=" + callback + ", type=" + type + "]";
	}
	Datum datum;
	private ITarget from;
	private IOwner now;
	public ServerTask(ClientRequest request, int callback, ITarget from,IOwner now) {
		this(request.getDatum(),callback,request.getType().equals(ClientRequestType.WRITE)?TaskType.WRITE:TaskType.READ,from,now);
	}
	public ServerTask(Datum datum, int callback, TaskType type, ITarget from,IOwner now) {
		super();
		this.datum = datum;
		this.callback = callback;
		this.type = type;
		this.from=from;
		this.now=now;
	}
	public Datum getDatum() {
		return datum;
	}
	public void setDatum(Datum datum) {
		this.datum = datum;
	}
	public int getCallback() {
		return callback;
	}
	public void setCallback(int callback) {
		this.callback = callback;
	}
	public TaskType getType() {
		return type;
	}
	public void setType(TaskType type) {
		this.type = type;
	}
	public ITarget getFrom() {
		return from;
	}
	public void setFrom(ITarget from) {
		this.from = from;
	}
	public IOwner getNow() {
		return now;
	}
	public void setNow(IOwner now) {
		this.now = now;
	}
	int callback;
	TaskType type;
	public static enum TaskType{
		WRITE, READ, RESPONSE
	}
}
