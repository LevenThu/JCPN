package cassandra.colors;

import jcpn.elements.colorsets.IColor;

public class Datum extends IColor{
	long key;
	int value;
	long timestamp;
	
	public long getKey() {
		return key;
	}
	public void setKey(long key) {
		this.key = key;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	@Override
	public String toString() {
		return "Datum [key=" + key + ", value=" + value + ", timestamp=" + timestamp + ", time=" + time + "]";
	}
	public Datum(long key, int value, long timestamp) {
		super();
		this.key = key;
		this.value = value;
		this.timestamp = timestamp;
	}
	
}
