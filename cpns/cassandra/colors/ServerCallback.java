package cassandra.colors;

import java.util.concurrent.atomic.AtomicInteger;

import jcpn.elements.colorsets.IColor;
/**
 * server callback has no time properties
 * @author hxd
 *
 */
public class ServerCallback extends IColor{
	public static AtomicInteger callbackGenerater=new AtomicInteger(); 
	int id;
	int number;
	private long startTime;
	public ServerCallback(int id, int number, long startTime) {
		super();
		this.id = id;
		this.number = number;
		this.startTime=startTime;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	@Override
	public String toString() {
		return "ServerCallback [id=" + id + ", number=" + number + ", time=" + time + "]";
	}
	public long getStartTime() {
		return startTime;
	}
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}
	@Override
	public void setTime(long time){
		this.time=0;
	}
	

}
