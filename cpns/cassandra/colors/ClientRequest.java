package cassandra.colors;

import jcpn.elements.colorsets.IColor;

public class ClientRequest extends IColor{
	Datum datum;
	
	ClientRequestType type;
	
	 int consistency;
	
	public static enum ClientRequestType{
		WRITE,QUERY
	}
	
	@Override
	public String toString() {
		return "ClientRequest [datum=" + datum + ", time=" + time + ", type=" + type + ", consistency=" + consistency + "]";
	}
	public ClientRequest(long key, int value, long timestamp,ClientRequestType type,int consistency) {
		super();
		this.type=type;
		this.datum=new Datum(key, value, timestamp);
		this.consistency=consistency;
	}
	
	public Datum getDatum() {
		return datum;
	}
	public void setDatum(Datum datum) {
		this.datum = datum;
	}
	public ClientRequestType getType() {
		return type;
	}
	public void setType(ClientRequestType type) {
		this.type = type;
	}
	public int getConsistency() {
		return consistency;
	}
	public void setConsistency(int consistency) {
		this.consistency = consistency;
	}
	
}
