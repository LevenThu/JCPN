package cassandra.colors;

import jcpn.elements.places.IOwner;

public class ServerLocation extends Server{

	private long location;
	public ServerLocation(IOwner name,long location){
		super(name);
		this.location=location;
	}
	public long getLocation() {
		return location;
	}
	public void setLocation(long location) {
		this.location = location;
	}
	@Override
	public String toString() {
		return "ServerLocation [location=" + location + ", name=" + name + ", time=" + time + "]";
	}
	
	
}
