package cassandra.jcpn;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.IntStream;

import org.apache.commons.math3.random.EmpiricalDistribution;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cassandra.colors.ClientRequest;
import cassandra.colors.ClientRequest.ClientRequestType;
import cassandra.colors.Datum;
import cassandra.colors.Server;
import cassandra.colors.ServerCallback;
import cassandra.colors.ServerLocation;
import cassandra.colors.ServerTask;
import cassandra.colors.ServerTask.TaskType;
import jcpn.elements.CPNet;
import jcpn.elements.Pair;
import jcpn.elements.colorsets.GlobalPlaceColor;
import jcpn.elements.colorsets.IColor;
import jcpn.elements.colorsets.UnitColor;
import jcpn.elements.places.ConnectionPlace;
import jcpn.elements.places.GlobalPlace;
import jcpn.elements.places.IOwner;
import jcpn.elements.places.ITarget;
import jcpn.elements.places.IndividualPlace;
import jcpn.elements.places.Place;
import jcpn.elements.places.Place.PlaceStrategy;
import jcpn.elements.places.StringOwner;
import jcpn.elements.places.instance.ConnectionPlaceInstance;
import jcpn.elements.places.instance.ISimpleTokenOrganizater;
import jcpn.elements.transions.ConnectionTransition;
import jcpn.elements.transions.ITransitionMonitor;
import jcpn.elements.transions.IndividualTransition;
import jcpn.elements.transions.Transition;
import jcpn.elements.transions.instance.IOutputTokenBinding;
import jcpn.elements.transions.instance.LocalOutputTokenBinding;
import jcpn.elements.transions.instance.MixedInputTokenBinding;
import jcpn.elements.transions.instance.RemoteOutputTokenBinding;
import jcpn.runtime.AutoSimulationEngine;
import jcpn.runtime.FoldingCPNInstance;
import jcpn.runtime.GlobalClock;
import jcpn.runtime.SimulationEngine.Mode;
import jcpn.runtime.SimulationEngine.Status;

/**
 * properties:
 * replica: 3
 * consistency: 2 (must be less than replica )
 * clients: 1
 * ops: 200
 * maxTime: 100
 * nodes: 128
 * distributions: logs/empiricalDistribution
 * @author hxd
 *
 */
public class CassandraCPN {
	private static final StringOwner SOUT_CLIENT = new StringOwner("SOUT_Client");
	private static Logger logger=LogManager.getLogger();
	int ringSize=1000000;
	int replica=Integer.valueOf(System.getProperty("replica","3"));
	int consistency=Integer.valueOf(System.getProperty("consistency","2"));
	FoldingCPNInstance foldingCPN;
	AtomicInteger counter=new AtomicInteger(0);
	static Map<String,StringOwner> ownerCaches=new HashMap<>();
	int concurrentClients=Integer.valueOf(System.getProperty("clients","10"));
	int maxNodes=Integer.valueOf(System.getProperty("nodes","128"));
	
	//for control OPS.
	int opsInOneSecond=Integer.valueOf(System.getProperty("ops","200"));
	AtomicInteger finishedInOneSecond=new AtomicInteger(0);
	long currentSecond=0;//1s=1000000us
	long beginTime;
	AutoSimulationEngine engine;
//	List<Long> latencies=new ArrayList<>();
	long maxLatency=-1;
	long minLatency=Long.MAX_VALUE;
	AtomicLong sumLatency=new AtomicLong(0);
	Properties empiricalDistributions;
	ConcurrentHashMap<IOwner, Integer> writeLengths=new ConcurrentHashMap<>();
	ConcurrentHashMap<IOwner, ConcurrentHashMap<IOwner, Integer>> sendingLengths=new ConcurrentHashMap<>();
	
	long oneRange=ringSize/maxNodes;
	public static void main(String[] args) throws InterruptedException, FileNotFoundException, IOException{
		CassandraCPN cassandra=new CassandraCPN();
		
		IntStream.range(0, cassandra.maxNodes).forEach(i->ownerCaches.put("S"+i, new StringOwner("S"+i)));
		ownerCaches.put("SOUT_Client",SOUT_CLIENT);
		ownerCaches.put("_SYS_HIDDEN_INSTANCE",new StringOwner("_SYS_HIDDEN_INSTANCE"));
		
		
		cassandra.empiricalDistributions=new Properties();
		Properties distributions=new Properties();
		distributions.load(new FileReader(System.getProperty("distributions","configs/empiricalDistribution")));
		distributions.forEach((k,v)->{
			cassandra.empiricalDistributions.put(k, Arrays.asList(v.toString().split(",")).stream().mapToDouble(x->Double.valueOf(x)).toArray());
		});
		
		cassandra.construct();
		cassandra.foldingCPN.compile();
		cassandra.engine=new AutoSimulationEngine(cassandra.foldingCPN,Mode.STREAM);
		cassandra.engine.compile();
		
		cassandra.engine.addTransitionMonitor(8, new ITransitionMonitor() {
			@Override
			public void reportWhenFired(IOwner owner, String transitionName, int transitionId, MixedInputTokenBinding inputs,
					IOutputTokenBinding outputTokenBinding) {
				logger.debug(()->owner+" executes transition: "+transitionName+", inputs: "+inputs+", outputs: "+outputTokenBinding);
			}
		});
		new Thread(()->{
			try {
				long time=0;
				for(int i=0;i<cassandra.maxNodes;i++){
					ITarget target=ownerCaches.get("S"+i);
					cassandra.sendingLengths.put(target, new ConcurrentHashMap<>());
					for(int j=0;j<cassandra.maxNodes;j++){
						cassandra.sendingLengths.get(target).put(ownerCaches.get("S"+j), 0);
					}
					cassandra.writeLengths.put(target, 0);
					cassandra.foldingCPN.registerNewOwner(target);
					time=cassandra.engine.pushTokens(time+100, target, Collections.singletonMap(1,Collections.singletonList(new ServerLocation(ownerCaches.get("S"+i),i*cassandra.oneRange))));
				}
				
				ITarget[] targets=new ITarget[cassandra.concurrentClients];
				IntStream.range(0, cassandra.concurrentClients).forEach(i->targets[i]=ownerCaches.get("S"+i%cassandra.maxNodes));
				cassandra.engine.pushSoutTokens(time+100, SOUT_CLIENT, targets);
				//				engine.pushSoutTokens(100L, "CLIENT", new Object[]{"s1"});
				cassandra.beginTime=time+100;
				cassandra.engine.setMaximumExecutionTime(1000000L*Integer.valueOf(System.getProperty("maxTime","100"))+cassandra.beginTime);//us
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();
		while(cassandra.engine.hasNextTimepoint()&&cassandra.engine.getStatus()!=Status.STOPPED){
			logger.debug("next time point:"+cassandra.engine.getNextTimepoint());
			boolean ok=cassandra.engine.nextRound();
			if(!ok){
				logger.debug(()-> "no transitions can be fired");
			}
//			if(logger.isTraceEnabled()){
//				cassandra.foldingCPN.getIndividualInstances().values().forEach(c-> {
//					logger.trace(c.printInterestingTokens(new int[]{6,7,8,9}));
//				});
//				logger.trace("\n============");
//			}
		}
		if(cassandra.engine.getStatus().equals(Status.STOPPED)){
			logger.info(()->"simulation engine stopped.");
			double ops=cassandra.counter.get()/((cassandra.engine.getMaximumExecutionTime()-cassandra.beginTime)/1000000);
			logger.info(()->"finished: "+ cassandra.counter.get()+ ", ops is: "+ ops);
			
//			LongSummaryStatistics statistics=cassandra.latencies.stream().mapToLong(x->x).summaryStatistics();
			logger.info(()->String.format("latency avg: %f,max: %d,min: %d",(cassandra.sumLatency.get()+0.0)/cassandra.counter.get(),cassandra.maxLatency,cassandra.minLatency));
			logger.info(()->"max write queue lengths:"+cassandra.writeLengths);
			if(logger.isInfoEnabled()){
				StringBuilder sBuilder=new StringBuilder();
				cassandra.sendingLengths.forEach((node,lt)->{
					sBuilder.append("\n"+node.getName()+"::\t");
					sBuilder.append(" avg:"+lt.values().stream().mapToInt(x->x).sum()/(cassandra.maxNodes-1)+";\t");
					lt.forEach((target,value)->{sBuilder.append(target.getName()+":"+value+",\t");});
				});
				logger.info("max sending queue lengths: "+sBuilder.toString());
			}
				
		}
	}
	@SuppressWarnings("unchecked")
	public void construct(){
		Random random=new Random();
//		ExponentialDistribution clientRequestDistribution=new ExponentialDistribution(1);//send one data per 10ms (mean value) 
//		ExponentialDistribution lookupTimeCost=new ExponentialDistribution(10);// 10ms (mean value) 
//		ExponentialDistribution internalNetworkTimeCost=new ExponentialDistribution(50);// 10ms (mean value) 
//		ExponentialDistribution writeTimeCost=new ExponentialDistribution(100);
//		ExponentialDistribution syncTimeCost=new ExponentialDistribution(5);
		EmpiricalDistribution lookupTimeCost2=new EmpiricalDistribution(100);
		lookupTimeCost2.load((double[]) empiricalDistributions.get("write_lookup"));
		EmpiricalDistribution internalNetworkTimeCost2=new EmpiricalDistribution(100);
		internalNetworkTimeCost2.load((double[]) empiricalDistributions.get("write_network"));
		EmpiricalDistribution writeTimeCost2=new EmpiricalDistribution(100);
		writeTimeCost2.load((double[]) empiricalDistributions.get("write_locally"));
		EmpiricalDistribution syncTimeCost2=new EmpiricalDistribution(100);
		syncTimeCost2.load((double[]) empiricalDistributions.get("write_response"));
		
		CPNet net=new CPNet();
		foldingCPN=new FoldingCPNInstance(net);
		Map<Integer, Place> places=new HashMap<>();
		Map<Integer, Transition>transitions=new HashMap<>();
		net.setPlaces(places);
		net.setTransitions(transitions);
		Place p=new IndividualPlace(1, "unstarted servers");
		Transition t=new IndividualTransition(1, "start up");
		transitions.put(1, t);
		p.addOutput(1, 1);
		places.put(1, p);
		p=new IndividualPlace(2, "alive servers");//Server
		places.put(2, p);
		t.addOutput(2, 1);
		p=new GlobalPlace(3, "lookup table");//ServerLocation
		places.put(3, p);
		((GlobalPlace)p).setMergeFunction((lt,token)->{//if need to merge, it means the server restart...
			int loc=lt.size();
			for(int i=0;i<lt.size();i++){
				if(((ServerLocation)lt.get(i)).getLocation()>((ServerLocation)token).getLocation()){
					loc=i;
					break;
				}
			}
			lt.add(loc,token);
		});
		p.addOutput(1, 1);//lookup table also be an input as start up, then he can know who are alive.
		t.addOutput(3, 1);
		p=new IndividualPlace(4, "write resources");//UnitColor
		places.put(4, p);
		t.addOutput(4, 1);
		p=new IndividualPlace(5, "response resources");//UnitColor
		places.put(5, p);
		t.addOutput(5, 1);
		p=new ConnectionPlace(6, "network resources");//UnitColor
		places.put(6, p);
		t.addOutput(6, 1);
		p=new ConnectionPlace(7, "connection notifications");//UnitColor
		places.put(7, p);
		t.addOutput(7, 1);


		t=new ConnectionTransition(2, "connection notify");
		transitions.put(2, t);
		p.addOutput(2, 1);//p.id=7
		p=new IndividualPlace(8, "received connections");//Server
		t.addOutput(8, 1);

		places.put(8, p);
		t=new IndividualTransition(3, "complete connection");
		transitions.put(3, t);
		p.addOutput(3, 1);//p.id=8
		t.addOutput(6, 1);

		p=new IndividualPlace(9, "client requests");//ClientRequest
		places.put(9, p);


		t=new IndividualTransition(4, "scan lookup table");
		transitions.put(4, t);
		p.addOutput(4, 1);
		places.get(3).addOutput(4, 1);//lookup table 
		p=new ConnectionPlace(10, "reqeusts for sending");//ServerTask
		p.setPlaceStrategy(PlaceStrategy.FIFO);//
		places.put(10, p);
		t.addOutput(10, 1);
		p=new IndividualPlace(11, "requests for writing");//ServerTask
		p.setPlaceStrategy(PlaceStrategy.FIFO);
		places.put(11, p);
		t.addOutput(11, 1);
		p=new IndividualPlace(12, "callbacks");//ServerCallback
		places.put(12,p);
		t.addOutput(12, 1);

		t=new ConnectionTransition(5, "network transmit");
		transitions.put(5, t);
		places.get(10).addOutput(5, 1);//requests for sending
		places.get(6).addOutput(5, 1);//network resource
		p=new IndividualPlace(13, "received tasks");//ServerTask
		p.setPlaceStrategy(PlaceStrategy.FIFO);
		places.put(13,p);
		t.addOutput(13, 1);		
		t.addOutput(6, 1);

		t=new IndividualTransition(6, "put received task into the write queue");
		transitions.put(6, t);
		p.addOutput(6, 1);
		t.addOutput(11,	1);



		t=new IndividualTransition(7, "put received task into the sync queue");
		transitions.put(7, t);
		p.addOutput(7, 1);
		p=new IndividualPlace(14, "requests for sync");//ServerTask
		places.put(14,p);
		t.addOutput(14,	1);

		t=new IndividualTransition(8, "execute mutate");//execute write
		transitions.put(8, t);
		places.get(11).addOutput(8, 1);//requests for writing
		places.get(4).addOutput(8, 1);//write resources

		p=new IndividualPlace(15, "mutated task");//ServerTask
		places.put(15,p);
		p.setPlaceStrategy(PlaceStrategy.FIFO);
		t.addOutput(15, 1);
		p=new IndividualPlace(16, "data store");//Datum
		places.put(16, p);
		p.setInfiniteRead(true);
		t.addOutput(16, 1);
		t.addOutput(4, 1);


		t=new IndividualTransition(9, "generate response msg for sending back");
		transitions.put(9, t);
		places.get(15).addOutput(9, 1);//mutated task
		t.addOutput(10, 1);//requests for sending


		t=new IndividualTransition(10, "generate resposne msg for sync");
		transitions.put(10,t);
		places.get(15).addOutput(10, 1);//mutated task
		t.addOutput(14, 1);//requests for sync


		t=new IndividualTransition(11, "sync");
		t.setPriority(499);
		transitions.put(11, t);
		t.addOutput(5, 1);
		t.addOutput(12, 1);
		places.get(14).addOutput(11, 1);//requests for sync
		places.get(12).addOutput(11, 1);//callbacks
		places.get(5).addOutput(11, 1);//response resources
		p=new IndividualPlace(17, "result");
		places.put(17,p);
		t.addOutput(17, 1);
		
		t=new IndividualTransition(12, "redundant sync");
		transitions.put(12,t);
		places.get(14).addOutput(12, 1);
		places.get(5).addOutput(12, 1);

		
		foldingCPN.addSoutInstance(SOUT_CLIENT, t1->// a sout to p9 (client requests)
		Collections.singletonMap(9,
				Collections.singletonList(new ClientRequest(random.nextLong()%ringSize,random.nextInt(),GlobalClock.getInstance().getTime(),ClientRequestType.WRITE,consistency))), 
		t2->0,//(long)clientRequestDistribution.sample(),
		false);
		
		transitions.get(1).setOutputFunction(input->{//start up
			//input: 1,3. output:2,3,4,5,6,7
			Server token=(Server) input.getSimpleTokens().get(1);
			//foldingCPN.registerNewOwner(token.getName());
			Map<Integer, List<IColor>> simpleTokens=new HashMap<>();
			Map<Integer,Map<ITarget, List<IColor>>> connectionTokens=new HashMap<>();
			connectionTokens.put(6, new HashMap<>());
			connectionTokens.put(7, new HashMap<>());
			GlobalPlaceColor<ServerLocation> global=(GlobalPlaceColor<ServerLocation>) input.getSimpleTokens().get(3);
			global.getContents().forEach(sl->{
				connectionTokens.get(6).put((ITarget)sl.getName(), UnitColor.generateList(Integer.valueOf(System.getProperty("netthreads","1"))));// these tokens can use the same object to save memory..
				connectionTokens.get(7).put((ITarget)sl.getName(), UnitColor.generateList(Integer.valueOf(System.getProperty("netthreads","1"))));// these tokens can use the same object to save memory..
			});
			simpleTokens.put(2, Collections.singletonList(token));
			//for each node ,we construct a 
			simpleTokens.put(3, Collections.singletonList(new ServerLocation(token.getName(), ((ServerLocation)token).getLocation())));
			simpleTokens.put(4, UnitColor.generateList(Integer.valueOf(System.getProperty("wthreads","8"))));
			simpleTokens.put(5, UnitColor.generateList(Integer.valueOf(System.getProperty("rthreads","8"))));
			//local, we set time cost as 0
			return LocalOutputTokenBinding.mixBinding(simpleTokens, 0, connectionTokens);
		});
		transitions.get(2).setOutputFunction(input->{//connection notify
			//input 7, output 8
			//remote, we set time cost as 1
			return  RemoteOutputTokenBinding.remoteBinding(Collections.singletonMap(8, Collections.singletonList(new Server(input.getOwner()))),
					1, input.getTarget());
		});
		transitions.get(3).setOutputFunction(input->{//complete connection
			//input: 8. output 6.
			Server server=(Server) input.getSimpleTokens().get(8);
			//local, we set time cost as 0
			return LocalOutputTokenBinding.connectionBinding(0L, Collections.singletonMap(6, Collections.singletonMap((ITarget)server.getName(), Collections.singletonList(new UnitColor()))));
		});
		transitions.get(4).setOutputFunction(input->{//scan lookup table
			//input: 3, 9. output: 10, 11, 12
			GlobalPlaceColor<ServerLocation> lookup=(GlobalPlaceColor<ServerLocation>) input.getSimpleTokens().get(3);
			ClientRequest request=(ClientRequest) input.getSimpleTokens().get(9);
			int realReplica=Math.min(lookup.getContents().size(), replica);
			List<ServerLocation> list=lookup.getContents();
			//LocalOutputTokenBinding.mixBinding(simpleTokens, effective, connectionTokens)
			int loc=0;
			if(list.size()==0){//no servers...
				logger.warn(()-> "no alive servers.. but the cluster received requests");
				return LocalOutputTokenBinding.simpleBinding(Collections.emptyMap(), 0);
			}
			List<ServerLocation> selected=new ArrayList<>();;
			for(ServerLocation location:list){
				if(location.getLocation()>request.getDatum().getKey()){
					lookup.getRwl().readLock().lock();
					try{
						selected.addAll(list.subList(loc, Math.min(loc+realReplica, list.size())));
					}finally{
						lookup.getRwl().readLock().unlock();
					}
					break;
				}
				loc++;
			}
			if(selected.size()<realReplica){
				lookup.getRwl().readLock().lock();
				try{
					selected.addAll(list.subList(0, Math.min(loc, realReplica-selected.size())));
				}finally{
					lookup.getRwl().readLock().unlock();
				}
			}

			Map<Integer, List<IColor>> localTasks=new HashMap<>();
			int callbackid=ServerCallback.callbackGenerater.incrementAndGet();
			localTasks.put(12, Collections.singletonList(new ServerCallback(callbackid, request.getConsistency(),request.getDatum().getTimestamp())));
			Map<Integer, Map<ITarget, List<IColor>>> tasks=new HashMap<>();
			selected.forEach(sl->{
				if(sl.getName().equals(input.getOwner())){
					localTasks.put(11,Collections.singletonList(new ServerTask(request,callbackid,(ITarget)input.getOwner(),input.getOwner())));
				}else{
					tasks.computeIfAbsent(10, k->new HashMap<>());
					tasks.get(10).put((ITarget)sl.getName(), Collections.singletonList(new ServerTask(request,callbackid,(ITarget)input.getOwner(),input.getOwner())));
				}
			});
			return LocalOutputTokenBinding.mixBinding(localTasks, (long)lookupTimeCost2.sample(), tasks);
		});
		transitions.get(5).setOutputFunction(input->{//network transmit
			//input 6, 10.  output 6, 13
			if(sendingLengths.containsKey(input.getOwner())){
				ConnectionPlaceInstance orga=((ConnectionPlaceInstance)foldingCPN.getIndividualInstances().get(input.getOwner()).getPlace(10));
				int size=0;
				if(orga.getFutureTokens().containsKey(input.getTarget())){size+=orga.getFutureTokens().get(input.getTarget()).size();}
				if(orga.getNewlyTokens().containsKey(input.getTarget())){ size+=orga.getNewlyTokens().get(input.getTarget()).size();}
				if(orga.getTestedTokens().containsKey(input.getTarget())){size+=orga.getTestedTokens().get(input.getTarget()).size();}
				int s=size;
				sendingLengths.get(input.getOwner()).compute(input.getTarget(), (k,x)->s>x?s:x);
			}
			long time=(long) internalNetworkTimeCost2.sample();
			ServerTask sTask=(ServerTask) input.getConnectionTokens().get(10);
			sTask.setNow(input.getTarget());
			return RemoteOutputTokenBinding.mixBinding(input.getTarget(),
					null,
					Collections.singletonMap(6, Collections.singletonMap(input.getTarget(), Collections.singletonList(input.getConnectionTokens().get(6)))),
					time, 
					Collections.singletonMap(13, Collections.singletonList(sTask)),
					time);
		});
		transitions.get(6).addSiCondition(13, token->((ServerTask)token).getType().equals(TaskType.WRITE));//put received task into the write queue
		transitions.get(6).setOutputFunction(input->{//put received task into the write queue
			//input  13, output 11
			ISimpleTokenOrganizater orga=((ISimpleTokenOrganizater)foldingCPN.getIndividualInstances().get(input.getOwner()).getPlace(11));
			int size=orga.getFutureTokens().size()+orga.getNewlyTokens().size()+orga.getTestedTokens().size();
			writeLengths.compute(input.getOwner(), (k,x)->size>x?size:x);
			return LocalOutputTokenBinding.simpleBinding(Collections.singletonMap(11, Collections.singletonList(input.getSimpleTokens().get(13))), 0);
		});
		transitions.get(7).addSiCondition(13, token->((ServerTask)token).getType().equals(TaskType.RESPONSE));//put received task into the sync queue
		transitions.get(7).setOutputFunction(input->{//put received task into the sync queue
			//input  13, output 14
			return LocalOutputTokenBinding.simpleBinding(Collections.singletonMap(14, Collections.singletonList(input.getSimpleTokens().get(13))), 0);
		});

		transitions.get(8).setOutputFunction(binding->{// execute mutate
			//input 4,11, write resources, requests for writing.  output 15,16,4 mutated task, data store, write resources
			Map<Integer, List<IColor>> simpleTokens=new HashMap<>();
			simpleTokens.put(4, Collections.singletonList(binding.getSimpleTokens().get(4)));
			simpleTokens.put(16, Collections.singletonList(((ServerTask)binding.getSimpleTokens().get(11)).getDatum()));
			simpleTokens.put(15, Collections.singletonList(binding.getSimpleTokens().get(11)));
			return LocalOutputTokenBinding.simpleBinding(simpleTokens, (long)writeTimeCost2.sample());
		});

		transitions.get(9).addSiCondition(//generate response msg for sending back
				15, token->
				!((ServerTask)token).getFrom().equals(((ServerTask)token).getNow())
				);
		transitions.get(9).setOutputFunction(binding->{//generate response msg for sending back
			//input 15, mutated task, output 10 requests for sending
			ServerTask sTask=(ServerTask) binding.getSimpleTokens().get(15);
			ITarget target=sTask.getFrom();
//			sTask.setFrom(target);
			sTask.setType(TaskType.RESPONSE);
			return LocalOutputTokenBinding.connectionBinding(0, 
					Collections.singletonMap(10, 
							Collections.singletonMap(target, Collections.singletonList(sTask))));
		});

		transitions.get(10).addSiCondition(//generate resposne msg for sync
				15, token->
				((ServerTask)token).getFrom().equals(((ServerTask)token).getNow())
				);
		transitions.get(10).setOutputFunction(binding->{//generate resposne msg for sync
			//input 15, mutated task, output 14 requests for sync	
			ServerTask sTask=(ServerTask) binding.getSimpleTokens().get(15);
			sTask.setType(TaskType.RESPONSE);
			return LocalOutputTokenBinding.simpleBinding( 
					Collections.singletonMap(14, Collections.singletonList(sTask)),0);
		});
		
		transitions.get(11).addBiCondition(new Pair<Integer,Integer>(14,12), pair->//sync
		//input 14, 12 requests for sync, callback, output 12
		((ServerTask)pair.getLeft()).getCallback()==((ServerCallback)pair.getRight()).getId()
				);
		
		transitions.get(11).setOutputFunction(binding->{//sync
			//input 14, 12,5 requests for sync, callback, response resource, output 12,5
			ServerCallback callback=(ServerCallback) binding.getSimpleTokens().get(12);
			Map<Integer, List<IColor>> output=new HashMap<>();
			output.put(5, Collections.singletonList(binding.getSimpleTokens().get(5)));
			logger.trace(()->"callback number:"+callback.getNumber()+ "callback id:"+callback.getId());
			if(callback.getNumber()==1){
				logger.trace(()->"get enough callbace for "+callback.getId());
				long thisTimeCost=(long)syncTimeCost2.sample();
				//finish one.
				counter.incrementAndGet();
//				logger.info(()->"finish one by :"+binding.getOwner());
				if(opsInOneSecond!=0){
					finishedInOneSecond.incrementAndGet();
				}
				//lantecy
				long latency=GlobalClock.getInstance().getTime()-callback.getStartTime()+thisTimeCost+(long)internalNetworkTimeCost2.sample();
				sumLatency.addAndGet(latency);
				if(maxLatency<latency) maxLatency=latency;
				if(minLatency>latency) minLatency=latency;
				if(counter.get()%10000==0){
					logger.info(()->"finish:"+counter.get());
					//TODO collect latencies
				}
				if(opsInOneSecond!=0){
					if(GlobalClock.getInstance().getTime()-currentSecond>=1000000){
						currentSecond=GlobalClock.getInstance().getTime()/1000000*1000000;
						finishedInOneSecond.set(0);
					}
				}
				long waitTime=0;
				if(opsInOneSecond!=0){
					if(finishedInOneSecond.get()>=opsInOneSecond){
	//					waitTime=Math.max(1,100000-(GlobalClock.getInstance().getTime()-currentSecond));
						waitTime=GlobalClock.getInstance().getTime()-currentSecond;
					}
					else{
	//					waitTime=Math.max(1,(100000-(GlobalClock.getInstance().getTime()-currentSecond))/(opsInOneSecond-finishedInOneSecond.get())-sumLatency.get()/counter.get())*concurrentClients;//;
						waitTime=Math.max(1,(GlobalClock.getInstance().getTime()-currentSecond)/(opsInOneSecond-finishedInOneSecond.get()))*concurrentClients;
					}
				}
				ITarget[] targets=new ITarget[1];
				targets[0]=(ITarget) binding.getOwner();
				try {
					engine.pushSoutTokens(GlobalClock.getInstance().getTime()+waitTime, SOUT_CLIENT, targets);
				} catch (Exception e) {
					e.printStackTrace();
				}
				output.put(17, Collections.singletonList(new UnitColor()));
				return LocalOutputTokenBinding.simpleBinding(output, thisTimeCost);
			}
			callback.setNumber(callback.getNumber()-1);
			output.put(12, Collections.singletonList(callback));
			return LocalOutputTokenBinding.simpleBinding(output, (long)syncTimeCost2.sample());
		});
		
		transitions.get(12).setOutputFunction(binding->{//redundant sync
			//input 14,5, output 5
			return LocalOutputTokenBinding.simpleBinding(Collections.singletonMap(5, Collections.singletonList(binding.getSimpleTokens().get(5))),
					(long)syncTimeCost2.sample());
		});		

		places.get(16).setReplaceFunction((lt,token)->{//data store
			Iterator<IColor> iterator=lt.iterator();
			IColor replaced=null;
			while(iterator.hasNext()){
				IColor tmp=iterator.next();
				if(((Datum)tmp).getKey()==(((Datum)token).getKey())){
					replaced=tmp;
					iterator.remove();
					lt.add(token);
					break;
				}
			}
			return replaced;
		});

		net.preCompile();
	}
}
